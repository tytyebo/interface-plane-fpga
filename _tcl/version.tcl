proc generate_verilog { hex_value } {

	set num_digits 8
    set bit_width [expr { 4 * $num_digits } ]
    set high_index [expr { $bit_width - 1 } ]
    set reset_value [string repeat "0" $num_digits]
	set str [clock format [clock seconds] -format {%y%j%H%M}]

    if { [catch {
        set fh [open "_tcl/version_reg.v" w ]
        puts $fh "module version_reg (vout);"
        puts $fh "    output \[$high_index:0\] vout;"
        puts $fh "    assign vout = ${bit_width}'h${hex_value};"
        puts $fh "endmodule"
        close $fh
        
        set fh [open "_tcl/version.txt" w ]
        puts $fh "${hex_value}"
        close $fh
    } res ] } {
        return -code error $res
    } else {
        return 1
    }
}

foreach { flow project } $quartus(args) { break }

set str [clock format [clock seconds] -format {%y%j%H%M}]
set out [format "%X" $str]

if { [catch { generate_verilog $out } res] } {
	post_message -type critical_warning \
	"Couldn't generate Verilog file. $res"
} else {
	post_message "!!!!!!!!!!!!!!!!!!!!!!!!!!! Successfully updated version timestamp to\
	0x${str}"
}

`ifndef DEFINES_INCLUDE
    `define DEFINES_INCLUDE
    `timescale 1ns/1ns
//    `define SIMULATION 1
    `define NEW_MODES 1
    `define CF_SECTORS_BUFFER 6'd32
    `define CF_BASE_ADR 28'd256
	`define FRAM_LOG_BASE 16'h0000 // Адрес в Fram, где расположено начало журнала записей CF
    `define FRAM_LOG_BYTES_SIZE 4'd12
    
    `define CF_16GB_ADR_SIZE 28'h01DC_D600
`endif
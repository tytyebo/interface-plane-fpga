`include "defines.svh"
package defines_pkg;
    typedef struct packed {
        logic [31:0] cf_adr;
        logic [7:0] num;
        logic [7:0] crc8;
    } framLogMsg_s;
endpackage: defines_pkg

/*
 * Файл "intefaces.svh" для проекта Interface Plane.
 *
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`ifndef INTERFACES_INCLUDE
    `define INTERFACES_INCLUDE

    interface generic_bus_io#(parameter ADR_WIDTH = 16, MOSI_WIDTH = 16, MISO_WIDTH = 16);
        logic re;
        logic we;
        logic valid;
        logic ready;
        logic [MOSI_WIDTH - 1:0] dat_mosi;
        logic [MISO_WIDTH - 1:0] dat_miso;
        logic [ADR_WIDTH - 1:0] adr;
        logic busy;
        
        modport master(input dat_miso, busy, output re, we, dat_mosi, adr);
        modport slave(input re, we, dat_mosi, adr, output dat_miso, busy);       
        modport slave_hndshk(input valid, we, dat_mosi, adr, output dat_miso, ready);
        modport slave_hndshk_wo_adr(input valid, we, dat_mosi, output dat_miso, ready);
    endinterface: generic_bus_io
    
    interface spi_io;
        logic ncs;
        logic sck;
        logic mosi;
        logic miso;

        modport master(input miso, output ncs, sck, mosi);
        modport slave(input ncs, sck, mosi, output miso);
    endinterface: spi_io
    
    interface wishbone_io;
        logic [15:0] wbm_adr;
        logic [31:0] wbm_dat;
        logic [3:0] wbm_sel;
        logic wbm_cyc;
        logic wbm_stb;
        logic wbm_we;
        logic [31:0] wbs_dat;
        logic wbs_ack;

        modport master(output wbm_adr, wbm_dat, wbm_sel, wbm_cyc, wbm_stb, wbm_we,
                       input wbs_dat, wbs_ack);
        modport slave(input wbm_adr, wbm_dat, wbm_sel, wbm_cyc, wbm_stb, wbm_we,
                      output wbs_dat, wbs_ack);
    endinterface: wishbone_io
    
    interface mcu_bus_io#(parameter ADR_WIDTH = 20, MOSI_WIDTH = 16, MISO_WIDTH = 16);
        logic [MOSI_WIDTH - 1:0] dat_mosi;
        logic [MISO_WIDTH - 1:0] dat_miso;
        logic [ADR_WIDTH - 1:0] adr;
        logic noe;
        logic nwe;		
                  
        modport slave(input adr, dat_mosi, noe, nwe, output dat_miso);
    endinterface: mcu_bus_io
`endif
`timescale 1ps/1ps
module MKO_Controller
    (
        nrst,
        clk,
        
        //txap,
        //txan,
        txinha,
        rxena,
        rxap,
        rxan,
        
        txbp,
        txbn,
        txinhb,
        rxenb,
        rxbp,
        rxbn,
        
        //rxen,
        //rxp,
        //rxn,

        wb_adr_i,
        wb_dat_i,	
        wb_stb_i,
        wb_cyc_i,
        wb_sel_i,
        wb_we_i ,	
        wb_dat_o,
        wb_ack_o
    );
    input nrst;
    input clk;
    
//    output txap;
//    output txan;
    output txinha;
    output rxena;
    input rxap;
    input rxan;

    output txbp;
    output txbn;
    output txinhb;
    output rxenb;
    input rxbp;
    input rxbn;
    
    input [15:0] wb_adr_i;
    input [31:0] wb_dat_i;
    input wb_stb_i;
    input wb_cyc_i;
    input [3:0] wb_sel_i;
    input wb_we_i;
    output [31:0]wb_dat_o;
    output wb_ack_o;

    localparam chsel = 1'b0;
    
    reg [31:0] fifo_data;
    reg ack_o;    
    reg fifo_wrq;
    reg [31:0] mko_packet_cnt;
    reg [8:0] fifo_data_cnt;
    reg [31:0] dat_o;
    reg test;
    reg [7:0] test_cnt;
    wire [9:0] fifo_wudw;
    assign wb_ack_o = ack_o; 
    assign wb_dat_o = dat_o;//{31'h00000000, fifo_wudw[9] || fifo_wudw[8]};
    
    wire txap_w;
    wire txan_w;
    wire txbp_w;
    wire txbn_w;
    
    assign txbp = (chsel) ? txbp_w : txap_w;
    assign txbn = (chsel) ? txbn_w : txan_w;
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                ack_o <= 1'b0;
                fifo_wrq <= 1'b0;
                fifo_data <= 32'd0;
                mko_packet_cnt <= 32'd0;
                fifo_data_cnt <= 9'd0;
                dat_o <= 32'd0;
                test <= 1'b0;
                test_cnt <= 8'd0;
            end
        else
            begin
                ack_o <= 1'b0;
                fifo_wrq <= 1'b0;
                if (fifo_data_cnt == 256)
                    begin
                        mko_packet_cnt <= mko_packet_cnt + 1'b1; 
                        fifo_data_cnt <= 9'd0;
                    end
                    
                if (wb_stb_i && wb_cyc_i && ~wb_ack_o)
                    begin
                        ack_o <= 1'b1;
                        if (wb_we_i)
                            begin
                                case (wb_adr_i[1:0])
                                    2'h0:
                                        begin
                                            if (~test)
                                                begin
                                                    fifo_data <= {wb_dat_i[7:0], wb_dat_i[15:8], wb_dat_i[23:16], wb_dat_i[31:24]};
                                                    fifo_wrq <= 1'b1;                                
                                                    fifo_data_cnt <= fifo_data_cnt + 1'b1;
                                                end
                                        end
                                    2'h1:
                                        begin
                                            test <= wb_dat_i[0];
                                        end
                                endcase
                            end
                        else
                            begin
                                case (wb_adr_i[1:0])
                                    2'h0:
                                        dat_o <= {31'h00000000, fifo_wudw[9] || (fifo_wudw[8:0] > 9'd256)};
                                    2'h1:
                                        dat_o <= mko_packet_cnt;
                                    2'h2:
                                        dat_o <= {23'd0, fifo_data_cnt};
                                    default:
                                        dat_o <= 32'd0;
                                endcase
                            end
                    end
                 
                if (test)
                    begin
                        if (~(fifo_wudw[9] || fifo_wudw[8]))
                            begin
                                fifo_data <= test_cnt;
                                fifo_wrq <= 1'b1;                                
                                test_cnt <= test_cnt + 1'b1;
                            end
                    end
            end

mk3_phy mk3_phy0
    (
        .TXAP(txap_w),
        .TXAN(txan_w),
        .TXINHA(txinha),
        .RXENA(rxena),
        .RXAP(rxap),
        .RXAN(rxan),
        
        .TXBP(txbp_w),
        .TXBN(txbn_w),
        .TXINHB(txinhb),
        .RXENB(rxenb),
        .RXBP(rxbp),
        .RXBN(rxbn),
        
        .RXEN(1'b0),
        .RXP(),
        .RXN(),
        
        .FIFO_WRQ(fifo_wrq),
        //.FIFO_DATA(fifo_data),
        .FIFO_DATA(fifo_data), // FIFO_DATA is big-endian 32-bit words
        .FIFO_WUDW(fifo_wudw),
        
        .CHSEL(1'b0),
        .DTYPE(1'b1),
        
        .nRST(nrst),
        .CLK(clk)
    );
endmodule
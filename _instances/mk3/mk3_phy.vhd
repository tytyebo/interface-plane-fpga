-- ## First Code : 20/06/2016               ##
-- ## First Compilation: 09:00 21-06-2016   ##
-- ## First Simulation: 11:32 21-06-2016    ##
-- ## Author     : Sergey Romanov           ##
-- ## Company    : NII VS @ SU              ##

-- �������� : ���������� ���� (��) ������������ ����������������� ���������� �
-- ��������� ���������� � ������� 7 �� ������� 4 ���� � 52070-2003.

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.numeric_std.all; 
use ieee.math_real.all;

LIBRARY WORK;
USE WORK.exinf_se_package.all;

ENTITY mk3_phy is 
    port(
        -- HI-1567 Phy Interface
        -- Bus A
        TXAP : OUT std_logic;
        TXAN : OUT std_logic;
        TXINHA : OUT std_logic; -- Transmit inhibit, bus A: =1 => BUSA=nBUSA=Z
        RXENA : OUT std_logic;    -- Receiver A Enable: =0 => RXAP=RXAN=0
        RXAP : IN std_logic := '0';
        RXAN : IN std_logic := '0';
        -- Bus B
        TXBP : OUT std_logic;
        TXBN : OUT std_logic;
        TXINHB : OUT std_logic; -- Transmit inhibit, bus B: =1 => BUSB=nBUSB=Z
        RXENB : OUT std_logic;    -- Receiver B Enable: =0 => RXBP=RXBN=0
        RXBP : IN std_logic := '0';
        RXBN : IN std_logic := '0';
        
        -- RX MuxOut Interface
        RXEN : IN std_logic := '0';
        RXP : OUT std_logic;
        RXN : OUT std_logic;
        
        -- MK3 TX-FIFO Write Interface
        FIFO_WRQ  :  IN std_logic;
        FIFO_DATA :  IN std_logic_vector(31 downto 0);
        FIFO_WUDW  : OUT std_logic_vector(9 downto 0);
        
        -- CONTROL Signals
        CHSEL : IN std_logic;    -- Select Bus: =0 => BUSA; =1 => BUSB
        DTYPE : IN std_logic;    -- Data Type: =0 => Service Info (code 00111); =1 => TM-Data (code 01001)
        
        -- System Signals (��������� �������)
        nRST : IN std_logic := '1';
        CLK : IN std_logic
    );
END mk3_phy;

ARCHITECTURE SEHDL of mk3_phy is

-- SECTION 1. COMPONENTS
component mk3_tx_fifo    -- ��������� ������ ������ FIFO
    PORT
    (
        aclr        : IN STD_LOGIC ;
        clock        : IN STD_LOGIC ;
        data        : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
        rdreq        : IN STD_LOGIC ;
        sclr        : IN STD_LOGIC ;
        wrreq        : IN STD_LOGIC ;
        full        : OUT STD_LOGIC ;
        q        : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
        usedw        : OUT STD_LOGIC_VECTOR (8 DOWNTO 0)
    );
end component;

-- SECTION 2. SIGNALS
CONSTANT MSB : POSITIVE := 15;
CONSTANT TIME_ACC_VALUE : POSITIVE := NATURAL(REAL(2**(MSB + 1)) / FSYS + 0.499);

-- ��3 TX-FIFO
signal fifo_clear : std_logic;
signal fifo_q : std_logic_vector(31 downto 0);
signal fifo_full : std_logic;
signal fifo_usedw : std_logic_vector(8 downto 0);
signal fifo_fusedw : std_logic_vector(9 downto 0);

-- MK3 I/O-Mux/Demux
signal TXP : std_logic;
signal TXN : std_logic;
signal TXINH : std_logic;

-- TX1 :: C������������ ����������� (TX Synchronizer)
signal tx_sinchro_acc : std_logic_vector(MSB downto 0);
signal tx_sinchro_p2m : std_logic;    -- 2 ���

-- TX2 :: ������������ ����������� (TX Serializer)
signal tx_serial_ack : std_logic;
signal tx_serial_odd : std_logic;
signal tx_serial_ogib : std_logic;
type TX_SERIAL_STATE_TYPE is (s0, s1, s2, s3, s4);
signal tx_serial_state : TX_SERIAL_STATE_TYPE;
signal tx_serial_nf : std_logic;
signal tx_serial_out : std_logic;
signal tx_serial_shift_reg : std_logic_vector(15 downto 0);
signal tx_serial_pary_bit : std_logic;
subtype TX_SERIAL_BCNT_TYPE is natural range 0 to 15;
signal tx_serial_bcnt : TX_SERIAL_BCNT_TYPE;
signal tx_serial_out_zm1 : std_logic;

-- FSM
type FSM_STATE_TYPE is (fsm_state_idle, fsm_state_txstart, fsm_state_txdata, fsm_state_txpause);
signal fsm_state : FSM_STATE_TYPE;
signal fsm_tx_ena : std_logic;
signal fsm_tx_ks : std_logic;
signal fsm_tx_data : std_logic_vector(15 downto 0);
signal fsm_tx_out_ena : std_logic;
signal fsm_dcnt : std_logic_vector(4 downto 0);
signal fsm_fifo_rrq : std_logic;

signal fsm_pa_cnt : std_logic_vector(3 downto 0);
    
BEGIN

-- ��3 TX-FIFO
fifo_clear <= not nRST;
fifo_buf : mk3_tx_fifo    -- ��������� ������ ������ FIFO
port map
(
    data        => FIFO_DATA,    --    : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
    wrreq        => FIFO_WRQ,    --    : IN STD_LOGIC ;
    rdreq        => fsm_fifo_rrq,    --    : IN STD_LOGIC ;
    q            => fifo_q,    --    : OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
    full        => fifo_full,    --        : OUT STD_LOGIC ;
    usedw        => fifo_usedw,    --    : OUT STD_LOGIC_VECTOR (8 DOWNTO 0)
    aclr        => fifo_clear,    --    : IN STD_LOGIC ;
    sclr        => fifo_clear,    --    : IN STD_LOGIC ;
    clock        => CLK    --    : IN STD_LOGIC ;
);
fifo_fusedw <= fifo_full & fifo_usedw;
FIFO_WUDW <= fifo_fusedw;

-- MK3 I/O-Mux/Demux
muxout : process(CHSEL, TXP, TXN, TXINH, RXEN, RXAP, RXAN, RXBP, RXBN)
begin
    if CHSEL = '0' then
        -- Bus A Select
        TXAP <= TXP;    -- : OUT std_logic;
        TXAN <= TXN;    -- : OUT std_logic;
        TXINHA <= TXINH;    -- : OUT std_logic; -- Transmit inhibit, bus A: =1 => BUSA=nBUSA=Z
        RXENA <= RXEN;    -- : OUT std_logic;    -- Receiver A Enable: =0 => RXAP=RXAN=0
        RXP <= RXAP;    -- : IN std_logic := '0';
        RXN <= RXAN;    -- : IN std_logic := '0';
        -- Bus B DeSelect
        TXBP <= '0';    -- : OUT std_logic;
        TXBN <= '0';    -- : OUT std_logic;
        TXINHB <= '1';    -- : OUT std_logic; -- Transmit inhibit, bus B: =1 => BUSB=nBUSB=Z
        RXENB <= '0';    -- : OUT std_logic;    -- Receiver B Enable: =0 => RXBP=RXBN=0
    else
        -- Bus A DeSelect
        TXAP <= '0';    -- : OUT std_logic;
        TXAN <= '0';    -- : OUT std_logic;
        TXINHA <= '1';    -- : OUT std_logic; -- Transmit inhibit, bus B: =1 => BUSB=nBUSB=Z
        RXENA <= '0';    -- : OUT std_logic;    -- Receiver B Enable: =0 => RXBP=RXBN=0    
        -- Bus B Select
        TXBP <= TXP;    -- : OUT std_logic;
        TXBN <= TXN;    -- : OUT std_logic;
        TXINHB <= TXINH;    -- : OUT std_logic; -- Transmit inhibit, bus A: =1 => BUSA=nBUSA=Z
        RXENB <= RXEN;    -- : OUT std_logic;    -- Receiver A Enable: =0 => RXAP=RXAN=0
        RXP <= RXBP;    -- : IN std_logic := '0';
        RXN <= RXBN;    -- : IN std_logic := '0';
    end if;
end process;

-- TX1 :: C������������ ����������� (TX Synchronizer)
tx_sinchro_proc: process(CLK, nRST)
begin
    if nRST = '0' then
        tx_sinchro_acc <= (others => '0');
    elsif CLK'event and (CLK = '1') then
        if fsm_tx_ena = '0' then
            tx_sinchro_acc <= (others => '0');
        else
            tx_sinchro_acc <= '0' & tx_sinchro_acc(MSB-1 downto 0) + TIME_ACC_VALUE;
        end if;
    end if;
end process;
tx_sinchro_p2m <= tx_sinchro_acc(MSB);    -- 2 ���

-- TX2 :: ������������ ����������� (TX Serializer)
tx_serial_proc: process(CLK, nRST)
begin
    if nRST = '0' then
        tx_serial_ack <= '0';
        tx_serial_odd <= '0';
        tx_serial_ogib <= '0';
        tx_serial_state <= s0;
        tx_serial_nf <= '0';
        tx_serial_out <= '0';
        tx_serial_shift_reg <= (others => '0');
        tx_serial_pary_bit <= '0';
        tx_serial_bcnt <= 0;
        tx_serial_out_zm1 <= '0';
    elsif CLK'event and (CLK = '1') then
        -- 1-takt strobes
        tx_serial_ack <= '0';
        -- TX Serial FSM
        if fsm_tx_ena = '0' then
            tx_serial_odd <= '0';
            tx_serial_ogib <= '0';
            tx_serial_state <= s0;
        elsif tx_sinchro_p2m = '1' then
            tx_serial_nf <= '0';    -- ���� ���������� ��������, ���� �� ���������
            if tx_serial_odd = '1' then
                -- ��������� ���� ����������� ���������������� ������
                if tx_serial_nf = '0' then
                    tx_serial_out <= not tx_serial_out;
                end if;
            else
                -- ������ ���� ����������� ���������������� ������ (�����)
                case tx_serial_state is
                when s0 =>
                    tx_serial_out <= fsm_tx_ks;
                    tx_serial_ogib <= fsm_tx_out_ena;
                    tx_serial_shift_reg <= fsm_tx_data;
                    tx_serial_nf <= '1';
                    tx_serial_pary_bit <= '1';
                    tx_serial_ack <= '1';
                    tx_serial_state <= s1;
                when s1 =>
                    tx_serial_state <= s2;
                when s2 =>
                    tx_serial_nf <= '1';
                    tx_serial_bcnt <= 15;
                    tx_serial_state <= s3;
                when s3 =>
                    tx_serial_out <= tx_serial_shift_reg(15);    -- ��������� �������� ��������� ������
                    tx_serial_pary_bit <= tx_serial_pary_bit xor tx_serial_shift_reg(15);
                    tx_serial_shift_reg(15 downto 1) <= tx_serial_shift_reg(14 downto 0);
                    if tx_serial_bcnt /= 0 then
                        tx_serial_bcnt <= tx_serial_bcnt - 1;
                    else
                        tx_serial_state <= s4;
                    end if;
                when s4 =>
                    tx_serial_out <= tx_serial_pary_bit;
                    tx_serial_state <= s0;
                end case;
            end if;
            tx_serial_odd <= not tx_serial_odd;        -- ����� ����
        end if;
        tx_serial_out_zm1 <= tx_serial_ogib and tx_serial_out;        -- ������������ ������ C��������� �������� ������������� �����������, ������������ �� 1 ����
    end if;
end process;

TXINH <= not tx_serial_ogib;
TXP <=     ((tx_serial_ogib and tx_serial_out)  or tx_serial_out_zm1);
TXN <= not ((tx_serial_ogib and tx_serial_out) and tx_serial_out_zm1);

fsm_proc: process(CLK, nRST)
begin
    if nRST = '0' then
        fsm_state <= fsm_state_idle;
        fsm_tx_ena <= '0';
        fsm_tx_ks <= '0';
        fsm_tx_data <= (others => '0');
        fsm_tx_out_ena <= '0';
        fsm_dcnt <= (others => '0');
        fsm_fifo_rrq <= '0';
        fsm_pa_cnt <= (others => '0');
    elsif CLK'event and (CLK = '1') then
        -- 1-takt strobes
        fsm_fifo_rrq <= '0';
        -- FSM
        case fsm_state is
        when fsm_state_idle =>
            fsm_tx_ena <= '0';
            if fifo_fusedw >= "00" & X"10" then -- >= 16
                fsm_state <= fsm_state_txstart;
            end if;
        when fsm_state_txstart =>
            fsm_tx_ks <= '1';
            if DTYPE = '1' then
                fsm_tx_data <= X"F8E0";
            else
                fsm_tx_data <= X"F920";
            end if;
            fsm_tx_data(8 downto 5) <= fsm_pa_cnt;
            fsm_pa_cnt <= fsm_pa_cnt + 1;
            if fsm_pa_cnt = X"F" then
                fsm_tx_data(9) <= '0';
            else
                fsm_tx_data(9) <= '1';

            end if;
            fsm_tx_ena <= '1';
            fsm_tx_out_ena <= '1';
            fsm_dcnt <= '1' & X"F";    -- 31
            fsm_state <= fsm_state_txdata;
        when fsm_state_txdata =>
            if tx_serial_ack = '1' then
                fsm_tx_ks <= '0';
                if fsm_dcnt(0) = '1' then
                    fsm_tx_data <= fifo_q(31 downto 16);
                else
                    fsm_tx_data <= fifo_q(15 downto  0);
                    fsm_fifo_rrq <= '1';
                end if;
                if fsm_dcnt /= 0 then
                    fsm_dcnt <= fsm_dcnt - 1;
                else
                    fsm_dcnt <= '1' & X"3";    -- 19
                    fsm_state <= fsm_state_txpause;
                end if;
            end if;
        when fsm_state_txpause =>
            if tx_serial_ack = '1' then
                fsm_tx_out_ena <= '0';
                if fsm_dcnt /= 0 then
                    fsm_dcnt <= fsm_dcnt - 1;
                else
                    fsm_state <= fsm_state_idle;
                end if;
            end if;
        end case;
    end if;
end process;

END SEHDL;
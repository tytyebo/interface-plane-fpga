`include "defines.svh"
`include "interfaces.svh"
module cfhl_ctrl(input bit clk, nrst,
                 wishbone_io.master wbm, //Wishbone master port (ATA controller connection)
                 wishbone_io.slave wbs,  //Wishbone slave port (controller programming and FIFO access)
                 input logic [1:0] card_detect,
                 input logic [27:0] start_cf_adr,
                 input logic log_full, ena);

    assign wbs.wbs_dat = data_output;

    // ATA regsiters address table
    localparam ATA_ALT_STATUS   = 8'h1E,
               ATA_COMMAND      = 8'h17,
               ATA_CYLINDER_H   = 8'h15,
               ATA_CYLINDER_L   = 8'h14,
               ATA_DATA         = 8'h10,
               ATA_CONTROL      = 8'h1E,
               ATA_HEAD         = 8'h16,
               ATA_ERROR        = 8'h11,
               ATA_FEATURES     = 8'h11,
               ATA_SECTOR       = 8'h13,
               ATA_COUNT        = 8'h12,
               ATA_STATUS       = 8'h17;
               
    localparam ATA_CTRL_REG_ADR         = 16'h0,
               ATA_PIO_REG_ADR          = 16'h2,
               ATA_PIO_FAST0_REG_ADR    = 16'h3,
               ATA_DMA0_REG_ADR         = 16'h5,
               ATA_DMA_DATA_ADR         = 16'hF;
    // Main state machine state names
    typedef enum bit [4:0] {ST_INIT_IDLE, ST_INIT_ATA_CTRL, ST_INIT_ATA_PIO, ST_INIT_ATA_PIO_FAST0, ST_INIT_ATA_DMA0, ST_GET_STATUS, ST_CHECK_STATUS, ST_IDLE, ST_SAVE_LOG,
                            ST_SEND_FEATURES, ST_SEND_HEAD, ST_SEND_CYL_HIGH, ST_SEND_CYL_LOW, ST_SEND_SECTOR, ST_SEND_COUNT, ST_SEND_COMMAND, ST_SEND_DATA_LOW,
                            ST_SEND_DATA_HIGH, ST_GET_DATA_LOW, ST_GET_DATA_HIGH, ST_ERROR} state_e;
	state_e state;
              
    localparam ATA_CTRL_REG = {16'd0, 16'b0000_0000_1011_0000},
               ATA_PIO_TIMING = {8'h01, 8'h00, 8'h07, 8'h01}, // PIO4 for 128 MHz -> PIO4 Teoc = T0 - T1 - T2 = 25;T4 = 10ns T2 = 70ns T1 = 25ns
               ATA_PIO_FAST0_TIMING = {8'h01, 8'h00, 8'h07, 8'h01}, // PIO4 for 128 MHz
               ATA_DMA0_TIMING = {8'h04, 8'h00, 8'h07, 8'h00}; // PIO4 for 128 MHz
    
    localparam SECTORS_COUNT        = `CF_SECTORS_BUFFER, 
               SECTOR_WORD_SIZE     = 9'd256; // В секторе 512 байт, в слове 16 бит
    
    localparam REG_CTRL_READY           = 12'h000,
               REG_FEATURES             = 12'h001,
               REG_SEC_CNT              = 12'h002,
               REG_SEC_NUM              = 12'h003,
               REG_CYLINDER             = 12'h004,
               REG_HEAD                 = 12'h005,
               REG_ATA_CMD              = 12'h006,
               REG_CF_CMD               = 12'h007,
               REG_RX_FIFO_STATUS       = 12'h008,
               REG_RX_FIFO_IN           = 12'h008,
               REG_TX_FIFO_Q            = 12'h009,
               REG_TX_FIFO_STATUS       = 12'h00A,
               REG_ATA_ERROR            = 12'h00B,
               REG_TX_FIFO_CTRL         = 12'h00C,
               REG_CF_SIZE              = 12'h00D,
               REG_CARD_DETECT          = 12'h00E;
               
    wire ata_status_busy = ata_status[7];
    wire ata_status_rdy  = ata_status[6];
    //wire ata_status_dwf  = ata_status[5]; //Write fault - for future use
    wire ata_status_dsc  = ata_status[4]; //CompactFlash Storage Card is ready
    wire ata_status_drq  = ata_status[3]; //Data request
    //wire ata_status_corr = ata_status[2]; //correctable error - for future use
    wire ata_status_idx  = ata_status[1]; //always zero
    wire ata_status_err  = ata_status[0]; //Error. See error register for further details

    logic wbm_cyc;
    logic wbm_stb;
    logic wbm_we;
    logic [15:0] wbm_addr;
    logic [3:0] wbm_sel;
    logic [31:0] wbm_data;
    logic [31:0] data_i;
    logic wbm_done;
    logic [1:0] wbm_state;
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbm_cyc <= 1'b0;
            wbm_stb <= 1'b0;
            wbm_we  <= 1'b0;
            wbm_addr <= 16'h0;
            wbm_sel <= 4'h0;
            wbm_done <= 1'b0;
            wbm_data <= 32'h0000;
            wbm_state <= 2'b00;
        end 
        else begin
            wbm_done <= 1'b0;
            unique case (wbm_state)
                2'b00: begin
                    unique case ({wbm_write, wbm_read})
                        2'b10: begin
                            wbm_addr <= addr; 
                            wbm_cyc <= 1'b1;
                            wbm_stb <= 1'b1;
                            wbm_we  <= 1'b1;
                            wbm_sel <= sel;
                            wbm_data <= data;
                            wbm_state <= 2'b01;
                        end 
                        2'b01: begin
                            wbm_addr <= addr; 
                            wbm_cyc <= 1'b1;
                            wbm_stb <= 1'b1;
                            wbm_we  <= 1'b0;
                            wbm_sel <= sel;
                            wbm_state <= 2'b01;
                        end 
                        default: begin 
                            wbm_cyc <= 1'b0;
                            wbm_stb <= 1'b0;
                            wbm_we  <= 1'b0;
                            wbm_sel <= 4'h0;
                        end 
                    endcase
                end
                2'b01: begin
                    if (wbm.wbs_ack) begin
                        wbm_done <= 1'b1;
                        wbm_cyc <= 1'b0;
                        wbm_stb <= 1'b0;
                        wbm_we  <= 1'b0;
                        wbm_sel <= 4'h0;
                        data_i <= wbm.wbs_dat;
                        wbm_state <= 2'b10;
                    end
                end
                2'b10: begin
                    wbm_state <= 2'b00;
                end
                default: begin
                    wbm_state <= 2'b00;
                end
            endcase
        end
    end
    
    logic [15:0] addr;
    logic [3:0] sel;   
    logic [31:0] data;
    logic wbm_write;
    logic wbm_read;
    logic init_done;

    logic [7:0]ata_status;
    
    logic [27:0] auto_lba_addr;
    logic rx_fifo_rrq;
    logic last_sector;
    logic tx_fifo_wrq;
    
    assign wbm.wbm_adr = wbm_addr;
    assign wbm.wbm_dat = wbm_data;
    assign wbm.wbm_cyc = wbm_cyc;
    assign wbm.wbm_stb = wbm_stb;
    assign wbm.wbm_sel = wbm_sel;
    assign wbm.wbm_we = wbm_we;
	
    logic cmd_error;
    logic cmd_done;
    logic [7:0] wr_rd_word_cnt;
    logic [7:0] wr_rd_sector_cnt;
    logic [31:0] tx_fifo_data;
    logic [15:0] error_cnt;
    logic [7:0] ata_error;
    logic cf_full, cf_adr_locked;
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            sel <= 4'h0;
            wbm_write <= 1'b0;
            wbm_read <= 1'b0;
            state <= ST_INIT_IDLE;
            data <= 32'h0000;
            init_done <= 1'b0;
            auto_lba_addr <= 28'd256;
            rx_fifo_rrq <= 1'b0;
            last_sector <= 1'b0;
            cmd_error <= 1'b0;
            cmd_done <= 1'b0;
            tx_fifo_wrq <= 1'b0;
            wr_rd_word_cnt <= 8'd0;
            wr_rd_sector_cnt <= 8'd0;
            tx_fifo_data <= 32'd0;
            error_cnt <= 16'd0;
            ata_error <= 8'd0;
            cf_full <= 1'b0;
            cf_adr_locked <= 1'b0;
        end
        else begin 
            wbm_write <= 1'b0;
            wbm_read <= 1'b0;
            rx_fifo_rrq <= 1'b0;
            cmd_error <= 1'b0;
            cmd_done <= 1'b0;
            tx_fifo_wrq <= 1'b0;
            unique case (state)//normal functioning	
                ST_INIT_IDLE: begin
                    if (card_present) begin                            
                        state <= ST_INIT_ATA_CTRL;
                    end
                end
                ST_INIT_ATA_CTRL: begin
                    sel <= 4'hF;
                    wbm_write <= 1'b1;
                    addr <= ATA_CTRL_REG_ADR;
                    data <= ATA_CTRL_REG;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_INIT_ATA_PIO;
                    end
                end
                ST_INIT_ATA_PIO: begin
                    sel <= 4'hF;
                    wbm_write <= 1'b1;
                    addr <= ATA_PIO_REG_ADR;
                    data <= ATA_PIO_TIMING;
                    if (wbm_done)
                        begin
                            wbm_write <= 1'b0;
                            state <= ST_INIT_ATA_PIO_FAST0;
                        end
                end
                ST_INIT_ATA_PIO_FAST0: begin
                    sel <= 4'hF;
                    wbm_write <= 1'b1;
                    addr <= ATA_PIO_FAST0_REG_ADR;
                    data <= ATA_PIO_FAST0_TIMING;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_INIT_ATA_DMA0;
                    end
                end
                ST_INIT_ATA_DMA0: begin
                    sel <= 4'hF;
                    wbm_write <= 1'b1;
                    addr <= ATA_DMA0_REG_ADR;
                    data <= ATA_DMA0_TIMING;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_GET_STATUS;
                    end
                end
                ST_GET_STATUS: begin
                    sel <= 4'h3; 
                    wbm_read <= 1'b1;
                    addr <= ATA_STATUS;
                    if (wbm_done) begin
                        wbm_read <= 1'b0;
                        state <= ST_CHECK_STATUS;
                        ata_status <= data_i[7:0];
                    end
                end
                ST_CHECK_STATUS: begin
                    if (!init_done) begin
                        if (!ata_status_busy && ata_status_rdy && ata_status_dsc && !ata_status_idx) begin
                            state <= ST_IDLE;
                            init_done <= 1'b1;
                        end
                        else begin
                            state <= ST_GET_STATUS;
                        end
                    end
                    else begin
                        if (ata_status_busy) begin
                            state <= ST_GET_STATUS;
                        end
                        else begin
                            if (last_sector) begin
                                if (!ata_status_busy && ata_status_rdy && ata_status_dsc && !ata_status_idx) begin
                                    last_sector <= 1'b0;
                                    if (cmd_auto_write) begin
                                        if (cf_size == auto_lba_addr) begin
                                            cf_full <= 1'b1;
                                        end                               
                                        auto_lba_addr <= auto_lba_addr + SECTORS_COUNT;
                                        wr_rd_sector_cnt <= sector_count - 1'b1;
                                        // state <= ST_SEND_FEATURES;
                                        state <= ST_SAVE_LOG;
                                    end
                                    else begin
                                        cmd_done <= 1'b1;
                                        state <= ST_IDLE;
                                    end
                                end
                                else begin
                                    state <= ST_GET_STATUS;
                                end
                            end
                            else begin
                                if (!ata_status_drq) begin
                                    state <= ST_GET_STATUS;
                                end
                                else begin
                                    case ({cmd_auto_write, cmd_write, cmd_read})
                                        3'b001:
                                            state <= ST_GET_DATA_LOW;
                                        3'b010:
                                            state <= ST_SEND_DATA_LOW;                                                            
                                        3'b100:
                                            state <= ST_SEND_DATA_LOW; 
                                        default: begin
                                            state <= ST_IDLE;
                                            cmd_error <= 1'b1;
                                        end
                                    endcase
                                end             
                                if (ata_status_err) begin
                                    state <= ST_ERROR;
                                end
                            end
                        end
                    end
                end
                ST_IDLE: begin
                    if (ena) begin
                        if (!cf_adr_locked) begin
                            auto_lba_addr <= start_cf_adr;
                            cf_adr_locked <= 1'b1;
                        end
                        if (cmd_full && !cmd_done) begin
                            if (cmd_fifo_clear) begin
                                state <= ST_IDLE; 
                                cmd_done <= 1'b1;
                            end
                            else begin
                                wr_rd_sector_cnt <= sector_count - 1'b1; 
                                state <= ST_SEND_FEATURES; 
                            end
                        end
                    end
                end
                ST_SAVE_LOG: begin
                    sel <= 4'hf;
                    wbm_write <= 1'b1;
                    addr <= 16'h9002;
                    data <= {4'd0, auto_lba_addr};
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_SEND_FEATURES;
                    end
                end
                ST_SEND_FEATURES: begin
                    if (!cf_full) begin
                        wbm_write <= 1'b1;
                        addr <= ATA_FEATURES;
                        data[31:0] <= {24'd0, features};
                        sel <= 4'h3;
                        if (cmd_auto_write && ({ rx_fifo_full, rx_fifo_usedw } < 14'd4096)) begin
                            wbm_write <= 1'b0;
                        end
                        if (wbm_done) begin
                            wbm_write <= 1'b0;
                            state <= ST_SEND_HEAD;
                        end
                    end
                    else begin
                        state <= ST_IDLE;
                    end
                end
                ST_SEND_HEAD: begin			
                    wbm_write <= 1'b1;
                    addr <= ATA_HEAD;
                    data <= {24'd0, head};
                    sel <= 4'h3;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_SEND_CYL_HIGH;
                    end
                end
                ST_SEND_CYL_HIGH: begin
                    wbm_write <= 1'b1;
                    addr <= ATA_CYLINDER_H;
                    data <= {24'd0, cylinder[15:8]};
                    sel <= 4'h3;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_SEND_CYL_LOW;
                    end
                end
                ST_SEND_CYL_LOW: begin			
                    wbm_write <= 1'b1;
                    addr <= ATA_CYLINDER_L;
                    data <= {24'd0, cylinder[7:0]};
                    sel <= 4'h3;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_SEND_SECTOR;
                    end
                end
                ST_SEND_SECTOR: begin
                    wbm_write <= 1'b1;
                    addr <= ATA_SECTOR;
                    data <= {24'd0, sector[7:0]};
                    sel <= 4'h3;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_SEND_COUNT;
                    end
                end
                ST_SEND_COUNT: begin
                    wbm_write <= 1'b1;
                    addr <= ATA_COUNT;
                    data <= {24'd0, sector_count[7:0]};
                    sel <= 4'h3;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_SEND_COMMAND;
                    end
                end
                ST_SEND_COMMAND: begin
                    addr <= ATA_COMMAND;
                    data <= {24'd0, ata_command};
                    sel <= 4'h3;
                    wbm_write <= 1'b1;
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        state <= ST_GET_STATUS;
                    end
                end
                ST_GET_DATA_LOW: begin  
                    addr <= ATA_DATA;
                    sel <= 4'h3;
                    wbm_read <= 1'b1;

                    if (wbm_done) begin
                        wbm_read <= 1'b0;
                        tx_fifo_data[15:0] <= data_i[15:0];
                        wr_rd_word_cnt <= wr_rd_word_cnt + 1'b1;
                        state <= ST_GET_DATA_HIGH;
                    end
                end          
                ST_GET_DATA_HIGH: begin  
                    if (!tx_fifo_full) begin
                        addr <= ATA_DATA;
                        sel <= 4'h3;
                        wbm_read <= 1'b1;
                    end
                            
                    if (wbm_done) begin
                        wbm_read <= 1'b0;
                        tx_fifo_data[31:16] <= data_i[15:0];
                        tx_fifo_wrq <= 1'b1;
                        wr_rd_word_cnt <= wr_rd_word_cnt + 1'b1;
                        
                        if (wr_rd_word_cnt == (SECTOR_WORD_SIZE - 1)) begin
                            if (wr_rd_sector_cnt == 0) begin
                                last_sector <= 1'b1;
                            end
                            else begin
                                wr_rd_sector_cnt <= wr_rd_sector_cnt - 1'b1;
                            end
                            state <= ST_GET_STATUS;							
                        end 
                        else begin
                            state <= ST_GET_DATA_LOW;
                        end
                    end
                end
                ST_SEND_DATA_LOW: begin
                    if (!rx_fifo_empty && !rx_fifo_rrq) begin
                        // Запись во флеш глючит при второй и последующих записях, потому что я сначала выдаю команду на  запись,
                        // а сами данные начинаю грузить сильно потом. Нужно передавать команду и параметры в регистры по условию,
                        // что в RX фифо есть хотя бы одно слово.
                        addr <= ATA_DATA;
                        data <= {16'd0, rx_fifo_q[15:0]};
                        sel <= 4'h3;
                        wbm_write <= 1'b1;
                    end
                            
                    if (wbm_done) begin
                        wbm_write <= 1'b0;
                        wr_rd_word_cnt <= wr_rd_word_cnt + 1'b1;
                        state <= ST_SEND_DATA_HIGH;
                    end
                end
                ST_SEND_DATA_HIGH: begin
                    addr <= ATA_DATA;
                    data <= {16'd0, rx_fifo_q[31:16]};
                    sel <= 4'h3;
                    wbm_write <= 1'b1;
                            
                    if (wbm_done) begin
                        rx_fifo_rrq <= 1'b1;
                        wbm_write <= 1'b0;
                        wr_rd_word_cnt <= wr_rd_word_cnt + 1'b1;
                        if (wr_rd_word_cnt == (SECTOR_WORD_SIZE - 1)) begin
                            if (wr_rd_sector_cnt == 0) begin
                                last_sector <= 1'b1;
                            end
                            else begin
                                wr_rd_sector_cnt <= wr_rd_sector_cnt - 1'b1;
                            end
                            state <= ST_GET_STATUS;
                        end 
                        else begin
                            state <= ST_SEND_DATA_LOW;
                        end
                    end
                end
                ST_ERROR: begin
                    wbm_read <= 1'b1;
                    sel <= 4'h3;
                    addr <= ATA_ERROR;
                    if (wbm_done) begin			                                
                        wbm_read <= 1'b0;
                        ata_error <= data_i[7:0];
                        error_cnt <= error_cnt + 1'b1;
                        state <= ST_IDLE;
                    end			
                end                        
            endcase
        end
    end

    // CF card, when inserted, pulls CD pins to gnd
    logic card_present;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            card_present <= 1'b0;
        end
        else begin
            card_present <= ~|cd_stable;
        end
    end
        
    wire [7:0] features;
    wire [7:0] head;
    wire [15:0] cylinder;
    wire [7:0] sector;
    wire [7:0] sector_count;
    wire [7:0] ata_command;
    logic [7:0] command;
    
    // Регистры reg_* содержать данные для следующей команды
    logic [7:0] reg_features;
    logic [7:0] reg_head;
    logic [15:0] reg_cylinder;
    logic [7:0] reg_sector;
    logic [7:0] reg_sector_count;
    logic [7:0] reg_ata_command;
    
    // В регистрах cmd_* содержатся буферизованные данные для выполняемой команды
    logic [7:0] cmd_features;
    logic [7:0] cmd_head;
    logic [15:0] cmd_cylinder;
    logic [7:0] cmd_sector;
    logic [7:0] cmd_sector_count;
    logic [7:0] cmd_ata_command;
    
    assign features = (cmd_auto_write) ? 8'd0 : cmd_features;
    assign head = (cmd_auto_write) ? {4'hE, auto_lba_addr[27:24]} : cmd_head;
    assign cylinder = (cmd_auto_write) ? auto_lba_addr[23:8] : cmd_cylinder;
    assign sector = (cmd_auto_write) ? auto_lba_addr[7:0] : cmd_sector;
    assign sector_count = (cmd_auto_write) ? SECTORS_COUNT : cmd_sector_count;
    assign ata_command = (cmd_auto_write) ? 8'h30 : cmd_ata_command;

    logic is_cmd;
    logic cmd_full;
    logic cmd_auto_write;
    logic cmd_write;
    logic cmd_read;
    logic cmd_fifo_clear;
    logic rx_fifo_wrq;
    logic tx_fifo_rrq;
    logic [31:0] data_output;
    logic tx_fifo_clear;
    logic [27:0] cf_size;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            reg_features <= 8'h00;
            reg_sector_count <= 8'h00;
            reg_sector <= 8'h00;
            reg_cylinder <= 16'h00;
            reg_head <= 8'h00;
            reg_ata_command <= 8'h00;
            command <= 8'h00;
            is_cmd <= 1'b0;
            cmd_full <= 1'b0;
            cmd_auto_write <= 1'b0;
            cmd_write <= 1'b0;
            cmd_read <= 1'b0;
            cmd_fifo_clear <= 1'b0;
            cmd_features <= 8'h00;
            cmd_sector_count <= 8'h00;
            cmd_sector <= 8'h00;
            cmd_cylinder <= 16'h00;
            cmd_head <= 8'h00;
            cmd_ata_command <= 8'h00;
            tx_fifo_rrq <= 1'b0;
            data_output <= 32'd0;
            tx_fifo_clear <= 1'b0;
            cf_size <= 28'd0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            rx_fifo_wrq <= 1'b0;
            tx_fifo_rrq <= 1'b0;
                    
            if (wbs.wbm_cyc && wbs.wbm_stb && !wbs.wbs_ack) begin         
                if (wbs.wbm_we) begin
                    unique case (wbs.wbm_adr[11:0])
                        REG_FEATURES: begin
                            if (wbs.wbm_sel[0]) begin
                                reg_features <= wbs.wbm_dat[7:0];
                            end
                        end
                        REG_SEC_CNT: begin
                            if (wbs.wbm_sel[0]) begin
                                reg_sector_count <= wbs.wbm_dat[7:0];
                            end
                        end
                        REG_SEC_NUM: begin
                            if (wbs.wbm_sel[0]) begin
                                reg_sector <= wbs.wbm_dat[7:0];
                            end
                        end
                        REG_CYLINDER: begin
                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1]) begin
                                reg_cylinder <= wbs.wbm_dat[15:0];
                            end
                        end
                        REG_HEAD: begin
                            if (wbs.wbm_sel[0]) begin
                                reg_head <= wbs.wbm_dat[7:0];
                            end
                        end
                        REG_ATA_CMD: begin 
                            if (wbs.wbm_sel[0]) begin
                                reg_ata_command <= wbs.wbm_dat[7:0];
                            end
                        end
                        REG_CF_CMD: begin
                            if (wbs.wbm_sel[0]) begin
                                command <= wbs.wbm_dat[7:0]; 
                                is_cmd <= 1'b1;  
                            end
                        end
                        REG_RX_FIFO_IN: begin
                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1] & wbs.wbm_sel[2] & wbs.wbm_sel[3]) begin
                                rx_fifo_wrq <= 1'b1;
                            end
                        end
                        REG_TX_FIFO_CTRL: begin
                            if (wbs.wbm_sel[0]) begin
                                tx_fifo_clear <= wbs.wbm_dat[0];
                            end
                        end
                        REG_CF_SIZE: begin
                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1] & wbs.wbm_sel[2] & wbs.wbm_sel[3]) begin
                                cf_size <= wbs.wbm_dat[27:0];
                            end
                        end
                    endcase
                end
                else begin
                                    case (wbs.wbm_adr[3:0])
                                        REG_FEATURES:
                                            if (wbs.wbm_sel[0])
                                                data_output <= {24'd0, features};
                                            else
                                                data_output <= 32'd0;
                                        REG_SEC_CNT:
                                            if (wbs.wbm_sel[0])
                                                data_output <= {24'd0, sector_count};
                                            else
                                                data_output <= 32'd0;
                                        REG_SEC_NUM:
                                            if (wbs.wbm_sel[0])
                                                data_output <= {24'd0, sector};
                                            else
                                                data_output <= 32'd0;
                                        REG_CYLINDER:
                                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1])
                                                data_output <= {16'd0, cylinder};
                                            else
                                                data_output <= 32'd0;
                                        REG_HEAD:
                                            if (wbs.wbm_sel[0])
                                                data_output <= {24'd0, head};
                                            else
                                                data_output <= 32'd0;
                                        REG_ATA_CMD:
                                            if (wbs.wbm_sel[0])
                                                data_output <= {24'd0, ata_command};
                                            else
                                                data_output <= 32'd0;
                                        REG_CF_CMD:
                                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1])
                                                data_output <= {23'd0, is_cmd, command};
                                            else
                                                data_output <= 32'd0;
                                        REG_RX_FIFO_STATUS:
                                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1])
                                                data_output <= {16'd0, cf_full, rx_fifo_empty, rx_fifo_full, rx_fifo_usedw};
                                            else
                                                data_output <= 32'd0;
                                        REG_TX_FIFO_Q:
                                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1] & wbs.wbm_sel[2] & wbs.wbm_sel[3])
                                                begin
                                                    data_output <= tx_fifo_q;
                                                    tx_fifo_rrq <= 1'b1;
                                                end
                                            else
                                                data_output <= 32'd0;
                                        REG_TX_FIFO_STATUS:
                                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1])
                                                data_output <= {16'd0, 5'd0, tx_fifo_empty, tx_fifo_full ,tx_fifo_usedw};
                                            else
                                                data_output <= 32'd0;
                                        REG_ATA_ERROR:
                                            if (wbs.wbm_sel[0] & wbs.wbm_sel[1] & wbs.wbm_sel[2] & wbs.wbm_sel[3])
                                                data_output <= {error_cnt, ata_error, ata_status};
                                            else
                                                data_output <= 32'd0;
                                        REG_CTRL_READY:
                                            if (wbs.wbm_sel[0])
                                                data_output <= (state == ST_IDLE);
                                            else
                                                data_output <= 32'd0;
                                        REG_CARD_DETECT:
                                            if (wbs.wbm_sel[0])
                                                data_output <= card_present;
                                            else
                                                data_output <= 32'd0;
                                        default:
                                            data_output <= 32'd0;
                                    endcase
                                end
                            wbs.wbs_ack <= 1'b1;    
                        end
                        
                    if (cmd_done | cmd_error) begin
                        cmd_full <= 1'b0;
                        cmd_auto_write <= 1'b0;
                        cmd_write <= 1'b0;
                        cmd_read <= 1'b0;
                        cmd_fifo_clear <= 1'b0;
                    end
                        
                    if (is_cmd & !cmd_full) begin
                        cmd_full <= command[0];
                        cmd_auto_write <= command[1];
                        cmd_write <= command[2];
                        cmd_read <= command[3];
                        cmd_fifo_clear <= command[4];  
                        is_cmd <= 1'b0;
                        
                        cmd_features <= reg_features;
                        cmd_sector_count <= reg_sector_count;
                        cmd_sector <= reg_sector;
                        cmd_cylinder <= reg_cylinder;
                        cmd_head <= reg_head;
                        cmd_ata_command <= reg_ata_command;
                    end
                end
        end
    wire [1:0] cd_stable;
    metastable_chain
        #(
            .CHAIN_DEPTH(16),
            .BUS_WIDTH(2)
        ) 
    card_detect_chain        
        (
            .clk(clk),
            .nrst(1'b1),
            .din(card_detect),
            .dout(cd_stable)
        );
    
    wire rx_fifo_empty;
    wire [31:0] rx_fifo_q;
    wire [12:0] rx_fifo_usedw;
    wire rx_fifo_full;
    cfhl_rx_fifo rx_fifo
        (
            .aclr(~nrst),
            .clock(clk),
            .data(wbs.wbm_dat[31:0]),
            .rdreq(rx_fifo_rrq),
            .wrreq(rx_fifo_wrq),
            .empty(rx_fifo_empty),
            .full(rx_fifo_full),
            .q(rx_fifo_q),
            .usedw(rx_fifo_usedw)
        );
    
    wire [31:0] tx_fifo_q;
    wire tx_fifo_full;
    wire [8:0] tx_fifo_usedw;
    wire tx_fifo_empty;
    
    cfhl_tx_fifo tx_fifo
        (
            .aclr(~nrst | tx_fifo_clear),
            .clock(clk),
            .data(tx_fifo_data),
            .rdreq(tx_fifo_rrq),
            .wrreq(tx_fifo_wrq),
            .empty(tx_fifo_empty),
            .full(tx_fifo_full),
            .q(tx_fifo_q),
            .usedw(tx_fifo_usedw)
        );

endmodule: cfhl_ctrl

module turning
    #(
        parameter DATA_WIDTH=10
    )
    (
        input [DATA_WIDTH-1:0] in,
        output [DATA_WIDTH-1:0] out
    );
    genvar i;
    generate
        for (i = 0; i < DATA_WIDTH; i = i + 1)
            begin : generate_turn
                assign out[DATA_WIDTH-1 - i] = in[i];
            end
    endgenerate
endmodule

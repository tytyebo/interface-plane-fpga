module metastable_chain
    #(
        parameter CHAIN_DEPTH = 1,
        parameter BUS_WIDTH = 1        
    )
    (
        clk,
        nrst,
        din,
        dout
    );
    input clk;
    input nrst;
    input [BUS_WIDTH - 1:0] din;
    output [BUS_WIDTH - 1:0] dout;
    
    genvar i;
    generate
        for (i = 0; i <= BUS_WIDTH - 1; i = i + 1)
            begin : generate_chain
                metastable_primitive #(CHAIN_DEPTH) metastable_primitive_inst
                    (
                        .clk(clk),
                        .nrst(nrst),
                        .din(din[i]),
                        .dout(dout[i])
                    );
            end
    endgenerate
endmodule

module metastable_primitive
    #(
        parameter CHAIN_DEPTH = 1
    )
    (
        clk,
        nrst,
        din,
        dout
    );
    input clk;
    input nrst;
    input din;
    output dout;
    
    generate
        if (CHAIN_DEPTH >= 2)
            begin : chain
                reg [CHAIN_DEPTH - 1 : 0] chain;
                assign dout = chain[CHAIN_DEPTH - 1];
                always @(posedge clk or negedge nrst)
                    if (~nrst)
                        chain[CHAIN_DEPTH - 1 : 0] <= {CHAIN_DEPTH{1'b0}};
                    else
                        chain <= {chain[CHAIN_DEPTH - 2 : 0], din};
            end
        else
            begin : single
                reg chain;
                assign dout = chain;
                always @(posedge clk or negedge nrst)
                    if (~nrst)
                        chain <= 1'b0;
                    else
                        chain <= din;            
            end
    endgenerate   
endmodule          
                
                
/*
 * Модуль "ird_ip_fm25l_phy" для проекта Interface_plane.
 * Контроллер физического уровня микросхемы флэш-памяти FM25L16B.
 * Выход "next_byte" при записи запрашивает следующие данные на порте data_bus.mosi,
 * а при чтении информирует о готовности прочитанных данных.
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "interfaces.svh"
module ird_ip_fm25l_phy(input bit clk, nrst, // clk is 128 MHz
                         // Fifo map-interface
                         input logic [7:0] operation,
                         input logic [8:0] burst_len, // The maximum value is 256.
                         output logic next_byte,
                         generic_bus_io.slave data_bus,
                         spi_io.master spi);
   
    logic rise, down;
    logic [2:0] sck_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            rise <= 1'b0;
            down <= 1'b0;
            sck_cnt <= '0;
        end
        else begin
            rise <= 1'b0;
            down <= 1'b0;
            sck_cnt <= sck_cnt + 1'b1;
            if (sck_cnt == 3'd3) begin
                rise <= 1'b1;
            end
            if (sck_cnt == 3'd7) begin
                down <= 1'b1;
            end
        end
    end
    
    logic clk_ena;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            spi.sck <= 1'b0;
        end
        else begin
            if (clk_ena) begin
                if (rise) begin
                    spi.sck <= 1'b1;
                end
                if (down) begin
                    spi.sck <= 1'b0;
                end
            end
        end
    end
    
    typedef enum bit [2:0] {ST_IDLE, ST_OPERATION, ST_ADR, ST_GET_DUMMY, ST_GET_DATA, ST_END, ST_SET_DATA} state_e;
    state_e state; 

    logic rw;
    logic [7:0] cmd, data;
    logic [8:0] len;
    logic [23:0] adr;
    logic [4:0] bit_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            spi.ncs <= 1'b0;
			spi.mosi <= 1'b0;
            data_bus.busy <= 1'b0;
            data_bus.dat_miso <= '0;
            rw <= 1'b0;
            cmd <= '0;
            adr <= '0;
            bit_cnt <= '0;
            clk_ena <= 1'b0;
            data <= '0;
            len <= '0;
            next_byte <= 1'b0;
        end
        
        else begin
            next_byte <= 1'b0;
            unique case (state)
                ST_IDLE: begin
                    spi.ncs <= 1'b1;
                    if (data_bus.we) begin
                        rw <= 1'b1;
                        data_bus.busy <= 1'b1;
						// next_byte <= 1'b1;
                        state <= ST_OPERATION;
                    end                        
                    if (data_bus.re) begin
                        rw <= 1'b0;
                        data_bus.busy <= 1'b1;
						// next_byte <= 1'b1;
                        state <= ST_OPERATION;
                    end
                    cmd <= operation;
                    adr <= data_bus.adr;
                    // data <= data_bus.dat_mosi;
                    len <= (burst_len < 9'd256) ? burst_len : 9'd256;
                    bit_cnt <= 5'd8;
                end
                ST_OPERATION: begin
                    if (down) begin
                        spi.ncs <= 1'b0;
                        clk_ena <= 1'b1;
                        bit_cnt <= bit_cnt - 1'b1;
                        spi.mosi <= cmd[bit_cnt - 1'b1];
                        if (bit_cnt == 5'd0) begin
                            if (rw && len == 9'd0) begin // вероятно тут нужно убрать burst_len == 9'd0 иначе не будет проходить записи команд типа WRITE_ENABLE
                                state <= ST_END;
                            end
                            else begin
                                state <= ST_ADR;
                                bit_cnt <= 5'd15;
                            end
                        end
                    end
                end
                ST_ADR: begin
                    if (down) begin
                        bit_cnt <= bit_cnt - 1'b1;
                        spi.mosi <= adr[bit_cnt - 1'b1];
                    end
                    if (bit_cnt == 5'd0) begin
                        if (!rw) begin
                            state <= ST_GET_DUMMY;
                            bit_cnt <= 5'd7;
                        end
                        else begin
                            state <= ST_SET_DATA;
                            bit_cnt <= 5'd8;
                            data <= data_bus.dat_mosi;
                        end

                    end
                end
				ST_GET_DUMMY: begin
					if (rise) begin
						state <= ST_GET_DATA;
					end
				end
                ST_GET_DATA: begin
                    if (rise) begin
                        bit_cnt <= bit_cnt - 1'b1;
                        data_bus.dat_miso[bit_cnt] <= spi.miso;
                        if (bit_cnt == 5'd0) begin
                            state <= ST_END;
                            bit_cnt <= 5'd7;
							next_byte <= 1'b1;
                        end
                    end
                end
                ST_END: begin
					data <= data_bus.dat_mosi;
                    if (!rw) begin
                        unique case (len)
                            9'd0: begin
                                if (operation == cmd && data_bus.adr == adr && data_bus.re) begin
                                    adr <= adr + 1'b1;
                                    state <= ST_GET_DATA;
                                    spi.ncs <= 1'b0;
                                end
                                else begin
                                    state <= ST_IDLE;
                                    data_bus.busy <= 1'b0;
                                    spi.ncs <= 1'b1;
                                end
                            end
                            9'd1: begin
                                state <= ST_IDLE;
                                data_bus.busy <= 1'b0;
                                spi.ncs <= 1'b1;
                            end
                            default: begin
                                len <= len - 1'b1;
                                state <= ST_GET_DATA;
                                bit_cnt <= 5'd7;
                                spi.ncs <= 1'b0;
                            end
                        endcase
                    end
                    else begin
                        unique case (len)
                            9'd0: begin
                                state <= ST_IDLE;
                                data_bus.busy <= 1'b0;
                                spi.ncs <= 1'b0;
                            end
                            9'd1: begin
                                if (rise) begin
                                    state <= ST_IDLE;
                                    data_bus.busy <= 1'b0;
                                    spi.ncs <= 1'b0;
                                end
                            end
                            default: begin
                                len <= len - 1'b1;
                                state <= ST_SET_DATA;
                                bit_cnt <= 5'd8;
                                spi.ncs <= 1'b0;
                            end
                        endcase
                    end
                end
                ST_SET_DATA: begin
                    if (down) begin
                        bit_cnt <= bit_cnt - 1'b1;
                        spi.mosi <= data[bit_cnt - 1'b1];
                        if (bit_cnt == 5'd2) begin
                            next_byte <= 1'b1;
                        end
                        if (bit_cnt == 5'd1) begin

                            state <= ST_END;
                        end
                    end
                end
            endcase
        end
    end            
endmodule: ird_ip_fm25l_phy
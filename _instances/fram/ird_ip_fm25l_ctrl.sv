/*
 * Модуль "ird_ip_fm25l_ctrl" для проекта Interface_plane.
 * Контроллер физического уровня микросхемы флэш-памяти FM25L16B.
 * Выход "next_byte" при записи запрашивает следующие данные на порте data_bus.mosi,
 * а при чтении информирует о готовности прочитанных данных.
 * Кузнецов М.С.
 * МИЭТ, Институт МПСУ
 * 2020
 * email: kuznetsov_ms@org.miet.ru
 */
`include "defines.svh"
`include "interfaces.svh"
import defines_pkg::framLogMsg_s;
module ird_ip_fm25l_ctrl(input bit clk, nrst, // clk is 128 MHz
                         // Wishbone slave interface
                         wishbone_io.slave wbs,    
                         // SPI interface
                         spi_io.master spi,
                         
                         input logic ena,
                         output logic log_full,
                         output logic [27:0] next_cf_adr,
                         output logic [7:0] next_rec_num,
                         output logic log_rdy);
                         
    generic_bus_io#(.ADR_WIDTH(16), .MOSI_WIDTH(8), .MISO_WIDTH(8)) data_bus();
    // spi_io spi();
	// Debug section
    localparam logic [7:0] CMD_WRITE = 8'h02, CMD_READ = 8'h03, CMD_WREN = 8'h06, CMD_WRDI = 8'h04;
    
    typedef enum bit [2:0] {ST_INIT_READ, ST_READ_LOG, ST_CHECK_LOG, ST_IDLE, ST_WREN, ST_WRITE, ST_WRDI, ST_READ} state_e;
	state_e state;
    // WB signals
	logic wb_rd, wb_wr, wb_done;
	logic [15:0] wb_spi_wr_adr, wb_spi_rd_adr;
	logic [7:0] wb_spi_wr_data;
    //
    logic op_done, was_busy, next_byte;
    logic [7:0] mem_data, op;
    logic [8:0] burst_len, wb_wr_len;
    
    
    logic crc_calced, save_log, switch_log;
    logic [11:0] log_adr_shift;
    framLogMsg_s [1:0] log_msg;
    logic [1:0][2:0] log_cnt;
    logic [1:0][7:0] msg_crc;
    framLogMsg_s new_log_msg, last_log_msg;
    logic [47:0] new_log_msg_q;
    logic [31:0] new_cf_adr;
    // Работа с контроллером PHY
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
			data_bus.re <= 1'b0;
            data_bus.we <= 1'b0;
            data_bus.dat_mosi <= '0;
            data_bus.adr <= '0;			
			wb_done <= 1'b0;	
			op <= '0;
            burst_len <= '0;
			op_done <= 1'b0;
			mem_data <= '0;
			was_busy <= 1'b0;
            state <= ST_INIT_READ;
            
            log_adr_shift <= '0;
            log_msg <= '0;
            last_log_msg <= '0;
            log_cnt <= '0;
            crc_calced <= 1'b0;
            msg_crc <= '0;
            log_rdy <= 1'b0;
            new_log_msg_q <= '0;
            switch_log <= 1'b0;
        end
        else begin
			if (data_bus.busy) begin // WB debug only code
				was_busy <= 1'b1;
				data_bus.re <= 1'b0;
				data_bus.we <= 1'b0;
			end
			else begin 
				was_busy <= 1'b0;
			end
			wb_done <= 1'b0;
			op_done <= 1'b0;
			unique case (state)
                ST_INIT_READ: begin
                    if (log_adr_shift != 12'd768) begin                 
                        data_bus.adr <= `FRAM_LOG_BASE + log_adr_shift;	
                        // log_adr_shift <= log_adr_shift + `FRAM_LOG_REC_SIZE;
                        op <= CMD_READ;
                        data_bus.re <= 1'b1;
                        burst_len <= `FRAM_LOG_BYTES_SIZE; // There is a size of the log.
                        state <= ST_READ_LOG;
                    end
                    else begin
                        log_full <= 1'b1;
                        state <= ST_IDLE;
                    end
                end
                ST_READ_LOG: begin
					if (next_byte) begin
                        log_msg[log_cnt[1]] <= { log_msg[log_cnt[1]][39:0], data_bus.dat_miso };
                        log_cnt[0] <= log_cnt[0] + 1'b1;
                        if (log_cnt[0] == 3'd5) begin
                            log_cnt[1] <= log_cnt[1] + 1'b1;
                            log_cnt[0] <= '0;
                            if (log_cnt[1] == 1'b1) begin
                                log_cnt <= '0;
                                state <= ST_CHECK_LOG;
                            end
                        end
					end
				end
                ST_CHECK_LOG: begin
                    if (!crc_calced) begin
                        msg_crc[0] <= next_crc(log_msg[0][47:8], 8'h55);
                        msg_crc[1] <= next_crc(log_msg[1][47:8], 8'h55);
                        crc_calced <= 1'b1;
                    end
                    else begin
                        state <= ST_INIT_READ;
                        log_adr_shift <= log_adr_shift + `FRAM_LOG_BYTES_SIZE;
                        if (msg_crc[0] == log_msg[0].crc8) begin
                            last_log_msg <= log_msg[0];
                            if (msg_crc[1] == log_msg[1].crc8) begin
                                if (log_msg[1].cf_adr > log_msg[0].cf_adr) begin
                                    last_log_msg <= log_msg[1];
                                end
                            end
                        end
                        else begin
                            if (msg_crc[1] == log_msg[1].crc8) begin
                                last_log_msg <= log_msg[1];
                            end
                            else begin
                                state <= ST_IDLE;
                                log_adr_shift <= log_adr_shift;
                            end
                        end
                        crc_calced <= 1'b0;
                    end
                end
				ST_IDLE: begin
                    log_rdy <= 1'b1;
					if (wb_wr && !op_done) begin
                        burst_len <= 9'd0;
                        if (save_log) begin
                            new_log_msg_q <= new_log_msg;
                            data_bus.dat_mosi <=  new_log_msg[47:40];
                            data_bus.adr <= (switch_log) ? {4'd0, log_adr_shift + 12'd6} : {4'd0, log_adr_shift};
                            switch_log <= ~switch_log;
                        end
                        else begin
                            data_bus.adr <= wb_spi_wr_adr;
                            data_bus.dat_mosi <= wb_spi_wr_data;
                        end
                        state <= ST_WREN;
                        op <= CMD_WREN;
                        data_bus.we <= 1'b1;
					end
					if (wb_rd && !op_done) begin
						data_bus.adr <= wb_spi_rd_adr;	
						op <= CMD_READ;
						data_bus.re <= 1'b1;
                        burst_len <= 9'd1;
                        data_bus.dat_mosi <= wb_spi_wr_data;
						state <= ST_READ;
					end
				end
				ST_READ: begin
					if (!data_bus.busy && was_busy) begin
                        wb_done <= 1'b1;
                        op_done <= 1'b1;
                        mem_data <= data_bus.dat_miso;
                        state <= ST_IDLE;
					end
				end
                ST_WREN: begin
                    if (!data_bus.busy && was_busy) begin
                        op <= CMD_WRITE;
                        data_bus.we <= 1'b1;
                        burst_len <= wb_wr_len;
                        state <= ST_WRITE;
                    end
                end
                ST_WRITE: begin
                    if (next_byte) begin
                        if (burst_len != 9'd1) begin // Условие только при log_save
                            burst_len <= burst_len - 1'b1; // Значение burst_len важно только в первый момент подачи строба data_bus.we
                            data_bus.dat_mosi <=  new_log_msg_q[39:32];
                            new_log_msg_q <= {new_log_msg_q[39:0], 8'd0};
                        end
                    end
                    if (!data_bus.busy && was_busy) begin
                        op <= CMD_WRDI;
                        burst_len <= 9'd0;
                        state <= ST_WRDI;
                        data_bus.we <= 1'b1;
                    end
                end
                ST_WRDI: begin
                    if (!data_bus.busy && was_busy) begin
                        wb_done <= 1'b1;
                        op_done <= 1'b1;
                        state <= ST_IDLE;
                    end
                end
			endcase
        end
    end   
    
    // Работа с шиной WB. Отключать при штатной работе
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin				
            wbs.wbs_ack <= 1'b0;
            wbs.wbs_dat <= '0;
			wb_rd <= 1'b0;
			wb_wr <= 1'b0;
			wb_spi_wr_adr <= '0;
			wb_spi_rd_adr <= '0;
			wb_spi_wr_data <= '0;
            new_cf_adr <= '0;
            save_log <= 1'b0;
            wb_wr_len <= '0;
        end
        else begin
			wbs.wbs_ack <= 1'b0;
            save_log <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                if (wbs.wbm_we) begin
                    unique case (wbs.wbm_adr[11:0])
                        12'd0: begin // Запись флэш-памяти. В слове 32'hAA_XX_BBBB: AA - байт записи, BBBB  - адрес записи, XX - don't care
                            if (wbs.wbm_sel == '1) begin
                                wb_spi_wr_data <= wbs.wbm_dat[31:24];
                                wb_spi_wr_adr <= wbs.wbm_dat[15:0];
                                wb_wr <= 1'b1;
                                wb_wr_len <= 9'd1;
                            end
                            else begin
                                wbs.wbs_ack <= 1'b1;
                            end
                        end
                        12'd1: begin // Установка адреса для чтения флэш-памяти. 16 разрядов.
                            if (wbs.wbm_sel[1:0] == 2'h3) begin
                                wb_spi_rd_adr <= wbs.wbm_dat[15:0];
                                wbs.wbs_ack <= 1'b1;
                            end
                        end
                        12'd2: begin
                            if (wbs.wbm_sel == '1) begin
                                new_cf_adr <= wbs.wbm_dat;
                                wb_wr <= 1'b1;
                                wb_wr_len <= 9'd6;
                                save_log <= 1'b1;
                            end
                            else begin
                                wbs.wbs_ack <= 1'b1;
                            end
                        end
                        default: begin
                            wbs.wbs_ack <= 1'b1;
                        end
                    endcase
                end 
                else begin // Чтение флэш-памяти по адресу wb_spi_rd_adr, после чтения адрес увеличивается на 1.
                    if ((wbs.wbm_adr[11:0] == 12'd0) && (wbs.wbm_sel[0])) begin
                        wb_rd <= 1'b1;
                    end
                    else begin
                        wbs.wbs_ack <= 1'b1;
                    end
                end
            end
			
			if (wb_done) begin
				wbs.wbs_ack <= 1'b1;
				wbs.wbs_dat <= {24'd0, mem_data};
			end
			
			if (op_done) begin
				wb_wr <= 1'b0;
				wb_rd <= 1'b0;
				if (wb_rd) begin
					wb_spi_rd_adr <= wb_spi_rd_adr + 1'b1;
				end
			end
		end
	end
    assign new_log_msg.cf_adr = new_cf_adr;
    assign new_log_msg.num = last_log_msg.num + 1'b1;
    assign new_log_msg.crc8 = next_crc(new_log_msg[47:8], 8'h55);
    assign next_cf_adr = (last_log_msg.cf_adr != 0) ? (last_log_msg.cf_adr[27:0] + `CF_SECTORS_BUFFER) : `CF_BASE_ADR; // Проверка на переполненность флешки производится в других модулях
    assign next_rec_num = last_log_msg.num + 1'b1;
    ird_ip_fm25l_phy irdIpFm25l_Phy(.clk(clk), .nrst(nrst), .operation(op), .burst_len(burst_len), .next_byte(next_byte), .data_bus(data_bus), .spi(spi));
endmodule: ird_ip_fm25l_ctrl
    
function [7:0] next_crc(input logic [39:0] data, logic[7:0] crc);
    // crc[7:0]=1+x^1+x^2+x^5+x^7+x^8;
    logic [39:0] d;
    logic [7:0] c;
    logic [7:0] newcrc;
    d = data;
    c = crc;
    newcrc[0] = c[0] ^ c[1] ^ c[2] ^ c[7] ^ d[0] ^ d[1] ^ d[2] ^ d[4] ^ d[6] ^ d[8] ^ d[13] ^ d[14] ^ d[17] ^ d[18] ^ d[20] ^ d[22] ^ d[23] ^ d[24] ^ d[26] ^ d[27] ^ d[28] ^ d[31] ^ d[32] ^ d[33] ^ d[34] ^ d[39];
    newcrc[1] = c[3] ^ c[7] ^ d[0] ^ d[3] ^ d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[8] ^ d[9] ^ d[13] ^ d[15] ^ d[17] ^ d[19] ^ d[20] ^ d[21] ^ d[22] ^ d[25] ^ d[26] ^ d[29] ^ d[31] ^ d[35] ^ d[39];
    newcrc[2] = c[1] ^ c[2] ^ c[4] ^ c[7] ^ d[0] ^ d[2] ^ d[5] ^ d[7] ^ d[9] ^ d[10] ^ d[13] ^ d[16] ^ d[17] ^ d[21] ^ d[24] ^ d[28] ^ d[30] ^ d[31] ^ d[33] ^ d[34] ^ d[36] ^ d[39];
    newcrc[3] = c[0] ^ c[2] ^ c[3] ^ c[5] ^ d[1] ^ d[3] ^ d[6] ^ d[8] ^ d[10] ^ d[11] ^ d[14] ^ d[17] ^ d[18] ^ d[22] ^ d[25] ^ d[29] ^ d[31] ^ d[32] ^ d[34] ^ d[35] ^ d[37];
    newcrc[4] = c[0] ^ c[1] ^ c[3] ^ c[4] ^ c[6] ^ d[2] ^ d[4] ^ d[7] ^ d[9] ^ d[11] ^ d[12] ^ d[15] ^ d[18] ^ d[19] ^ d[23] ^ d[26] ^ d[30] ^ d[32] ^ d[33] ^ d[35] ^ d[36] ^ d[38];
    newcrc[5] = c[0] ^ c[4] ^ c[5] ^ d[0] ^ d[1] ^ d[2] ^ d[3] ^ d[4] ^ d[5] ^ d[6] ^ d[10] ^ d[12] ^ d[14] ^ d[16] ^ d[17] ^ d[18] ^ d[19] ^ d[22] ^ d[23] ^ d[26] ^ d[28] ^ d[32] ^ d[36] ^ d[37];
    newcrc[6] = c[1] ^ c[5] ^ c[6] ^ d[1] ^ d[2] ^ d[3] ^ d[4] ^ d[5] ^ d[6] ^ d[7] ^ d[11] ^ d[13] ^ d[15] ^ d[17] ^ d[18] ^ d[19] ^ d[20] ^ d[23] ^ d[24] ^ d[27] ^ d[29] ^ d[33] ^ d[37] ^ d[38];
    newcrc[7] = c[0] ^ c[1] ^ c[6] ^ d[0] ^ d[1] ^ d[3] ^ d[5] ^ d[7] ^ d[12] ^ d[13] ^ d[16] ^ d[17] ^ d[19] ^ d[21] ^ d[22] ^ d[23] ^ d[25] ^ d[26] ^ d[27] ^ d[30] ^ d[31] ^ d[32] ^ d[33] ^ d[38];
    next_crc = newcrc;
endfunction: next_crc

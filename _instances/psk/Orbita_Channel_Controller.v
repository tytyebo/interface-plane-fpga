`timescale 1ps/1ps
module Orbita_Channel_Controller
    (
        input nrst,
        input clk,
        
        input clk_ext,
        input video,
        
        output [15:0] wbm_adr_o,
        output [31:0] wbm_dat_o,
        output wbm_stb_o,
        output wbm_cyc_o,
        output [3:0] wbm_sel_o,
        output wbm_we_o,
        input [31:0] wbm_dat_i,
        input wbm_ack_i,
        
        input [15:0] wbs_adr_i,
        input [31:0] wbs_dat_i,
        input wbs_stb_i,
        input wbs_cyc_i,
        input [3:0] wbs_sel_i,
        input wbs_we_i,
        output [31:0] wbs_dat_o,
        output wbs_ack_o,  
        
        // output test_rst = orbita_start;
        input record_cmd,
        input [7:0] rec_num
    );

    //wishbone master registers
    reg m_stb_o;
    reg m_cyc_o;
    reg m_we_o;
    reg [31:0] m_dat_o;
    reg [15:0] m_adr_o;

    //wishbone slave registers
    reg s_ack_o;
    reg [31:0] s_dat_o;

    //internal registers
    reg fifo_rrq;  
    reg [2:0] mode; 
    reg [27:0] cf_size;
    reg mode_init;
    reg cf_size_init;
    reg wb_tx_state;
    reg packet_full;
    reg [31:0] wb_data;
    reg [15:0] wb_adr;
    reg wb_we;
    reg [3:0] state;
    reg [10:0] tx_data_cnt;
    reg skip_packet;
    reg cf_full;
    reg record_ena;
    reg packet_skip_lock;
    reg [3:0] skip_packet_cnt;
    
    wire [3:0] m_sel_o;    
    wire [9:0] fifo_rudw;
    wire [31:0] fifo_q;
    wire orbita_start;
    wire record;
 
    assign wbm_adr_o = m_adr_o;
    assign wbm_dat_o = m_dat_o;
    assign wbm_stb_o = m_stb_o;
    assign wbm_cyc_o = m_cyc_o;
    assign wbm_sel_o = {4{m_cyc_o}};
    assign wbm_we_o = m_we_o;	

    assign wbs_ack_o = s_ack_o; 
    assign wbs_dat_o = s_dat_o; 
    
    assign record = (record_ena) ? record_cmd : 1'b1;
    
    localparam [0:0] ST_WB_TX_IDLE        = 0,
                     ST_WB_TX_ACK         = 1;
    
    localparam [3:0] ST_IDLE                    = 0,
                     ST_GET_CFHL_FIFO_STATUS    = 1,
                     ST_CHECK_CFHL_FIFO_STATUS  = 2,
                     ST_WR_DATA                 = 3;
    
    localparam WB_ADR_CFHL_FIFO           = 16'h2008;
    
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                m_stb_o <= 1'b0;
                m_cyc_o <= 1'b0;
                m_we_o <= 1'b0;
                m_dat_o <= 32'h0000_0000;
                m_adr_o <= 16'h0000;
                
                s_ack_o <= 1'b0;
                s_dat_o <= 32'h0000_0000;
                mode <= 3'b000;
                fifo_rrq <= 1'b0;
                cf_size <= 28'd0;
                
                mode_init <= 1'b0;
                cf_size_init <= 1'b0;
                wb_tx_state <= 1'b0;
                packet_full <= 1'b0;
                wb_data <= 32'h0000_0000;
                wb_adr <= 16'h0000;
                wb_we <= 1'b0;
                state <= ST_IDLE;
                tx_data_cnt <= 11'h000;
                //orbita_start <= 1'b0;                
                record_ena <= 1'b0;
                skip_packet <= 1'b0;
                cf_full <= 1'b0;
                packet_skip_lock <= 1'b0;
                skip_packet_cnt <= 4'd0;
            end
        else
            begin
                s_ack_o <= 1'b0;
                //orbita_start <= mode_init & cf_size_init;
                if (wbs_stb_i & wbs_cyc_i & ~wbs_ack_o)
                    begin
                        s_ack_o <= 1'b1;
                        if (wbs_we_i)
                            begin
                                case (wbs_adr_i[0])
                                    1'b0:
                                        begin
                                            mode[2:0] <= wbs_dat_i[2:0];
                                            record_ena <= wbs_dat_i[4];
                                            mode_init <= 1'b1;
                                        end
                                    1'b1:
                                        begin
                                            cf_size <= wbs_dat_i[27:0];
                                            cf_size_init <= 1'b1;
                                        end
                                endcase
                            end
                        else
                            begin
                                case (wbs_adr_i[1:0])
                                    2'd0:
                                        begin
                                            s_dat_o <= {27'd0, record_ena, mode_init, mode[2:0]};
                                        end
                                    2'd1:
                                        begin
                                            s_dat_o <= {3'd0, cf_size_init, cf_size};
                                        end
                                    default:
                                        s_dat_o <= 32'd0;
                                endcase
                            end
                    end 
                    
                case (wb_tx_state)
                    ST_WB_TX_IDLE:
                        begin
                            if (packet_full)
                                begin
                                    m_stb_o <= 1'b1;
                                    m_cyc_o <= 1'b1;
                                    m_we_o <= wb_we;
                                    m_dat_o <= wb_data;
                                    m_adr_o <= wb_adr;
                                    wb_tx_state <= ST_WB_TX_ACK;                                    
                                end
                        end
                    ST_WB_TX_ACK: 
                        begin
                            if (wbm_ack_i)
                                begin                                       
                                    m_stb_o <= 1'b0;
                                    m_cyc_o <= 1'b0;
                                    m_we_o <= 1'b0;
                                    packet_full <= 1'b0;
                                    wb_tx_state <= ST_WB_TX_IDLE;
                                end                          
                        end
                endcase             
                fifo_rrq <= 1'b0;                
                case (state)
                    ST_IDLE:
                        begin
                            if (fifo_rudw >= 256)
                                begin
                                    if (packet_skip_lock)
                                        state <= ST_WR_DATA;
                                    else    
                                        state <= ST_GET_CFHL_FIFO_STATUS;
                                end
                        end
                    ST_GET_CFHL_FIFO_STATUS:
                        begin
                            packet_full <= 1'b1;
                            wb_we <= 1'b0;
                            wb_adr <= 16'h2008;
                            state <= ST_CHECK_CFHL_FIFO_STATUS;                           
                        end
                    ST_CHECK_CFHL_FIFO_STATUS:
                        begin
                            if (wbm_ack_i)
                                begin
                                    if (wbm_dat_i[13:0] > 13'd7936) // it's mean fifo full
                                        begin
                                            skip_packet <= 1'b1;
                                            skip_packet_cnt <= skip_packet_cnt + 1'b1;
                                        end
                                    else 
                                        skip_packet <= 1'b0;
                                        
                                    if (wbm_dat_i[15])
                                        cf_full <= 1'b1;
                                            
                                    packet_skip_lock <= 1'b1;  
                                    state <= ST_WR_DATA;
                                end
                        end
                    ST_WR_DATA:                     
                        begin
                            if (~packet_full && record)
                                begin
                                    packet_full <= (~skip_packet) ? 1'b1 : 1'b0;
                                    wb_adr <= WB_ADR_CFHL_FIFO;
                                    wb_we <= 1'b1;
                                    wb_data <= {fifo_q[7:0], fifo_q[15:8], fifo_q[23:16], fifo_q[31:24]};
                                    tx_data_cnt <= tx_data_cnt + 1'b1; 
                                    fifo_rrq <= 1'b1;
                                    if (tx_data_cnt[7:0] != 255)
                                        state <= ST_WR_DATA;
                                    else
                                        begin
                                            // wb_data <= {fifo_q[7:0], skip_packet_cnt, fifo_q[11:8], fifo_q[23:16], fifo_q[31:24]};
                                            wb_data <= {fifo_q[7:0], skip_packet_cnt, fifo_q[11:8], rec_num, fifo_q[31:24]};
                                            state <= ST_IDLE;
                                            packet_skip_lock <= 1'b0;
                                        end
                                end
                        end
                endcase
    end                  



psk_phy psk_phy0 
	(
		//-- PSK External Interface Inputs
		.SDI(video),
		
		//-- PSK RX-FIFO Read Interface
		.FIFO_RRQ(fifo_rrq),
		.FIFO_Q(fifo_q), // FIFO_Q is big-endian 32-bit words
		.FIFO_RUDW(fifo_rudw),
		
		//-- MODE TRUTH TABLE:
		//-- "000" - OFF
		//-- "001" -  16384 kbps
		//-- "010" -  32768 kbps
		//-- "011" -  65536 kbps
		//-- "100" - 131072 kbps
		//-- "101" - 262144 kbps
		//-- "110" - OFF
		//-- "111" - OFF
		.MODE(mode),		//-- In RCLK Clock-Domain
		
		//-- System Signals (Системные сигналы)
		.nRST(nrst & orbita_start),		//-- In RCLK Clock-Domain
		.RCLK(clk),
		.WCLK(clk_ext)
	);
metastable_chain
    #(
        .CHAIN_DEPTH(50), // FIXME: ЗАЧЕМ?! UPD: рудимент от отладки с передатчиком в этой же плис
        .BUS_WIDTH(1)
    ) 
    metastable_chain_occ        
    (
        .clk(clk),
        .nrst(1'b1),
        .din(mode_init & cf_size_init),
        .dout(orbita_start)
    );
endmodule
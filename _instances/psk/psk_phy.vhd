-- ## First Code : 14/06/2016               ##
-- ## First Compilation: 09:26 14-06-2016   ##
-- ## Author     : Sergey Romanov           ##
-- ## Company    : NII VS @ SU              ##


-- 13/02/2017 :: Add fsm_stop_pulse signal
-- 24/04/2017 :: Change MODE coding

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.numeric_std.all; 

LIBRARY WORK;
USE WORK.exinf_se_package.all;

ENTITY psk_phy is 
	port(
		-- PSK External Interface Inputs
		SDI : IN std_logic;
		
		-- PSK RX-FIFO Read Interface
		FIFO_RRQ  :  IN std_logic;
		FIFO_Q    :  OUT std_logic_vector(31 downto 0);
		FIFO_RUDW  : OUT std_logic_vector(9 downto 0);
		
		-- MODE TRUTH TABLE:
		-- "000" - OFF
		-- "001" -  16384 kbps
		-- "010" -  32768 kbps
		-- "011" -  65536 kbps
		-- "100" - 131072 kbps
		-- "101" - 262144 kbps
		-- "110" - OFF
		-- "111" - OFF
		MODE : IN std_logic_vector(2 downto 0);		-- In RCLK Clock-Domain
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';		-- In RCLK Clock-Domain
		RCLK : IN std_logic;
		WCLK : IN std_logic
	);
END psk_phy;

ARCHITECTURE SEHDL of psk_phy is

-- SECTION 1. COMPONENTS
COMPONENT metastable_chain	-- ��������� ������������ �������
	GENERIC (CHAIN_DEPTH : POSITIVE;
             BUS_WIDTH : POSITIVE);
	PORT
	(
		din		:	 IN STD_LOGIC_VECTOR(BUS_WIDTH-1 DOWNTO 0);
		dout	:	 OUT STD_LOGIC_VECTOR(BUS_WIDTH-1 DOWNTO 0);
		clk		:	 IN STD_LOGIC;
        nrst	:	 IN STD_LOGIC
	);
END COMPONENT;

component psk_rx_fifo	-- ��������� ������ ������ FIFO
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdclk		: IN STD_LOGIC;
		rdreq		: IN STD_LOGIC;
		wrclk		: IN STD_LOGIC;
		wrreq		: IN STD_LOGIC;
		q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdusedw		: OUT STD_LOGIC_VECTOR (9 DOWNTO 0);
		wrusedw		: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
	);
end component;

-- SECTION 2. SUBPROGRAMS (������� ��������������)
function  majn(n : positive; b : in std_logic_vector(31 downto 0)) return  std_logic is	-- majority function, size n <= 31
	variable c0, c1 : natural := 0;
	variable mj : std_logic;
begin
	for i in n-1 downto 0 loop
		if b(i) = '1' then c1 := c1 + 1; else c0 := c0 + 1; end if;
	end loop;
	if c1 > c0 then mj := '1'; else mj := '0'; end if;
	return mj;
end majn;

-- SECTION 3. SIGNALS

-- MST system/mode Signals
signal sdi_bus : std_logic_vector(0 downto 0);
signal sdi_mst : std_logic_vector(0 downto 0);

signal nrst_bus : std_logic_vector(0 downto 0);
signal nrst_mst : std_logic_vector(0 downto 0);
signal nRSTW : std_logic;

signal modew : std_logic_vector(2 downto 0);
signal modew_ena : std_logic;

-- Majority block Signals
signal maj_shr : std_logic_vector(31 DOWNTO 0);
signal maj_sdm : std_logic;
signal maj_enf : std_logic;
signal maj_sdo : std_logic;
signal maj_ena : std_logic;
signal maj_sdo_z : std_logic;
signal maj_ena_z : std_logic;

-- Decimation block Signals
signal decim_ena : std_logic;
signal decim_rt_ena : std_logic;
signal decim_start_ena : std_logic;
type type_decim_state is (decim_state_ini, decim_state_ena);
signal decim_state : type_decim_state;	-- ������ ���������
signal decim_sdo : std_logic;
SUBTYPE DECIM_COUNTER_TYPE IS NATURAL RANGE 0 TO 7;				-- ��� ��������� ���������� ��������		
signal decim_cnt : DECIM_COUNTER_TYPE;

-- Real Timer signals
signal rtl : std_logic_vector(30 downto 0);
signal rth : std_logic_vector(8 downto 0);
signal rtl_cout : std_logic;
signal rth_cout : std_logic;

-- FSM
signal stop_condition_flag : std_logic;
type type_fsm_state is (fsm_state_start, fsm_state_data);
signal fsm_state : type_fsm_state;	-- ������ ���������
signal fsm_rtl : std_logic_vector(30 downto 0);
signal fsm_rth : std_logic_vector(8 downto 0);
signal fsm_rth_cout : std_logic;
signal fsm_head : std_logic;
signal fsm_fifofull_skip_data_flag : std_logic;
signal fsm_fifofull_skip_data_reg : std_logic;
signal fsm_stop : std_logic;
signal fsm_ph, fsm_ph_rg : std_logic_vector(2 downto 0);
signal fsm_ph_ena : std_logic;
SUBTYPE NEQ_COUNTER_TYPE IS NATURAL RANGE 0 TO 2**12-1;				-- ��� ��������� ���������� ��������		
type NEQ_COUNTER_ARRAY_TYPE  is array(2 downto 0) of NEQ_COUNTER_TYPE;
signal fsm_pcnt : NEQ_COUNTER_ARRAY_TYPE;
signal fsm_bcnt : std_logic_vector(5 downto 0);
SUBTYPE WORD_COUNTER_TYPE IS NATURAL RANGE 0 TO 251;				-- ��� ��������� ���������� ��������		
signal fsm_wcnt : WORD_COUNTER_TYPE;
signal fsm_word : std_logic;
signal fsm_end_flag : std_logic;
signal fsm_stop_level : std_logic;
signal fsm_shift_regn : std_logic_vector(3 downto 0);
signal fsm_shift_regw : std_logic_vector(31 downto 0);

signal fsm_fifo_csum : std_logic_vector(31 downto 0);
signal fsm_fifo_cmd : std_logic_vector(30 downto 0);
signal fsm_cmd_pary_ena : std_logic;
signal fsm_head_stage2 : std_logic;
signal fsm_fifo_data : std_logic_vector(31 downto 0);
signal fsm_fifo_wrq : std_logic;
signal fsm_end : std_logic;
signal fsm_status : std_logic_vector(1 downto 0);
signal fsm_stop_pulse : std_logic;

type NC_ARRAY_TYPE  is array(2 downto 0) of std_logic_vector(1 downto 0);
signal fsm_nc : NC_ARRAY_TYPE;
	
-- PSK RX-FIFO Signals
signal fifo_wudw : std_logic_vector(9 downto 0);

-- READ SIDE PSK RX-FIFO CLOCK DOMAIN Signals
-- PSK RX-FIFO Signals
signal fifo_clear : std_logic;
signal moder_reg : std_logic_vector(2 downto 0);

signal CMODE : std_logic_vector(2 downto 0);

BEGIN

-- WRITE SIDE PSK RX-FIFO CLOCK DOMAIN

sdi_bus(0) <= SDI;
sdi_msch_cmp : metastable_chain
GENERIC MAP(CHAIN_DEPTH => MSN,
            BUS_WIDTH => 1)
PORT MAP(din => sdi_bus, dout => sdi_mst, clk => WCLK, nrst => '1');

nrst_bus(0) <= nRST;
nrst_msch_cmp : metastable_chain
GENERIC MAP(CHAIN_DEPTH => MSN + 3,
            BUS_WIDTH => 1)
PORT MAP(din => nrst_bus, dout => nrst_mst, clk => WCLK, nrst => nRST);
nRSTW <= nrst_mst(0);

mode_msch_cmp : metastable_chain
GENERIC MAP(CHAIN_DEPTH => MSN,
            BUS_WIDTH => 3)
PORT MAP(din => moder_reg, dout => modew, clk => WCLK, nrst => '1');

modew_proc: process(WCLK, nRSTW)
begin
	if nRSTW = '0' then
		modew_ena <= '0';
	elsif WCLK'event and (WCLK = '1') then
		modew_ena <= '1';
		if modew = "111" then
			modew_ena <= '0';
		end if;
	end if;
end process;

-- Majority block
maj_proc: process(WCLK, nRSTW)
variable m, n : natural;
begin
	if nRSTW = '0' then
		maj_shr <= (0 => '1', others => '0');
		maj_sdm <= '0';
		maj_enf <= '0';
		maj_sdo <= '0';
		maj_ena <= '0';
		maj_sdo_z <= '0';
		maj_ena_z <= '0';
	elsif WCLK'event and (WCLK = '1') then
		m := to_integer(unsigned(modew(1 downto 0)));		-- Int2Std: STD_LOGIC_VECTOR(to_unsigned(q,N));
		n := 2**(m+2)-1;
		maj_shr(0) <= sdi_mst(0);
		for i in 1 to 31 loop		-- maj_shr(n) - ������ ��� maj_ena
			if i <= n then
				maj_shr(i) <= maj_shr(i-1);
			end if;
		end loop;
		case n is
			when  3 => maj_sdm <= majn( 3,maj_shr);
			when  7 => maj_sdm <= majn( 7,maj_shr);
			when 15 => maj_sdm <= majn(15,maj_shr);
			when 31 => maj_sdm <= majn(31,maj_shr);
			when others => null;
		end case;
		if maj_shr(n) = '1' then
			maj_enf <= modew_ena;	-- ���������� ������ ����� �����������
		end if;
		if maj_enf = '1' then
			maj_sdo <= maj_sdm;
			maj_ena <= '1';
		end if;
		if maj_ena = '1' then
			maj_sdo_z <= maj_sdo;
			maj_ena_z <= '1';
		end if;
	end if;
end process;

-- Decimation block
decim_proc: process(WCLK, nRSTW)
variable m : natural;
variable ena : boolean;
begin
	if nRSTW = '0' then
		decim_ena <= '0';
		decim_rt_ena <= '0';
		decim_start_ena <= '0';
		decim_state <= decim_state_ini;
		decim_sdo <= '0';
		decim_cnt <= 0;
	elsif WCLK'event and (WCLK = '1') then
		ena := false;
		decim_ena <= '0';
		decim_rt_ena <= '0';
		decim_start_ena <= '0';
		case decim_state is
		when decim_state_ini =>
			if (maj_ena_z = '1') and ((maj_sdo xor maj_sdo_z) = '1') then
				ena := true;
				decim_state <= decim_state_ena;
			end if;
		when decim_state_ena =>
			if decim_cnt /= 0 then
				decim_cnt <= decim_cnt - 1;
			else
				ena := true;
			end if;
		end case;
		if ena then
			decim_ena <= (not fsm_stop) or (decim_sdo xor maj_sdo);
			decim_rt_ena <= '1';
			decim_start_ena <= decim_sdo xor maj_sdo;
			decim_sdo <= maj_sdo;
			m := to_integer(unsigned(modew(1 downto 0)));		-- Int2Std: STD_LOGIC_VECTOR(to_unsigned(q,N));
			decim_cnt <= 2**m - 1;
		end if;
	end if;
end process;

-- Real Timer process
rt_proc: process(WCLK, nRSTW)
begin
	if nRSTW = '0' then
		rtl <= (others => '0');
		rth <= (others => '0');
		rtl_cout <= '0';
		rth_cout <= '0';
	elsif WCLK'event and (WCLK = '1') then
		if decim_rt_ena = '1' then
			rtl <= rtl + 1;
			if rtl_cout = '1' then
				 rth <= rth + 1;
			end if;
		end if;
		if rtl = "111"&X"FFFFFFE" then
			rtl_cout <= '1';
		else
			rtl_cout <= '0';
		end if;
		if rth = '1'&X"FF" then
			rth_cout <= '1';
		end if;
	end if;
end process;

-- FSM
fsm_proc: process(WCLK, nRSTW)
variable nc : NC_ARRAY_TYPE;
variable p : std_logic;
begin
	if nRSTW = '0' then
		stop_condition_flag <= '0';
		fsm_state <= fsm_state_start;
		fsm_rth <= (others => '0');
		fsm_rtl <= (others => '0');
		fsm_rth_cout <= '0';
		fsm_head <= '0';
		fsm_fifofull_skip_data_flag <= '0';
		fsm_fifofull_skip_data_reg <= '0';
		fsm_stop <= '0';
		fsm_ph_ena <= '0';
		fsm_ph <= (others => '0');
		for i in 0 to 2 loop fsm_pcnt(i) <= 0; end loop;
		fsm_bcnt <= (others => '0');
		fsm_wcnt <= 0;
		fsm_word <= '0';
		fsm_end_flag <= '0';
		fsm_stop_level <= '0';
		
		fsm_shift_regn <= (others => '0');
		fsm_shift_regw <= (others => '0');
		
		fsm_fifo_csum <= (others => '0');
		fsm_fifo_cmd <= (others => '0');
		fsm_cmd_pary_ena <= '0';
		fsm_head_stage2 <= '0';
		fsm_fifo_data <= (others => '0');
		fsm_fifo_wrq <= '0';
		fsm_end <= '0';
		fsm_status <= "00";
		fsm_stop <= '0';
		for i in 0 to 2 loop fsm_nc(i) <= "00";	end loop;
		
		fsm_stop_pulse <= '0';
			
	elsif WCLK'event and (WCLK = '1') then
		-- Single strobes
		
		fsm_word <= '0';
		fsm_end_flag <= '0';
		fsm_end <= '0';
		fsm_cmd_pary_ena <= '0';
		fsm_fifo_wrq <= '0';
		fsm_stop_pulse <= '0';
		
		-- Decim Enable Operation
		if decim_ena = '1' then
			-- Stop Condition
			stop_condition_flag <= stop_condition_flag and not decim_start_ena;
			-- FSM
			case fsm_state is
			when fsm_state_start =>
				fsm_rth <= rth;
				fsm_rth_cout <= rth_cout;
				fsm_rtl <= rtl;
				if (fifo_wudw <= 256) and (fsm_stop_pulse = '0') then
					fsm_head <= '1';
					fsm_fifofull_skip_data_flag <= fsm_fifofull_skip_data_reg;
					fsm_fifofull_skip_data_reg <= '0';
					fsm_stop <= '0';
					fsm_state <= fsm_state_data;
				else
					fsm_fifofull_skip_data_reg <= '1';
				end if;
				stop_condition_flag <= '1';
				fsm_ph <= (others => '0');
				for i in 0 to 2 loop fsm_pcnt(i) <= 0; end loop;
				fsm_wcnt <= 251;
			when fsm_state_data =>
				if fsm_bcnt = "11"&X"F" then -- = 63
					fsm_word <= '1';
					if fsm_wcnt /= 0 then
						fsm_wcnt <= fsm_wcnt - 1;
					else
						fsm_end_flag <= '1';
						fsm_stop_level <= decim_sdo;
						fsm_stop_pulse <= stop_condition_flag;
						fsm_stop <= stop_condition_flag;
						fsm_state <= fsm_state_start;
					end if;
				end if;
			end case;
			-- Data Bit Processing
			fsm_bcnt <= fsm_bcnt + 1;
			fsm_shift_regn(0) <= decim_sdo;
			for i in 1 to 3 loop
				fsm_shift_regn(i) <= fsm_shift_regn(i-1);
			end loop;
			if fsm_bcnt(0) = '1' then
				fsm_shift_regw(0) <= decim_sdo;
				for i in 1 to 31 loop
					fsm_shift_regw(i) <= fsm_shift_regw(i-1);
				end loop;
			end if;
			fsm_ph_ena <= fsm_bcnt(1) and fsm_bcnt(0);	-- = "11"
			if fsm_ph_ena = '1' then
				for i in 0 to 2 loop
					if (fsm_shift_regn(i) xor fsm_shift_regn(i+1)) = '1' then
						fsm_ph(i) <= '1';
						fsm_pcnt(i) <= fsm_pcnt(i) + 1;
					end if;
				end loop;
			end if;
		end if;
		-- PACKING
		-- Packet Head
		if (fsm_head = '1') and (fsm_end = '0') then
			fsm_fifo_csum <= (others => '0');
			fsm_fifo_cmd <= X"B0B1" & '0' & MODE & fsm_fifofull_skip_data_flag & fsm_rth_cout & fsm_rth;	-- �������� MODE 10.02.2017
			fsm_cmd_pary_ena <= '1';
			fsm_head_stage2 <= '1';
			fsm_head <= '0';
		end if;
		if fsm_head_stage2 = '1' then
			fsm_fifo_cmd <= fsm_rtl;
			fsm_cmd_pary_ena <= '1';
			fsm_head_stage2 <= '0';
		end if;
		-- Packet Word
		if fsm_word = '1' then
			fsm_fifo_data <= fsm_shift_regw;
			fsm_fifo_wrq <= '1';
			fsm_fifo_csum <= fsm_fifo_csum + fsm_shift_regw;
			fsm_end <= fsm_end_flag;
		end if;
		-- Packet End
		if fsm_end_flag = '1' then
			--fsm_stop <= stop_condition_flag;
			fsm_status <= "00";	-- Normal Status: Continue Receive
			if stop_condition_flag = '1' then
				fsm_status <= '1' & fsm_stop_level;	-- Stop Status: Stop Recieve
				fsm_bcnt <= (others => '0');
			end if;
			for i in 0 to 2 loop
				nc(i) := "00";
				for j in 0 to 2 loop
					if fsm_pcnt(i) > fsm_pcnt(j) then
						nc(i) := nc(i) + 1;
					end if;
					if fsm_pcnt(i) >= 2**11 then
						nc(i) := nc(i) + 1;
					end if;
				end loop;
				fsm_nc(i) <= nc(i);
			end loop;
			fsm_ph_rg <= fsm_ph;
		end if;
		if fsm_end = '1' then
			fsm_fifo_data <= fsm_fifo_csum;
			fsm_fifo_wrq <= '1';
			fsm_fifo_cmd <= X"E0E1E" & fsm_nc(2) & fsm_nc(1) & fsm_nc(0) & fsm_status & fsm_ph_rg;
			fsm_cmd_pary_ena <= '1';
		end if;
		-- Paritet
		if fsm_cmd_pary_ena = '1' then
			p := '1';
			for i in 0 to 30 loop
				p := p xor fsm_fifo_cmd(i);
			end loop;
			fsm_fifo_data <= fsm_fifo_cmd & p;
			fsm_fifo_wrq <= '1';
		end if;
	end if;
end process;

-- PSK RX-FIFO
fifo_buf : psk_rx_fifo	-- ��������� ������ ������ FIFO
port map
(
	wrreq		=> fsm_fifo_wrq,	--: IN STD_LOGIC ;
	data		=> fsm_fifo_data,	--: IN STD_LOGIC_VECTOR (31 DOWNTO 0);		
	wrusedw		=> fifo_wudw,	--: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
	wrclk		=> WCLK,	--: IN STD_LOGIC ;
	
	rdreq		=> FIFO_RRQ,	--: IN STD_LOGIC ;
	q			=> FIFO_Q,	--: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
	rdusedw		=> FIFO_RUDW,	--: OUT STD_LOGIC_VECTOR (9 DOWNTO 0);
	aclr		=> fifo_clear,	--: IN STD_LOGIC  := '0';
	rdclk		=> RCLK	--: IN STD_LOGIC ;
);

-- READ SIDE PSK RX-FIFO CLOCK DOMAIN
fifo_clear <= not nRST;

-- �MODE TRUTH TABLE:
-- "000" - 131072 kbps
-- "X01" -  65536 kbps
-- "X10" -  32768 kbps
-- "011" -  16384 kbps
-- "100" - 262144 kbps
-- "111" - OFF
-- FORMULA MAJ: 2^(�MODE(1:0) + 2) - 1
-- FORMULA DECIM: 2^�MODE(1:0)
mode_mux: process(MODE)
begin
	case MODE is
		when "001" =>  CMODE <= "011";	--  16384 kbps
		when "010" =>  CMODE <= "010";	--  32768 kbps
		when "011" =>  CMODE <= "001";	--  65536 kbps
		when "100" =>  CMODE <= "000";	-- 131072 kbps
		when "101" =>  CMODE <= "100";	-- 262144 kbps
		when others => CMODE <= "111";	-- OFF
	end case;
end process;

moder_proc: process(RCLK, nRST)
variable ena : boolean;
begin
	if nRST = '0' then
		moder_reg <= (others => '0');
		ena := true;
	elsif RCLK'event and (RCLK = '1') then
		if ena then
			moder_reg <= CMODE;
			ena := false;
		end if;
	end if;
end process;

END SEHDL;
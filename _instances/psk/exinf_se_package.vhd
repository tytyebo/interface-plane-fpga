library IEEE,STD;
use IEEE.std_Logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

package exinf_se_package is

	-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	-- $$ ��������� ������� UZI75\EXINF-SE $$
	-- $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	
	-- 1) ����������������
	CONSTANT MSN : positive := 3;
	
	-- 2) �������� �������
	-- System Clock Frequency, MHz
	CONSTANT FSYS : REAL := 128.00;				-- F(CLK) = 128.00MHz
	
	-- 3) ������ ������
	CONSTANT PACK_SIZE : POSITIVE := 1024;								-- 1024 �����
	CONSTANT PACK_IN_BYTE_SIZE_NUM : POSITIVE := PACK_SIZE - 1;			-- = 1023
	CONSTANT PACK_IN_DWORD_SIZE_NUM : POSITIVE := PACK_SIZE / 4 - 1;	-- = 255
	

		
end exinf_se_package;
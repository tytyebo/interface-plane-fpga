-- ## Start Code : 15/08/2016               ##
-- ## First Code : 24/08/2016               ##
-- ## First Compilation: 09:26 24-08-2016   ##
-- ## Author     : Sergey Romanov           ##
-- ## Company    : NII VS @ SU              ##

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
USE IEEE.numeric_std.all; 

LIBRARY WORK;
USE WORK.exinf_se_package.all;

ENTITY psk_tgen is 
	port(
		-- PSK External Interface Output
		SDO : OUT std_logic;
		
		-- PSK TX-FIFO Write Interface
		FIFO_WRQ  :  IN std_logic;
		FIFO_DATA :  IN std_logic_vector(31 downto 0);
		FIFO_WUDW  : OUT std_logic_vector(9 downto 0);
		
		-- MODE:
		-- TRUTH TABLE:
		-- "000" - 131072 12-bwps = 1572864 bps		Kdiv =  4
		-- "X01" -  65536 12-bwps =  786432 bps		Kdiv =  8
		-- "X10" -  32768 12-bwps =  393216 bps		Kdiv = 16
		-- "011" -  16384 12-bwps =  196608 bps		Kdiv = 32
		-- "100" - 262144 12-bwps = 3145728 bps		Kdiv =  4
		-- "111" -  OFF
		-- FORMULA DECIM: 4 * 2^MODE(1:0)
		MODE : IN std_logic_vector(2 downto 0);		-- In WCLK Clock-Domain
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';		-- In WCLK Clock-Domain
		WCLK : IN std_logic;
		RCLK : IN std_logic		-- 4 * 3 * 2^20 �� = 12,582912 ��� - ������� ������ ������ "������"
	);
END psk_tgen;

ARCHITECTURE SEHDL of psk_tgen is

-- SECTION 1. COMPONENTS
COMPONENT metastable_chain	-- ��������� ������������ �������
	GENERIC (CHAIN_DEPTH : POSITIVE;
             BUS_WIDTH : POSITIVE);
	PORT
	(
		din		:	 IN STD_LOGIC_VECTOR(BUS_WIDTH-1 DOWNTO 0);
		dout	:	 OUT STD_LOGIC_VECTOR(BUS_WIDTH-1 DOWNTO 0);
		clk		:	 IN STD_LOGIC;
        nrst	:	 IN STD_LOGIC
	);
END COMPONENT;

component psk_tx_fifo	-- ��������� ������ ������ FIFO
	PORT
	(
		aclr		: IN STD_LOGIC  := '0';
		data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdclk		: IN STD_LOGIC;
		rdreq		: IN STD_LOGIC;
		wrclk		: IN STD_LOGIC;
		wrreq		: IN STD_LOGIC;
		q		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdusedw		: OUT STD_LOGIC_VECTOR (9 DOWNTO 0);
		wrusedw		: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
	);
end component;

-- SECTION 2. SIGNALS
-- WRITE SIDE PSK TX-FIFO CLOCK DOMAIN
-- System/Mode Signals
signal fifo_clear : std_logic;
signal modew_reg : std_logic_vector(2 downto 0);

-- WRITE-TO-READ SIDE PSK RX-FIFO CLOCK DOMAIN CROSSOVER
-- MST System/Mode Signals
signal nrst_bus : std_logic_vector(0 downto 0);
signal nrst_mst : std_logic_vector(0 downto 0);
signal nRSTR : std_logic;
signal moder : std_logic_vector(2 downto 0);

-- PSK TX-FIFO Read-Side Signals
signal fifo_q : STD_LOGIC_VECTOR (31 DOWNTO 0);
signal fifo_rudw : STD_LOGIC_VECTOR (9 DOWNTO 0);

-- READ SIDE PSK RX-FIFO CLOCK DOMAIN
-- Mode Enable Block Signals
signal moder_ena : std_logic;

-- Decimation block Signals
signal decim_ena : std_logic;
SUBTYPE DECIM_COUNTER_TYPE IS NATURAL RANGE 0 TO 31;				-- ��� ��������� ���������� ��������		
signal decim_cnt : DECIM_COUNTER_TYPE;

-- FSM Signals
signal fsm_fifo_rrq : std_logic;
signal fsm_bcnt : std_logic_vector(4 downto 0); 
signal fsm_bcnt_cout : std_logic;
type type_fsm_state is (fsm_state_idle, fsm_state_tx_packet);
signal fsm_state : type_fsm_state;	-- ������ ���������
signal fsm_shift_reg : std_logic_vector(31 downto 0);
SUBTYPE WORD_COUNTER_TYPE IS NATURAL RANGE 0 TO 255;				-- ��� ��������� ���������� ��������		
signal fsm_wcnt : WORD_COUNTER_TYPE;
signal fsm_sdo : std_logic;

BEGIN

-- WRITE SIDE PSK TX-FIFO CLOCK DOMAIN
fifo_clear <= not nRST;

modew_proc: process(WCLK, nRST)
variable ena : boolean;
begin
	if nRST = '0' then
		modew_reg <= (others => '0');
		ena := true;
	elsif WCLK'event and (WCLK = '1') then
		if ena then
			modew_reg <= MODE;
			ena := false;
		end if;
	end if;
end process;

-- WRITE-TO-READ SIDE PSK RX-FIFO CLOCK DOMAIN CROSSOVER
nrst_bus(0) <= nRST;
nrst_msch_cmp : metastable_chain
GENERIC MAP(CHAIN_DEPTH => MSN + 3,
            BUS_WIDTH => 1)
PORT MAP(din => nrst_bus, dout => nrst_mst, clk => RCLK, nrst => nRST);
nRSTR <= nrst_mst(0);

mode_msch_cmp : metastable_chain
GENERIC MAP(CHAIN_DEPTH => MSN,
            BUS_WIDTH => 3)
PORT MAP(din => modew_reg, dout => moder, clk => RCLK, nrst => '1');

-- PSK TX-FIFO
fifo_buf : psk_tx_fifo	-- ��������� ������ ������ FIFO
port map
(
	rdreq		=> fsm_fifo_rrq,	--: IN STD_LOGIC ;
	q			=> fifo_q,	--: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
	rdusedw		=> fifo_rudw,	--: OUT STD_LOGIC_VECTOR (9 DOWNTO 0);
	aclr		=> fifo_clear,	--: IN STD_LOGIC  := '0';
	rdclk		=> RCLK,	--: IN STD_LOGIC ;

	wrreq		=> FIFO_WRQ,	--: IN STD_LOGIC ;
	data		=> FIFO_DATA,	--: IN STD_LOGIC_VECTOR (31 DOWNTO 0);		
	wrusedw		=> FIFO_WUDW,	--: OUT STD_LOGIC_VECTOR (9 DOWNTO 0)
	wrclk		=> WCLK	--: IN STD_LOGIC ;
);

-- READ SIDE PSK RX-FIFO CLOCK DOMAIN
-- Mode Enable Block
moder_proc: process(RCLK, nRSTR)
begin
	if nRSTR = '0' then
		moder_ena <= '0';
	elsif RCLK'event and (RCLK = '1') then
		moder_ena <= '1';
		if moder = "111" then
			moder_ena <= '0';
		end if;
	end if;
end process;

-- Decimation block
decim_proc: process(RCLK, nRSTR)
variable m : natural;
variable ena : boolean;
begin
	if nRSTR = '0' then
		decim_ena <= '0';
		decim_cnt <= 0;
	elsif RCLK'event and (RCLK = '1') then
		ena := false;
		decim_ena <= '0';
		if moder_ena = '1' then
			if decim_cnt /= 0 then
				decim_cnt <= decim_cnt - 1;
			else
				ena := true;
			end if;
		end if;
		if ena then
			decim_ena <= '1';
			m := to_integer(unsigned(moder(1 downto 0)));		-- Int2Std: STD_LOGIC_VECTOR(to_unsigned(q,N));
			decim_cnt <= 4 * 2**m - 1;
		end if;
	end if;
end process;

-- FSM
fsm_proc: process(RCLK, nRSTR)
begin
	if nRSTR = '0' then

		fsm_fifo_rrq <= '0';
		fsm_bcnt_cout <= '0';
		fsm_state <= fsm_state_idle;
		fsm_shift_reg <= (others => '0');
		fsm_bcnt <= (others => '0');
		fsm_wcnt <= 0;
		fsm_sdo <= '0';
			
	elsif RCLK'event and (RCLK = '1') then
		-- Single strobes		
		fsm_fifo_rrq <= '0';
		fsm_bcnt_cout <= '0';
		
		-- FSM
		case fsm_state is
		when fsm_state_idle =>
			fsm_shift_reg <= fifo_q;
			if fifo_rudw(9 downto 8) /= "00" then   -- >= 256
				-- Read First FIFO Data Word
				fsm_fifo_rrq <= '1';
				fsm_state <= fsm_state_tx_packet;
			end if;
			fsm_bcnt <= '1' & X"F";	-- = 31
			fsm_wcnt <= 255;
		when fsm_state_tx_packet =>
			if decim_ena = '1' then
				fsm_sdo <= fsm_shift_reg(31);
				fsm_shift_reg(31 downto 1) <= fsm_shift_reg(30 downto 0);
				fsm_bcnt <= fsm_bcnt - 1;
				if fsm_bcnt = 0 then
					fsm_bcnt_cout <= '1';
				end if;			
			end if;
		end case;
		-- Read Next FIFO Data Word
		if fsm_bcnt_cout = '1' then
			fsm_shift_reg <= fifo_q;
			fsm_fifo_rrq <= '1';
			if fsm_wcnt /= 0 then
				fsm_wcnt <= fsm_wcnt - 1;
			else
				-- Last FIFO Data Word has read
				fsm_state <= fsm_state_idle;
			end if;
		end if;

	end if;
end process;

SDO <= fsm_sdo;

END SEHDL;
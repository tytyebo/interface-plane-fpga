`timescale 1ps/1ps
module wb_registers
    (
        input nrst,
        input clk,
        
        input [15:0] wb_adr_i,
        input [31:0] wb_dat_i,	
        input wb_stb_i,
        input wb_cyc_i,
        input [3:0] wb_sel_i,
        input wb_we_i,
        output [31:0] wb_dat_o,
        output wb_ack_o
    );
    localparam register0 = 32'hAAAA_5555;
    localparam register1 = 32'h5555_AAAA;
    reg ack;
    reg [31:0] register2;
    reg [31:0] data_out;
    reg [31:0] dat_o;
    //reg [31:0] dat_i;
    assign wb_dat_o = data_out;
    assign wb_ack_o = ack;
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                ack <= 1'b0;
                register2 <= 32'h0000;
                data_out <= 32'h0000;
            end
        else
            begin
                ack <= 1'b0;
                if (wb_stb_i && wb_cyc_i && ~wb_ack_o)
                    begin
                        if (wb_we_i)
                            case (wb_adr_i[11:0])
                                16'h0002:
                                    register2 <= wb_dat_i;
                            endcase
                        else
                            case (wb_adr_i[11:0])
                                12'h000:
                                    data_out <= register0;
                                12'h001:
                                    data_out <= register1;
                                12'h002:
                                    data_out <= register2;
                                default:
                                    data_out <= 32'd0;
                            endcase
                        ack <= 1'b1;
                    end
            end
endmodule
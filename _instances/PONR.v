/** Версия 3. 17.11.16
  * 1. Исправил табуляцию кода в состоянии "state_in_reset"
  * Версия 2. 26.04.16
  * 1. Добавил выход RESET_OUT - сброс по положительному фронту.
  * 2. Добавил историю версий. Заменил табуляцию на пробелы.
  * Версия 1.
  * Нет истории измнений.
  */
module PONR(
    input CLOCK_IN,
    input PLL_LOCK_IN,
    output nRESET_OUT,
    output RESET_OUT
); 
    localparam      state_in_reset = 1'b0,
                    state_out_reset = 1'b1;
    reg [7:0] reset_cnt;
    reg nRESET_REG;
    reg RESET_REG;
    reg state;    
    assign nRESET_OUT = nRESET_REG;
    assign RESET_OUT = RESET_REG;
    always @(posedge CLOCK_IN)
        begin
            if (reset_cnt != 8'hAA)
                begin
                    reset_cnt <= reset_cnt + 1'b1;
                    nRESET_REG <= 1'b0;
                    RESET_REG <= 1'b1;  
                    state <= state_in_reset;                    
                end            
            else   
                case (state)
                    state_in_reset:
                        begin
                            if (PLL_LOCK_IN)
                                state <= state_out_reset;
                            nRESET_REG <= 1'b0;
                            RESET_REG <= 1'b1;
                        end
                    state_out_reset:
                        begin
                            nRESET_REG <= 1'b1;
                            RESET_REG <= 1'b0;
                        end
                endcase
        end
endmodule

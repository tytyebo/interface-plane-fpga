`include "defines.svh"
`include "interfaces.svh"
module sequencer(input bit nrst, clk,

        input logic spi_clk,
        output logic spi_miso,
        input logic spi_mosi,
        input logic spi_ncs,

        wishbone_io.master wbm,
        wishbone_io.slave wbs,

        input logic rec_cmd, // Active is 1
        
        output logic irq1,
        output logic irq2,
        
        input logic [7:0] rec_num,
        output logic sett_upload,
        output logic rec_cmd_ena,
        output logic rec_state);

    localparam ST_IDLE                      = 0,
               ST_PRECHARGE                 = 1,
               ST_WORD_LL                   = 5,
               ST_WORD_LH                   = 4,
               ST_WORD_HL                   = 3,
               ST_WORD_HH                   = 2,
               ST_WR_ARBITER                = 6,
               ST_ARB_RD_STATUS             = 7,
               ST_ARB_CHECK_STATUS          = 8,
               ST_GET_CFHL_FIFO_STATUS      = 9,
               ST_CHECK_CFHL_FIFO_STATUS    = 10,    
               ST_WR_DATA                   = 11,
               ST_WR_FEATURES               = 12,
               ST_WR_SEC_CNT                = 13,
               ST_WR_SEC_NUM                = 14,
               ST_WR_CYLINDER               = 15,
               ST_WR_HEAD                   = 16,
               ST_WR_ATA_CMD                = 17,
               ST_WR_CF_CMD                 = 18;

    localparam ST_INIT_IDLE                 = 0,
               ST_INIT_WAIT_READY           = 1,
               ST_INIT_FEATURES             = 2,
               ST_INIT_SEC_CNT              = 3,
               ST_INIT_SEC_NUM              = 4,
               ST_INIT_CYLINDER             = 5,
               ST_INIT_HEAD                 = 6,
               ST_INIT_ATA_CMD              = 7,
               ST_INIT_CF_CMD               = 8,
               ST_INIT_CMD_COMPLETE         = 9,
               ST_INIT_WAIT_CONFIG_DATA     = 10,
               ST_INIT_WAIT_INIT_CMD        = 11,
               ST_INIT_ANSWER_TO_INIT_CMD   = 12,
               ST_INIT_GET_CONF_DATA        = 13,
               ST_INIT_WR_PK1_SETT          = 14,
               ST_INIT_WR_PK2_SETT          = 15,
               ST_INIT_WR_ORBITA_SETT1      = 16,
               ST_INIT_WR_ORBITA_SETT2      = 17,
               ST_INIT_WR_MKO_SETT          = 18,
               ST_INIT_WR_CFHL_SETT1        = 19,
               ST_INIT_WR_CFHL_SETT2        = 20,
               ST_INIT_START_RESET          = 21,
               ST_INIT_STOP_RESET           = 22,
               ST_INIT_CHECK_RESET          = 23;  
               
    localparam [1:0] ST_ARB_MKO             = 1,
                     ST_ARB_PK1             = 2,
                     ST_ARB_PK2             = 3,
                     ST_ARB_OFF             = 0;
               
    localparam WB_CONF_WORDS_CNT            = 9'd256;
    localparam WB_ADR_CFHL_FIFO             = 16'h2008;
        
    localparam ST_WB_TX_IDLE                = 1'd0;
    localparam ST_WB_TX_ACK                 = 1'd1;
    
    //Wishbone registers
    logic [15:0] wb_adr_o_ext;
    logic [31:0] wb_dat_o_ext;
    logic wb_stb_o_ext;
    logic wb_cyc_o_ext;
    logic wb_we_o_ext;
    logic [15:0] wb_adr_o_init;
    logic [31:0] wb_dat_o_init;
    logic wb_stb_o_init;
    logic wb_cyc_o_init;
    logic wb_we_o_init;    
    logic [2:1] conf_sectors_cnt;    
    //SPI RX part and CF part registers 
    logic [31:0] dword;
    logic [15:0] wb_adr;
    logic wb_we;
    logic packet_full;
    logic [7:0] dword_cnt;
    logic rxb_re;
    logic rxb_rst;
    logic init_done;
    logic settings_downloaded; // флаг полной загрузки конфигурации в ПИ из CF
    logic settings_uploaded; // флаг полной загрузки конфигурации в ПД из CF
    assign sett_upload = settings_uploaded;
    logic [31:0] device_config;
    logic spi_out_buf_busy;
    //SPI TX part registers
    logic txb_wr_init;
    logic [31:0] txb_din_init;
    logic [15:0] tx_size_init;
    logic txb_rst;
    logic [7:0] txb_adr_init;
    logic conf_data_ready;
    logic cf_full;
    logic skip_packet;
    logic skip_packet_error;
    logic packet_skip_lock;
    //State machines registers and other internal registers
    logic [4:0] init_stage;
    logic [4:0] rxb_stage;
    logic wb_tx_stage;
    logic [1:0] arbiter_state;
    logic [15:0] arbiter_adr;
    logic [1:0] arbiter_try;
    logic [1:0] arbiter_status;
    logic arbiter_lock;
    logic arbiter_skip;
    
    //Wires for connected instances
    wire spi_cmd_class_init;
    wire spi_cmd_class_store;
    wire [7:0] rxb_dout;
    wire rxb_ne;
    wire tx_done;
    wire spi_clk_q, spi_mosi_q, spi_ncs_q;
    wire crc_error;
    //Internal wires

    wire [15:0] self_status = {arbiter_status, skip_packet, skip_packet_error, cf_full, crc_error, 1'b0, rec_num, ^{self_status[15:1]}};
    wire [7:0] tx_status = {6'd0, tx_nsend, conf_data_ready};
    
    assign wbm.wbm_adr = (settings_downloaded) ? wb_adr_o_ext : wb_adr_o_init;
    assign wbm.wbm_dat = (settings_downloaded) ? wb_dat_o_ext : wb_dat_o_init;
    assign wbm.wbm_stb = (settings_downloaded) ? wb_stb_o_ext : wb_stb_o_init;
    assign wbm.wbm_cyc = (settings_downloaded) ? wb_cyc_o_ext : wb_cyc_o_init;
    assign wbm.wbm_we  = (settings_downloaded) ? wb_we_o_ext : wb_we_o_init;
    assign wbm.wbm_sel = {4{wbm.wbm_cyc}};
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            rec_state <= 1'b0;
        end
        else begin
            if (rec_cmd_ena) begin
                if (dword_cnt[7:0] == 0) begin
                    rec_state <= rec_cmd;
                end
            end
            else begin
                rec_state <= 1'b1;
            end
        end
    end
/**
  * @brief  Прием команд из SPI_TOP и перепаковка их в шину WB.
            Команды: spi_cmd_class_store 
  */  
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            rxb_stage <= ST_IDLE;
            rxb_re <= 1'b0;
            rxb_rst <= 1'b0;
            dword <= 32'h00000000;
            wb_adr <= 16'h0000;
            wb_we <= 1'b0;
            packet_full <= 1'b0;
            dword_cnt <= 8'd0;

            arbiter_state <= ST_ARB_OFF;
            arbiter_adr <= 16'h0000;
            
            wb_adr_o_ext <= 16'h0000;
            wb_dat_o_ext <= 32'h00000000;
            wb_stb_o_ext <= 1'b0;
            wb_cyc_o_ext <= 1'b0;
            wb_we_o_ext <= 1'b0;
            wb_tx_stage <= 1'b0;
            arbiter_try <= 2'b0;
            arbiter_status <= 2'b0;
            arbiter_lock <= 1'b0;
            arbiter_skip <= 1'b0;
            cf_full <= 1'b0;
            skip_packet <= 1'b0;
            skip_packet_error <= 1'b0;
            packet_skip_lock <= 1'b0;
        end
        else begin
            unique case (wb_tx_stage)
                ST_WB_TX_IDLE: begin
                    if (packet_full) begin
                        wb_stb_o_ext <= 1'b1;
                        wb_cyc_o_ext <= 1'b1;
                        wb_we_o_ext <= wb_we;
                        wb_dat_o_ext <= dword;
                        wb_adr_o_ext <= wb_adr;
                        wb_tx_stage <= ST_WB_TX_ACK;                                    
                    end
                end
                ST_WB_TX_ACK: begin
                    if (wbm.wbs_ack) begin                                       
                        wb_stb_o_ext <= 1'b0;
                        wb_cyc_o_ext <= 1'b0;
                        wb_we_o_ext <= 1'b0;
                        packet_full <= 1'b0;
                        wb_tx_stage <= ST_WB_TX_IDLE;
                    end                          
                end
            endcase
                
            unique case (rxb_stage)
                ST_IDLE: begin
                    rxb_rst <= 1'b0;
                    if (spi_cmd_class_store) begin
                        rxb_stage <= ST_PRECHARGE;
                        rxb_re <= 1'b1;
                    end
                    else begin 
                        if (rxb_ne) begin
                            rxb_rst <= 1'b1;
                        end 
                    end
                end
                ST_PRECHARGE: begin
                    rxb_re <= 1'b1;
                    rxb_stage <= ST_WORD_HH;
                end
                ST_WORD_HH: begin 
                    // dword[31:24] <= rxb_dout;
                    // dword[15:8] <= rxb_dout;
                    dword[7:0] <= rxb_dout; 
                    if (dword_cnt[7:0] != 255) begin
                        rxb_re <= 1'b1;
                    end
                    else begin
                        rxb_re <= 1'b0;
                    end
                    rxb_stage <= ST_WORD_HL;
                end
                ST_WORD_HL: begin 
                    // dword[23:16] <= rxb_dout;  
                    // dword[7:0] <= rxb_dout;   
                    dword[15:8] <= rxb_dout;                            
                    if (dword_cnt[7:0] != 255) begin
                        rxb_re <= 1'b1;
                    end
                    else begin 
                        rxb_re <= 1'b0;
                    end
                    rxb_stage <= ST_WORD_LH;
                end
                ST_WORD_LH: begin 
                    rxb_re <= 1'b0;
                    if (dword_cnt[7:0] != 255) begin
                        // dword[15:8] <= rxb_dout;
                        // dword[31:24] <= rxb_dout;
                        dword[23:16] <= rxb_dout; 
                    end
                    rxb_stage <= ST_WORD_LL;
                end
                ST_WORD_LL: begin 
                    rxb_re <= 1'b0;
                    if (dword_cnt[7:0] != 255) begin
                        // dword[7:0] <= rxb_dout;
                        // dword[23:16] <= rxb_dout;
                        dword[31:24] <= rxb_dout;
                    end
                    else begin
                        //dword[15:8] <= self_status[15:8];
                        //dword[7:0] <= self_status[7:0];
                        dword[31:24] <= self_status[15:8];
                        dword[23:16] <= self_status[7:0];                                    
                    end
                    if (arbiter_lock) begin
                        rxb_stage <= ST_WR_ARBITER;
                    end
                    else begin
                        rxb_stage <= ST_ARB_RD_STATUS;
                    end
                end                        
                ST_WR_ARBITER: begin
                    if (!arbiter_skip) begin
                        if (!packet_full) begin
                            packet_full <= 1'b1;
                            wb_we <= 1'b1;    
                            wb_adr <= arbiter_adr;
                            if (packet_skip_lock) begin
                                rxb_stage <= ST_WR_DATA;
                            end
                            else begin
                                rxb_stage <= ST_GET_CFHL_FIFO_STATUS;
                            end
                        end
                    end
                    else begin
                        if (packet_skip_lock) begin
                            rxb_stage <= ST_WR_DATA;
                        end
                        else begin
                            rxb_stage <= ST_GET_CFHL_FIFO_STATUS;
                        end
                    end
                end
                ST_ARB_RD_STATUS: begin
                    if (!packet_full) begin
                        wb_we <= 1'b0;
                        unique case (arbiter_state)
                            ST_ARB_OFF: begin
                                if (pk2_ena) begin
                                    arbiter_state <= ST_ARB_PK2;
                                end
                                if (pk1_ena) begin
                                    arbiter_state <= ST_ARB_PK1;
                                end
                                if (mko_ena) begin
                                    arbiter_state <= ST_ARB_MKO;
                                end
                                if (!(pk2_ena || pk1_ena || mko_ena)) begin
                                    if (packet_skip_lock) begin
                                        rxb_stage <= ST_WR_DATA;
                                    end
                                    else begin
                                        rxb_stage <= ST_GET_CFHL_FIFO_STATUS;           
                                    end
                                end
                                arbiter_status <= ST_ARB_OFF;
                            end
                            ST_ARB_MKO: begin
                                wb_adr <= 16'h3000; //MKO
                                arbiter_adr <= 16'h3000;//MKO
                                packet_full <= 1'b1;
                                if (pk2_ena) begin
                                    arbiter_state <= ST_ARB_PK2;
                                end
                                if (pk1_ena) begin
                                    arbiter_state <= ST_ARB_PK1;
                                end
                                arbiter_try <= arbiter_try + 1'b1;  
                                rxb_stage <= ST_ARB_CHECK_STATUS;
                                arbiter_status <= ST_ARB_MKO;
                            end
                            ST_ARB_PK1: begin
                                wb_adr <= 16'h6000; //PK1
                                arbiter_adr <= 16'h6000;//PK1
                                packet_full <= 1'b1;
                                if (mko_ena) begin
                                    arbiter_state <= ST_ARB_MKO;
                                end
                                if (pk2_ena) begin
                                    arbiter_state <= ST_ARB_PK2;
                                end
                                arbiter_try <= arbiter_try + 1'b1;
                                rxb_stage <= ST_ARB_CHECK_STATUS;
                                arbiter_status <= ST_ARB_PK1;
                            end
                            ST_ARB_PK2: begin
                                wb_adr <= 16'h7000; //PK2
                                arbiter_adr <= 16'h7000;//PK2
                                packet_full <= 1'b1;
                                if (pk1_ena) begin
                                    arbiter_state <= ST_ARB_PK1;
                                end
                                if (mko_ena) begin
                                    arbiter_state <= ST_ARB_MKO;  
                                end
                                arbiter_try <= arbiter_try + 1'b1;
                                rxb_stage <= ST_ARB_CHECK_STATUS;
                                arbiter_status <= ST_ARB_PK2;
                            end
                        endcase
                    end
                end
                ST_ARB_CHECK_STATUS: begin
                    if (wbm.wbs_ack) begin
                        if (wbm.wbs_dat == 32'd0) begin // it's mean READY
                            arbiter_lock <= 1'b1;
                            rxb_stage <= ST_WR_ARBITER;
                            arbiter_try <= 2'b00;
                        end
                        else begin
                            if (arbiter_try == 2'b11) begin
                                arbiter_skip <= 1'b1;
                                if (packet_skip_lock) begin
                                    rxb_stage <= ST_WR_DATA;
                                end
                                else begin
                                    rxb_stage <= ST_GET_CFHL_FIFO_STATUS;
                                end
                                arbiter_try <= 2'b00;
                                arbiter_status <= 2'b00;
                            end
                            else begin
                                rxb_stage <= ST_ARB_RD_STATUS;
                            end
                        end
                    end
                end
                ST_GET_CFHL_FIFO_STATUS: begin
                    if (!packet_full) begin
                        packet_full <= 1'b1;
                        wb_we <= 1'b0;
                        wb_adr <= 16'h2008;
                        rxb_stage <= ST_CHECK_CFHL_FIFO_STATUS;                           
                    end
                end
                ST_CHECK_CFHL_FIFO_STATUS: begin
                    skip_packet_error <= skip_packet;
                    if (wbm.wbs_ack) begin
                        if (wbm.wbs_dat[13:0] > 13'd7936 && cf_ena) begin// it's mean that fifo is full
                            skip_packet <= 1'b1;
                        end
                        else begin
                            skip_packet <= 1'b0;
                        end                                
                        if (wbm.wbs_dat[15]) begin
                            cf_full <= 1'b1;
                        end                    
                        packet_skip_lock <= 1'b1;                                   
                        rxb_stage <= ST_WR_DATA;
                    end
                end
                ST_WR_DATA: begin
                    rxb_re <= 1'b0; 
                    if (cf_ena && rec_state) begin
                        if (!packet_full) begin
                            packet_full <= (~skip_packet) ? 1'b1 : 1'b0;
                            wb_adr <= WB_ADR_CFHL_FIFO;
                            wb_we <= 1'b1;
                            dword_cnt <= dword_cnt + 1'b1;
                            if (dword_cnt[7:0] != 255) begin
                                rxb_re <= 1'b1;
                                rxb_stage <= ST_PRECHARGE;
                            end                                            
                            else begin
                                arbiter_lock <= 1'b0;
                                arbiter_skip <= 1'b0;
                                packet_skip_lock <= 1'b0;
                                rxb_stage <= ST_IDLE;
                            end
                        end
                    end
                    else begin
                        rxb_stage <= ST_PRECHARGE;
                        dword_cnt <= dword_cnt + 1'b1;
                        if (dword_cnt[7:0] == 255) begin
                            rxb_stage <= ST_IDLE;
                            arbiter_lock <= 1'b0;
                            arbiter_skip <= 1'b0;
                            packet_skip_lock <= 1'b0;
                        end
                        else begin
                            rxb_re <= 1'b1;
                        end
                    end
                end  
            endcase  
        end
    end

/**     
  * @brief  Данный STATE MACHINE читает сектор конфигурации из CF и по запросу от ПД выдает в SPI конфигурационные данные.
  */     
    logic [7:0] conf_rd_dword_cnt;
    logic [7:0] init_rst_cnt;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            init_stage <= ST_INIT_IDLE;
            settings_downloaded <= 1'b0;
            settings_uploaded <= 1'b0;
            init_done <= 1'b0;
            wb_adr_o_init <= 16'h0000;
            wb_dat_o_init <= 32'h0000_0080;
            wb_cyc_o_init <= 1'b0;
            wb_stb_o_init <= 1'b0;
            wb_we_o_init <= 1'b0;
            device_config <= 32'h0000_0000;
            txb_rst <= 1'b0;
            conf_sectors_cnt <= 2'd0;
            conf_data_ready <= 1'b0;
            txb_din_init <= 32'h0000_0000;
            txb_wr_init <= 1'b0;
            tx_size_init <= 16'h0000;
            txb_adr_init <= 8'd0;    

            spi_out_buf_busy <= 1'b0;      
            conf_rd_dword_cnt <= 8'd0;   
            init_rst_cnt <= 8'd0;            
        end
        else begin
            txb_wr_init <= 1'b0;
            txb_rst <= 1'b0;
            if (spi_cmd_class_init) begin
                if (init_done) begin
                    init_stage <= ST_INIT_START_RESET;
                    settings_downloaded <= 1'b0;
                    settings_uploaded <= 1'b0;
                    init_done <= 1'b0;
                    tx_size_init <= 16'h0000;
                    conf_sectors_cnt <= 2'd0;
                    spi_out_buf_busy <= 1'b0;
                    conf_data_ready <= 1'b0;
                    init_rst_cnt <= 8'd0;
                end
            end
                    
            if (txb_wr_init) begin
                txb_adr_init <= txb_adr_init + 1'b1;
            end      
            if (spi_out_buf_busy &&  tx_done) begin
                spi_out_buf_busy <= 1'b0;
            end                    
            if (settings_downloaded & tx_done) begin
                settings_uploaded <= 1'b1;
            end
                
            unique case (init_stage)
                ST_INIT_IDLE: begin 
                    txb_adr_init <= 8'h00;
                    if (!settings_downloaded && spi_cmd_class_init) begin
                        init_stage <= ST_INIT_WAIT_READY;
                    end
                end
                ST_INIT_WAIT_READY: begin
                    wb_adr_o_init <= 16'h2000;
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b0;
                        
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0; 
                        if (wbm.wbs_dat[0]) begin
                            init_stage <= ST_INIT_FEATURES;
                        end
                    end
                end
                ST_INIT_FEATURES: begin               
                    wb_adr_o_init <= 16'h2001;
                    wb_dat_o_init <= 32'h0000_0000;
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1;
                    init_stage <= ST_INIT_SEC_CNT;
                end
                ST_INIT_SEC_CNT: begin
                    if (wbm.wbs_ack) begin
                        wb_adr_o_init <= 16'h2002;
                        wb_dat_o_init <= 32'h0000_0008;
                        init_stage <= ST_INIT_SEC_NUM;
                    end
                end
                ST_INIT_SEC_NUM: begin
                    if (wbm.wbs_ack) begin
                        wb_adr_o_init <= 16'h2003;
                        wb_dat_o_init <= 32'h0000_0000;
                        init_stage <= ST_INIT_CYLINDER;
                    end
                end
                ST_INIT_CYLINDER: begin
                    if (wbm.wbs_ack) begin
                        wb_adr_o_init <= 16'h2004;
                        wb_dat_o_init <= 32'h0000_0000;
                        init_stage <= ST_INIT_HEAD;
                    end
                end                    
                ST_INIT_HEAD: begin
                    if (wbm.wbs_ack) begin
                        wb_adr_o_init <= 16'h2005;
                        wb_dat_o_init <= 32'h0000_00e0;
                        init_stage <= ST_INIT_ATA_CMD;
                    end
                end   
                ST_INIT_ATA_CMD: begin
                    if (wbm.wbs_ack) begin
                        wb_adr_o_init <= 16'h2006;
                        wb_dat_o_init <= 32'h0000_0020;
                        init_stage <= ST_INIT_CF_CMD;
                    end
                end   
                ST_INIT_CF_CMD: begin
                    if (wbm.wbs_ack) begin
                        wb_adr_o_init <= 16'h2007;
                        wb_dat_o_init <= 32'h0000_0009;
                        init_stage <= ST_INIT_CMD_COMPLETE;
                    end
                end     
                ST_INIT_CMD_COMPLETE: begin
                    if (wbm.wbs_ack) begin
                        init_stage <= ST_INIT_WAIT_CONFIG_DATA;
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                    end
                end
                ST_INIT_WAIT_CONFIG_DATA: begin
                    wb_adr_o_init <= 16'h200A;
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b0;
                            
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0; 
                        if ((wbm.wbs_dat[9:0] >= WB_CONF_WORDS_CNT) && !spi_out_buf_busy) begin
                            spi_out_buf_busy <= 1'b1;
                            if (!conf_data_ready) begin
                                init_stage <= ST_INIT_WAIT_INIT_CMD;
                            end
                            else begin
                                init_stage <= ST_INIT_GET_CONF_DATA;
                                wb_adr_o_init <= 16'h2009;                          
                                wb_cyc_o_init <= 1'b1;
                                wb_stb_o_init <= 1'b1;
                            end
                        end
                    end    
                end
                ST_INIT_WAIT_INIT_CMD: begin
                    if (spi_cmd_class_init) begin
                        conf_data_ready <= 1'b1;
                    end
                    if (!spi_cmd_class_init && conf_data_ready) begin                      
                        init_stage <= ST_INIT_ANSWER_TO_INIT_CMD;
                    end
                end
                ST_INIT_ANSWER_TO_INIT_CMD: begin
                    if (spi_cmd_class_init) begin
                        init_stage <= ST_INIT_GET_CONF_DATA;
                        wb_adr_o_init <= 16'h2009;                          
                        wb_cyc_o_init <= 1'b1;
                        wb_stb_o_init <= 1'b1;
                    end
                end
                ST_INIT_GET_CONF_DATA: begin
                    if (wbm.wbs_ack) begin
                        if (!init_done) begin
                            device_config <= wbm.wbs_dat;
                            init_done <= 1'b1;
                        end
                        conf_rd_dword_cnt <= conf_rd_dword_cnt + 1'b1;                                    
                        txb_din_init <= wbm.wbs_dat;
                        txb_wr_init <= 1'b1;
                        if (conf_rd_dword_cnt == WB_CONF_WORDS_CNT - 1'b1) begin                                          
                            wb_cyc_o_init <= 1'b0;
                            wb_stb_o_init <= 1'b0;
                            init_stage <= ST_INIT_WAIT_CONFIG_DATA;
                            tx_size_init <= 1024;
                            if (conf_sectors_cnt == 2'b11) begin
                                init_stage <= ST_INIT_WR_PK1_SETT;
                                conf_data_ready <= 1'b0;
                            end                  
                            conf_sectors_cnt <= conf_sectors_cnt + 1'b1;
                        end
                    end
                end
                ST_INIT_WR_PK1_SETT: begin
                    wb_adr_o_init <= 16'h6001;
                    wb_dat_o_init <= {28'd0, pk1_test, pk1_set[2:0]};
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1;
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_WR_PK2_SETT;
                    end
                end
                ST_INIT_WR_PK2_SETT: begin
                    wb_adr_o_init <= 16'h7001;
                    wb_dat_o_init <= {28'd0, pk2_test, pk2_set[2:0]};
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1;                            
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_WR_ORBITA_SETT1;
                    end
                end
                ST_INIT_WR_ORBITA_SETT1: begin
                    wb_adr_o_init <= 16'h1000;
                    wb_dat_o_init <= {27'd0, rec_cmd_ena, orbita_set};
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1; 
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_WR_ORBITA_SETT2;
                    end
                end
                ST_INIT_WR_ORBITA_SETT2: begin
                    wb_adr_o_init <= 16'h1001;
                    wb_dat_o_init <= {4'h00, cf_size};
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1; 
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_WR_MKO_SETT;
                    end
                end
                ST_INIT_WR_MKO_SETT: begin
                    wb_adr_o_init <= 16'h3001;
                    wb_dat_o_init <= {31'd0, mko_test};
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1;
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_WR_CFHL_SETT1;
                    end
                end
                ST_INIT_WR_CFHL_SETT1: begin
                    wb_adr_o_init <= 16'h200D;
                    wb_dat_o_init <= {4'h00, cf_size};
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1; 
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_WR_CFHL_SETT2;
                    end
                end
                ST_INIT_WR_CFHL_SETT2: begin
                    wb_adr_o_init <= 16'h2007;
                    wb_dat_o_init <= 32'd3;
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1;
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        settings_downloaded <= 1'b1;    
                        init_stage <= ST_INIT_IDLE;
                    end
                end
                ST_INIT_START_RESET: begin
                    wb_adr_o_init <= 16'h200C;
                    wb_dat_o_init <= 32'd1;
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1;
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_STOP_RESET;
                    end
                end
                ST_INIT_STOP_RESET: begin
                    wb_adr_o_init <= 16'h200C;
                    wb_dat_o_init <= 32'd0;
                    wb_cyc_o_init <= 1'b1;
                    wb_stb_o_init <= 1'b1;
                    wb_we_o_init <= 1'b1;
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0;
                        wb_we_o_init <= 1'b0;
                        init_stage <= ST_INIT_CHECK_RESET;
                    end
                end
                ST_INIT_CHECK_RESET: begin
                    init_rst_cnt <= init_rst_cnt + 1'b1;
                    if (init_rst_cnt == 8'd255) begin
                        wb_adr_o_init <= 16'h200A;
                        wb_cyc_o_init <= 1'b1;
                        wb_stb_o_init <= 1'b1;
                        wb_we_o_init <= 1'b0;
                    end
                                
                    if (wbm.wbs_ack) begin
                        wb_cyc_o_init <= 1'b0;
                        wb_stb_o_init <= 1'b0; 
                        if (wbm.wbs_dat[9:0] != 10'd0) begin
                            init_stage <= ST_INIT_START_RESET;
                        end
                        else begin
                            init_stage <= ST_INIT_IDLE;
                        end
                    end
                end
            endcase                    
        end
    end
    spi_top spi_top(.nrst(nrst),
                    .clk(clk),
                    
                    .spi_clk(spi_clk_q),
                    .spi_miso(spi_miso),
                    .spi_mosi(spi_mosi_q),
                    .spi_ncs(spi_ncs_q),

                    .txb_din(txb_din),
                    .txb_wr(txb_wr),
                    .txb_rst(txb_rst),  
                    .tx_size(tx_size),
                    .txb_adr(txb_adr),
                    .tx_done(tx_done),
                    .tx_status(tx_status),
                    .tx_ready(tx_nsend),
                    
                    .rxb_dout(rxb_dout),
                    .rxb_re(rxb_re),
                    .rxb_rst(rxb_rst),
                    .rxb_ne(rxb_ne),	
                    
                    .rx_cmd_class_store(spi_cmd_class_store),
                    .rx_cmd_class_init(spi_cmd_class_init),
                    
                    .irq1(irq1),
                    .irq2(irq2),
                    .crc_error(crc_error));   
    metastable_chain#(.CHAIN_DEPTH(2), .BUS_WIDTH(3))
    metastable_chain_spi(.clk(clk),
                        .nrst(1'b1),
                        .din({spi_clk, spi_mosi, spi_ncs}),
                        .dout({spi_clk_q, spi_mosi_q, spi_ncs_q}));  

    /**
    * @brief  Технологический USB -> SPI.
    */   
    wire usb2spi = (settings_uploaded) ? 1'b1 : 1'b0;
    logic [31:0] txb_din_usb;
    logic txb_wr_usb;
    logic [15:0] tx_size_usb;
    logic [7:0] txb_adr_usb;
    logic [2:0] tx_cnt;
    logic [15:0] tx_done_cnt;
    logic tx_nsend;
    logic s_ack_o;
    logic [31:0] s_dat_o;
    
    wire [31:0] txb_din;
    wire txb_wr;
    wire [15:0] tx_size;
    wire [7:0] txb_adr;
    
    assign txb_din = (usb2spi) ? txb_din_usb : txb_din_init;
    assign txb_wr = (usb2spi) ? txb_wr_usb : txb_wr_init;
    assign tx_size = (usb2spi) ? tx_size_usb : tx_size_init;
    assign txb_adr  = (usb2spi) ? txb_adr_usb : txb_adr_init;
    
    assign wbs.wbs_dat = s_dat_o;
    assign wbs.wbs_ack = s_ack_o;

    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin            
            txb_din_usb <= 32'h0000_0000;
            txb_wr_usb <= 1'b0;
            tx_size_usb <= 16'd0;
            txb_adr_usb <= 8'h00;    
            tx_cnt <= 3'd0;  
            tx_done_cnt <= 16'h0000;
            tx_nsend <= 1'b0;
            s_dat_o <= 32'd0;
            s_ack_o <= 1'b0;
        end
        else begin
            s_ack_o <= 1'b0;
            txb_wr_usb <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                s_ack_o <= 1'b1;
                if (wbs.wbm_we) begin
                    txb_din_usb <= wbs.wbm_dat; //Т.к. память двухпортовая 32 разряда в 8 разрядов,
                                              //то данные будут выдаваться с 8 разрядного порта начиная со старшего байта 32 разрядного слова.
                                              //Поэтому необходимо разворачивать данные заранее.
                    
                    txb_adr_usb <= wbs.wbm_adr[7:0];
                    txb_wr_usb <= 1'b1;
                    tx_cnt <= tx_cnt + 1'b1;
                end
                else begin
                    case (wbs.wbm_adr[15])
                        1'b0:
                            s_dat_o <= txb_din_usb;
                        1'b1:
                            s_dat_o <= {15'd0, tx_nsend, tx_done_cnt};
                    endcase
                end
            end
            if (tx_cnt == 3'd4) begin
                tx_size_usb <= 16'd16;
                tx_nsend <= 1'b1;
                tx_cnt <= 3'd0; 
            end
            if (usb2spi && tx_done) begin
                tx_done_cnt <= tx_done_cnt + 1'b1;
                tx_size_usb <= 16'd0;
                tx_nsend <= 1'b0;
            end
        end
    end   
    
    
    logic [27:0] cf_size;
    logic [3:0] pk1_set;
    logic [3:0] pk2_set;
    logic [3:0] orbita_set;
    logic pk1_ena;
    logic pk2_ena;
    logic mko_ena;
    logic cf_ena;

    logic pk1_test;
    logic pk2_test;
    logic mko_test;    
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            cf_size <= 28'd0;
            pk1_set <= 4'd0;
            pk2_set <= 4'd0;
            orbita_set <= 4'd0;
            rec_cmd_ena <= 1'b0;
            pk1_ena <= 1'b0;
            pk2_ena <= 1'b0;
            mko_ena <= 1'b0;
            cf_ena <= 1'b0;
            pk1_test <= 1'b0;
            pk2_test <= 1'b0;
            mko_test <= 1'b0;
        end
        else begin
            if (init_done) begin                      
                rec_cmd_ena <= device_config[0];                        
                pk1_set <= device_config[11:8]; 
                pk2_set <= device_config[23:20];
`ifndef NEW_MODES 
                orbita_set <= (device_config[5]) ? device_config[15:12] : 4'b0000; // device_config[5] = mode2
`else
                orbita_set <= (device_config[5] || device_config[4]) ? device_config[15:12] : 4'b0000; // device_config[5] = mode2 // device_config[4] = mode1
`endif
                
                pk1_test <= 1'b0;
                pk2_test <= 1'b0;
                mko_test <= 1'b0;
                                        
                unique case (device_config[7:4])
                    4'b0001: begin // Mode A                                    
                        pk1_ena <= 1'b1;
                        pk2_ena <= 1'b1;
                        mko_ena <= 1'b1;
`ifndef NEW_MODES                        
                        cf_ena <= 1'b1;
`else
                        cf_ena <= 1'b0;
`endif
                    end
                    4'b0010: begin // Mode B
                        cf_ena <= 1'b0;
`ifndef NEW_MODES
                        pk1_ena <= 1'b0;
                        pk2_ena <= 1'b0;
                        mko_ena <= 1'b1;

`else
                        pk1_ena <= 1'b1;
                        pk2_ena <= 1'b1;
                        mko_ena <= 1'b0;                       
`endif
                    end
                    4'b0100: begin // Mode C                                    
                        pk1_ena <= 1'b0;
                        pk2_ena <= 1'b0;
                        mko_ena <= 1'b0;
                        cf_ena <= 1'b1;
                    end
                    4'b1000: begin // Mode D                                     
                        pk1_ena <= device_config[28] & (~device_config[24]);
                        pk2_ena <= device_config[29] & (~device_config[25]);
                        mko_ena <= device_config[30] & (~device_config[26]);
                        cf_ena <= (device_config[27]) ? 1'b0 : device_config[31];
                        
                        pk1_test <= device_config[24];
                        pk2_test <= device_config[25];
                        mko_test <= device_config[26];
                        orbita_set <= (device_config[27]) ? device_config[15:12] : 4'b0000; // device_config[27] = orbita_test
                    end
                    default: begin
                        pk1_ena <= 1'b0;
                        pk2_ena <= 1'b0;
                        mko_ena <= 1'b0;
                        cf_ena <= 1'b0;
                    end
                endcase
                            
                unique case (device_config[19:16])
                    4'd0:  
                        cf_size <= 28'h07F_FF80;
                    4'd1:  
                        // cf_size <= 28'h0FF_FF80;
                        cf_size <= `CF_16GB_ADR_SIZE; // 28'h01DC_D650; - true size
                    4'd2:  
                        cf_size <= 28'h1FF_FF80;
                    4'd3:  
                        cf_size <= 28'h3FF_FF80;
                    4'd4:  
                        cf_size <= 28'h7FF_FF80;
                    default:    
                        cf_size <= 28'd0;
                endcase 
            end
            else begin
                cf_size <= 28'd0;
                pk1_set <= 4'd0;
                pk2_set <= 4'd0;
                orbita_set <= 4'd0;
                rec_cmd_ena <= 1'b0;
                pk1_ena <= 1'b0;
                pk2_ena <= 1'b0;
                mko_ena <= 1'b0;
                cf_ena <= 1'b0;
            end
        end
    end
endmodule: sequencer

1. От SPI приходит: 0x87 0x22 0x00 0x0K 0xF6 0x03 0x00 0x13 (например).
В фифо spi_top запишется в таком же байтовом порядке.

2. При передаче по Wishbone контроллер sequencer развернет данные в 32 рязрядные слова следующим образом:
1е слово: 0x00 0x0K 0x87 0x22
2е слово: 0x00 0x13 0xF6 0x03

3. В cf_controller 32 разрядные слова запишутся в 2 портовую память, из которой будут читаться по 16 разрядов следующим образом:
1е слово: 0x87 0x22 0x00 0x0K
2е слово: 0xF6 0x03 0x00 0x13
Т.е. из памяти читается сначала младшая часть 32 разрядного слова.

4. usb2wb контроллер читает 32 разрядные слова по шине Wishbone из compact flash, которая выдает только 16 разрядные слова.
После чего проиходит упаковка данных вида 0x00 0x00 0xAA 0xBB в вид 0x00 0x00 0xBB 0xAA.
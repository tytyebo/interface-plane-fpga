`timescale 1ps/1ps
module tb_sequencer();
/**
  * @brief  Описание регистров.
  */  
    reg nrst;
    reg clk;
    reg [15:0] cnt = 16'h0000;
    reg wb_ack;
    wire wb_stb;
    reg wb_stb_d;
    reg wb_stb_dd;
    reg wb_stb_ddd;
    reg wb_stb_dddd;
    
    wire spi_clk;
    wire spi_miso;
    wire spi_mosi;
    reg spi_ncs;

    wire [7:0]cr;
    wire [7:0]sr;

    reg spi_byte_start;

    reg [7:0]tx_buffer;
    wire [7:0]rx_buffer;
    wire spi_txc;
    wire [15:0] wb_adr_o;

    
    reg [7:0] tx_header;
    reg [7:0] tx_type;
    reg [7:0] txb_din;
    reg txb_wr;
    
    reg [31:0] wb_dat_i;
    reg [15:0] wb_adr;
    
    reg cf_ctrl_ready;
    


    
    assign cr = {spi_byte_start, 7'b111_1111};
    assign spi_txc = sr[3]; 
    
/**
  * @brief  Чтение в массива настроек из файла. Эмуляция чтения настроек из CompactFlash.
  */   
    integer file_id;
    integer i;
    reg [31:0] config_words[0:1023];
    initial 
        begin    
            file_id = $fopen("settings_01.hex", "rb");
            for (i = 0; i < 4096/4; i = i + 1)
                begin
                    $fread(config_words[i], file_id, i*32, 32);
                    $display("%d", i);
                    $display("%h\n", config_words[i]);
                end
            $fclose(file_id);        
        end
        
    reg [7:0] cmd03[0:1023+8];
    initial 
        begin    
            file_id = $fopen("cmd03_01.hex", "rb");
            for (i = 0; i < 1030; i = i + 1)
                begin
                    $fread(cmd03[i], file_id, i*8, 8);
                    $display("%h", cmd03[i]);
                end
            $fclose(file_id);        
        end   
    reg [7:0] cmd10[0:1021];
    initial 
        begin    
            file_id = $fopen("cmd10.hex", "rb");
            for (i = 0; i < 1022; i = i + 1)
                begin
                    $fread(cmd10[i], file_id, i*8, 8);
                    $display("%h", cmd10[i]);
                end
            $fclose(file_id);        
        end   

/**
  * @brief  Выработка сигнала сброса "nrst".
  */     
    initial 
        begin
            nrst <= 0;
            cf_ctrl_ready <= 0;
            #1000
            nrst <= 1;
            #1000
            cf_ctrl_ready <= 1;
        end
/**
  * @brief  Формирование сигнала тактовой частоты "clk".
  */ 
    initial 
        begin
            clk <= 0;
            forever 
                begin
                    #10 clk <= ~clk;
                end
            end
/**
  * @brief  Task для записи байта информации в модуль SPI.
  */             
    task spi_send();
        begin
            spi_byte_start <= 1;
            @(posedge clk);
            spi_byte_start <= 0;
            @spi_txc;
            @(posedge clk);
        end
    endtask

/**
  * @brief  Эмулятор ответа по шине Wishbone на запрос конфигурации из CompactFlash.
  */     
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                wb_ack <= 1'b0;
                wb_adr <= 16'h2000;
                wb_dat_i <= 32'h12345600;
            end
        else 
            begin
                wb_ack <= 1'b0;
                if (~wb_ack)
                    begin
                        wb_stb_d <= wb_stb;
                        wb_stb_dd <= wb_stb_d;
                        wb_stb_ddd <= wb_stb_dd;
                        wb_stb_dddd <= wb_stb_ddd;
                        if (wb_stb_d) //количество d определяет задержку ответа по шине
                            begin
                                wb_ack <= 1'b1;
                                wb_stb_d <= 1'b0;
                                wb_stb_dd <= 1'b0;
                                wb_stb_ddd <= 1'b0;
                                wb_stb_dddd <= 1'b0;
                                if (wb_adr_o == wb_adr)
                                    begin
                                        wb_dat_i <= config_words[wb_adr - 16'h2000];
                                        wb_adr <= wb_adr + 1'b1;
                                    end                                     
                            end
                    end  
            end
    task spi_send_config_command();
        begin
            spi_ncs <= 1'b0;
            repeat(5)@(posedge clk);        
            tx_buffer <= 8'h87;
            spi_send();
            tx_buffer <= 8'h02;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h67;
            spi_send();
            tx_buffer <= 8'hAC;
            spi_send();
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(500)@(posedge clk);
            /*             
            spi_ncs <= 1'b0;
            repeat(5)@(posedge clk);        
            tx_buffer <= 8'h87;
            spi_send();
            tx_buffer <= 8'h02;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h00;
            spi_send();
            tx_buffer <= 8'h67;
            spi_send();
            tx_buffer <= 8'hAC;
            spi_send();
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(50)@(posedge clk); */
            cnt <= 0;
            spi_ncs <= 0;   
            tx_buffer <= 8'h00;
            repeat(5)@(posedge clk);            
            repeat(1024+8)
                begin
                    tx_buffer <= cmd03[cnt];
                    cnt = cnt + 1'b1;
                    spi_send();
                end
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(50)@(posedge clk);
            cnt <= 0;
            repeat(500)@(posedge clk);
            
            
            spi_ncs <= 0;   
            tx_buffer <= 8'h00;
            repeat(5)@(posedge clk);            
            repeat(1024+8)
                begin
                    tx_buffer <= cmd03[cnt];
                    cnt = cnt + 1'b1;
                    spi_send();
                end
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(50)@(posedge clk);
             cnt <= 0;
            repeat(500)@(posedge clk);
            
            
            spi_ncs <= 0;   
            tx_buffer <= 8'h00;
            repeat(5)@(posedge clk);            
            repeat(1024+8)
                begin
                    tx_buffer <= cmd03[cnt];
                    cnt = cnt + 1'b1;
                    spi_send();
                end
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(50)@(posedge clk);
             cnt <= 0;
            repeat(500)@(posedge clk);
            
            
            spi_ncs <= 0;   
            tx_buffer <= 8'h00;
            repeat(5)@(posedge clk);            
            repeat(1024+8)
                begin
                    tx_buffer <= cmd03[cnt];
                    cnt = cnt + 1'b1;
                    spi_send();
                end
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(50)@(posedge clk);
        end
        
    endtask    
    
    task spi_write_data_command();
        begin
            cnt <= 0;
            spi_ncs <= 0;   
            tx_buffer <= 8'h00;
            repeat(5)@(posedge clk);            
            repeat(1022)
                begin
                    tx_buffer <= cmd10[cnt];
                    cnt = cnt + 1'b1;
                    spi_send();
                end
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(50)@(posedge clk);
             cnt <= 0;
            repeat(500)@(posedge clk);
            
            
            spi_ncs <= 0;   
            tx_buffer <= 8'h00;
            repeat(5)@(posedge clk);            
            repeat(1022)
                begin
                    tx_buffer <= cmd10[cnt];
                    cnt = cnt + 1'b1;
                    spi_send();
                end
            repeat(5)@(posedge clk);
            spi_ncs <= 1;
            repeat(50)@(posedge clk);
             cnt <= 0;
            repeat(500)@(posedge clk);
        end
    endtask
      
            
    initial begin
        spi_ncs <= 1;
        spi_byte_start <= 0;
        @(posedge nrst);
        repeat(5)@(posedge clk);
        
        spi_send_config_command();
        spi_write_data_command();
         
            
    end   

/**
  * @brief  Подключенные модули.
  */     
    sequencer seq0(
        .nrst(nrst),
        .clk(clk),
        
        .spi_clk (spi_clk),
        .spi_miso(spi_miso),
        .spi_mosi(spi_mosi),
        .spi_ncs (spi_ncs),
        
        .wb_adr_o(wb_adr_o),
        .wb_dat_o(),	
        .wb_stb_o(wb_stb),
        .wb_cyc_o(),
        .wb_sel_o(),
        .wb_we_o(),	
        .wb_dat_i(wb_dat_i),
        .wb_ack_i(wb_ack),
        .cf_ctrl_ready(cf_ctrl_ready)
    );
    
    spi_master_slave spi_tester
        (
            .rst(nrst),
            .clk(clk),
          
            .spi_clk (spi_clk),
            .spi_miso(spi_miso),
            .spi_mosi(spi_mosi),

            .cr(cr),
            .sr(sr),
          
            .timeout(10),
            .div(2),
          
            .tx_buffer(tx_buffer),
            .rx_buffer(rx_buffer)
        );
endmodule

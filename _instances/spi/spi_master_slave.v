//SPI master/slave controller
//Notes: 
//	1. In slave mode receiver, whith cpol = 1, there is no way for slave to detect
// last edge of last clock (it's merged with inactive clock line state). So RXC
//	is set half cycle before completion.
// 2. In slave mode trasmiter will hold MISO line in the state of last bit transmitted.
// New data will become visible one internal clock after first edge of next transaction.
// 3. In master mode TXC and RXC are time delimited (by half cycle), in slave mode
// TXC and RXC are set simultaneously
//
//Status register bits:
//-
//-
//CLK_DET- [slave only] clock detector, set to one if spi_clk input change detectet, set to zero otherwise
//Timeout- [slave only] timeout appeared since transfer started, e.g. clock was held high for too long
//TXC		- set when whole byte is transmited and CLK line returned to idle state
//RXC		- set when whole byte is received, e.g. half cycle BEFORE end of transaction
//					(since last edge of last clock cycle is invisible to slave)
//Busy
//Enabled
//
//Control register bits:
//Start
//Clock Phase - NOT IMPLEMETED
//Clock Polarity
//MSB First
//Timeout Ack - reset timeout status bit
//-
//Master
//Enabled
`timescale 1ps/1ps
module spi_master_slave(
	input clk,
	input rst,//active low
	
	inout spi_clk,
	inout spi_miso,
	inout spi_mosi,
	
	//output wire master,
	
	//control
	input [7:0]cr,
	input [7:0]div,
	input [7:0]timeout,
	output [7:0]sr,
	//data
	input [7:0]tx_buffer,
	output [7:0]rx_buffer
	//output reg[7:0]slave_rx_buffer,
	//output reg[7:0]master_rx_buffer
);

wire enabled;
wire rsti = rst & enabled;
//[CR] Mode selection register, 1 => Master, 0 => Slave
wire spi_mode_master;
//assign master = spi_mode_master;

//CLK: output in master mode, input in slave mode
reg spi_clk_out;
assign spi_clk = spi_mode_master == 1'b1 ? spi_clk_out : 1'bz;
wire spi_clk_in = spi_clk;

//MISO: Master input (z), Slave output (reg)
reg spi_slave_output;
assign spi_miso = spi_mode_master == 1'b1 ? 1'bz : spi_slave_output;
wire spi_master_input = spi_miso;

//MOSI: Master output, Slave input (z)
reg spi_master_output;
assign spi_mosi = spi_mode_master == 1'b1 ? spi_master_output : 1'bz;
wire spi_slave_input = spi_mosi;

//
reg [7:0]slave_rx_buffer;
reg [7:0]master_rx_buffer;
assign rx_buffer = spi_mode_master ? master_rx_buffer : slave_rx_buffer;

wire timeout_ack;

//TX stages
//0 - idle
//1 - clock first phase
//2 - clock second phase
reg [1:0]tx_stage;

//[SR] Busy register (transfer ongoing)
reg slave_active;
reg [15:0]clk_count;
wire busy = spi_mode_master == 1'b1 ? tx_stage != 0 : 
					slave_active | (|clk_count );//(clk_count > 0);//XXX				

//Clock division for master transmitter
//reg [7:0]div;
reg [7:0]div_counter;
wire div_match = div == div_counter ? 1'b1 : 1'b0;
always@(posedge clk)
begin
	if(~rsti)
		div_counter <= #1 0;
	else if(div_counter == div || ~busy)//|| 0 == tx_stage)//
		div_counter <= #1 0;
	else
		div_counter <= #1 div_counter + 1'b1;
end

//master and slave has separate bit counters
reg [4:0]bit_counter;
//tx_buffer is shared between master and slave (see module declaration)
//wire [7:0]tx_buffer;
//slave and master has separate rx_buffers
//reg [7:0]master_rx_buffer;

//[CR] Most Significant Bit transfered first
wire msb_first;
//[SR] Master transmit completed
reg master_txc;
reg master_rxc;
/*
A     B   C   D
------    ----
      ----    ----
A - incactive phase (~cpol)
B - first phase
C - second phase
D - first phase of next clock

AB transition - entry edge
BC transition - first edge
CD transition - second edge
*/
wire cpol;//clock polarity, 0 - active high
wire cpha;//clock data phase, 0 - read data on first edge, 1 - on second
wire start;//external signal, indicating thad tx reigster conatins valid data


always@(posedge clk)
begin
	if(~rsti | ~spi_mode_master)
	begin
		master_txc <= #1 1'b0;
		master_rxc <= #1 1'b0;
		tx_stage <= #1 0;
		bit_counter <= #1 0;
		spi_clk_out <= #1 cpol;
		spi_master_output <= #1 0;
	end
	else
	begin
		master_txc <= #1 1'b0;
		master_rxc <= #1 1'b0;
		case(tx_stage)
			0:begin//idle stage
				if(start)//if tx buffer has data
				begin
					tx_stage <= #1 1;
					spi_clk_out <= #1 cpol;
					bit_counter <= #1 0;
				end
				else
				begin
					tx_stage <= #1 tx_stage;
					spi_clk_out <= #1 cpol;
				end
			end
			1:if(div_match)begin//phase 1
				spi_clk_out <= #1 ~cpol;
				
				if(msb_first == 1'b1)//latch output
					spi_master_output <= #1 tx_buffer[7-bit_counter];					
				else
					spi_master_output <= #1 tx_buffer[bit_counter];
					
				tx_stage <= #1 2;				
			end
			2:if(div_match)begin	//phase 2		
				//latch input
				if(msb_first == 1'b1)
					master_rx_buffer[7-bit_counter] <= #1 spi_master_input;
				else
					master_rx_buffer[bit_counter] <= #1 spi_master_input;
					
				spi_clk_out <= #1 cpol;				
				
				if(bit_counter == 5'd7)
				begin
					tx_stage <= #1 3;
					master_rxc <= #1 1'b1;
				end
				else
					tx_stage <= #1 1'b1;
					
				bit_counter <= #1 bit_counter + 1'b1;
			end			
			3:if(div_match)begin//tail of the last clock
				tx_stage <= #1 0;
				master_txc <= #1 1'b1;				  
			end				
		endcase
	end
end

// ----------- Slave section -----------
reg [2:0]slave_bit_coutner;
//reg [7:0]slave_rx_buffer;
//[SR] Slave receive completed
reg slave_rxc;
//reg slave_txe;
//reg [1:0]rx_stage;
reg rx_stage;
reg [7:0]timeout_counter;

reg [7:0]spi_slave_input_buffer;
reg [7:0]clk_detector;
always@(posedge clk)
begin
	clk_detector[7:0] <= #1 {clk_detector[6:0], spi_clk_in};
	spi_slave_input_buffer[7:0] <= #1 {spi_slave_input_buffer[6:0], spi_slave_input};
end

wire clk_detector_change = clk_detector[1] != clk_detector[0];
wire clk_detector_phase1 = (clk_detector[1] == cpol) && (clk_detector[0] == ~cpol);
wire clk_detector_phase2 = (clk_detector[1] == ~cpol) && (clk_detector[0] == cpol);

always@(posedge clk)
begin
	if(~rsti | spi_mode_master)
	begin
		slave_rxc <= #1 1'b0;
		//slave_txe <= #1 1'b1;
		rx_stage <= #1 0;//2'b00;
		spi_slave_output <= #1 1'b0;
		slave_bit_coutner <= #1 0;
		slave_active <= #1 0;		
	end
	else
	begin
		slave_rxc <= #1 1'b0;
		
		if(clk_detector_phase1 && rx_stage == 0)//rx first stage
		begin
			//latch slave tx data
			if(msb_first == 1'b1)
				spi_slave_output <= #1 tx_buffer[3'd7-slave_bit_coutner];					
			else
				spi_slave_output <= #1 tx_buffer[slave_bit_coutner];
			rx_stage <= #1 1;
			slave_active <= #1 1;
		end else 
		if(clk_detector_phase2 && rx_stage == 1)//rx second stage
		begin
			//latch input
			if(msb_first == 1'b1)
				slave_rx_buffer[7-slave_bit_coutner] <= #1 spi_slave_input_buffer[1];//spi_slave_input;
			else
				slave_rx_buffer[slave_bit_coutner] <= #1 spi_slave_input_buffer[1];//spi_slave_input;
			
			if(slave_bit_coutner == 3'd7)
			begin
				slave_rxc <= #1 1'b1;
				slave_bit_coutner <= #1 0;
				slave_active <= #1 1'b0;
			end
			else
			begin
				slave_bit_coutner <= #1 slave_bit_coutner + 1'b1;
			end
			
			rx_stage <= 0;
		end
	end
end

reg timeout_status;

always@(posedge clk)
begin	
	if(~rsti || clk_detector_change || timeout_status == 1'b1 || (timeout == 0) || spi_mode_master)
		timeout_counter <= #1 0;
	else
		if(div_match)
			timeout_counter <= timeout_counter + 1'b1;
end

always@(posedge clk)
begin
	if(~rst || timeout_ack)
	begin
		timeout_status <= 1'b0;
	end
	else
	if((timeout != 0) && (timeout_counter == timeout))
	begin
		timeout_status <= 1'b1;
	end
end

//estimate clock width
always@(posedge clk)
begin
  if(~rsti)
    clk_count <= #1 0;
  else
    begin      
		if(clk_detector_phase1 && rx_stage == 0)//entered first stage
        clk_count <= #1 0;
      else if(rx_stage == 1'b1 && ~&clk_count)			
			clk_count <= #1 clk_count + 1'b1;
      else if(rx_stage == 1'b0 && |clk_count)
        clk_count <= #1 clk_count - 1'b1;		  
    end
end

wire txc = master_txc | slave_rxc;
wire rxc = master_rxc | slave_rxc;

//Gather registers
//Status register (read only)
//master_txc valid only for master mode
//slave_rxc valid onlu for slave mode
assign sr[7:0] = {2'b00, clk_detector_change, timeout_status, txc, rxc, busy, enabled};

assign enabled = cr[0];
assign spi_mode_master = cr[1];
assign timeout_ack = cr[3];
assign msb_first = cr[4];
assign cpol = cr[5];
assign cpha = cr[6];
assign start = cr[7];

endmodule

/**
  * @file       CRC16_D8.c
  * @author     Kuznetsov M.S.
  * @version    V1.0
  * @date       20/02/2016
  * @brief      CRC16 IBM (Modbus) Calculation.
                Design polynom: 0x8005 (x^16 + x^15 + x^2 + x^0).
                Initial value 0xFFFF.
                Final Xor Value: 0x0000.
                Input reflected and result reflected.
                Check with http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
                           http://www.lammertbies.nl/comm/info/crc-calculation.html
                
                Core based on Verilog function by http://www.easics.com/webtools/crctool
  */ 

module CRC16_D8
    (
        nrst,
        clk,
        data,
        stb,
        crc
    );
    parameter INITIAL_VALUE = 16'hffff;
    input nrst;
    input clk;
    input [7:0] data;
    input stb;
    output [15:0] crc;
    
    reg [15:0] newcrc;
    reg [15:0] c;
    wire [7:0] d;
    
    genvar i;
    generate
    for (i=0; i <= 7; i=i+1)
        begin : reflect_input
            assign d[7-i] = data[i];
        end
    for (i=0; i <= 15; i=i+1)
        begin : reflect_output
            assign crc[15-i] = newcrc[i];
        end   
    endgenerate
    always@(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    c <= INITIAL_VALUE;
                    newcrc <= INITIAL_VALUE;
                end 
            else 
                if (stb)
                    begin
                        newcrc[0] <= d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
                        newcrc[1] <= d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
                        newcrc[2] <= d[1] ^ d[0] ^ c[8] ^ c[9];
                        newcrc[3] <= d[2] ^ d[1] ^ c[9] ^ c[10];
                        newcrc[4] <= d[3] ^ d[2] ^ c[10] ^ c[11];
                        newcrc[5] <= d[4] ^ d[3] ^ c[11] ^ c[12];
                        newcrc[6] <= d[5] ^ d[4] ^ c[12] ^ c[13];
                        newcrc[7] <= d[6] ^ d[5] ^ c[13] ^ c[14];
                        newcrc[8] <= d[7] ^ d[6] ^ c[0] ^ c[14] ^ c[15];
                        newcrc[9] <= d[7] ^ c[1] ^ c[15];
                        newcrc[10] <= c[2];
                        newcrc[11] <= c[3];
                        newcrc[12] <= c[4];
                        newcrc[13] <= c[5];
                        newcrc[14] <= c[6];
                        newcrc[15] <= d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
                    end
                else
                    c <= newcrc;
        end
endmodule

`timescale 1ps/1ps
module spi_top
    (
        input nrst,
        input clk,
        
        input spi_clk,
        output spi_miso,
        input spi_mosi,
        input spi_ncs,
        output reg irq1,
        output irq2,
        
        input [31:0] txb_din,
        input txb_wr,
        input [7:0] txb_adr,
        input txb_rst,//reset tx buffer write pointer        
        input [15:0] tx_size,
        output reg tx_done,
        input [7:0] tx_status,
        input tx_ready,
        
        output [7:0] rxb_dout,
        input rxb_re,  //Read Enable. Data can be retreived on the second rising edge since asserting RE.
        input rxb_rst,
        output rxb_ne,  //Receiver buffer Not Empty. Asserted with rx_done. 	
        
        output reg rx_cmd_class_store,
        output reg rx_cmd_class_init,
        
        output reg crc_error
    );

    // -- Local wires --
    wire [7:0]sr;
    wire [7:0]cr;
    reg spi_ncs_reg;
    wire enabled = ~spi_ncs_reg;
    wire spi_mode_master = 1'b0;//always slave
    wire timeout_ack = 1'b0;
    wire msb_first = 1'b1;
    wire cpol = 1'b1;
    wire cpha = 1'b1;
    wire start = 1'b0;//slave is host controlled

    wire txc;
    wire rxc;

    wire [7:0]rxd;
    wire [7:0]txd;
    wire [7:0]tx_ram_q;
    reg [15:0]rx_crc;
    wire [15:0]tx_crc;

    wire rxb_empty;




    localparam CMD_CLASS_INIT       = 8'h02,
               CMD_CLASS_GET_CONF   = 8'h03,
               CMD_CLASS_DEBUG      = 8'h05,
               CMD_CLASS_STORE_0    = 8'h10,
               CMD_CLASS_STORE_1    = 8'h11,
               CMD_CLASS_STORE_2    = 8'h12,
               CMD_CLASS_STORE_3    = 8'h13,
               CMD_CLASS_STORE_4    = 8'h14,
               CMD_CLASS_STORE_5    = 8'h15,
               CMD_CLASS_STORE_6    = 8'h16,
               CMD_CLASS_STORE_7    = 8'h20,
               CMD_CLASS_STORE_8    = 8'h21,
               CMD_CLASS_STORE_9    = 8'h22,
               CMD_CLASS_STORE_10    = 8'h23,
               CMD_CLASS_STORE_11    = 8'h24,
               CMD_CLASS_STORE_12    = 8'h25,
               CMD_CLASS_STORE_13    = 8'h26,
               CMD_CLASS_STORE_14    = 8'h27,
               CMD_CLASS_STORE_15    = 8'h28,
               CMD_CLASS_STORE_16    = 8'h29,
               CMD_CLASS_STORE_CFG_0 = 8'h30,
               CMD_CLASS_STORE_CFG_1 = 8'h31,
               CMD_CLASS_STORE_CFG_2 = 8'h32;

    localparam RX_STAGE_HEAD        = 4'd0,
               RX_STAGE_TYPE        = 4'd1,
               RX_STAGE_MARK_L      = 4'd2,
               RX_STAGE_MARK_H      = 4'd3,
               RX_STAGE_SIZE_L	    = 4'd4,
               RX_STAGE_SIZE_H      = 4'd5,
               RX_STAGE_DATA        = 4'd6,
               RX_STAGE_CRC_L       = 4'd7,
               RX_STAGE_CRC_H       = 4'd8;

    localparam TX_STAGE_HEAD		= 4'd0,
               TX_STAGE_TYPE		= 4'd1,
               TX_STAGE_STATUS      = 4'd2,   
               TX_STAGE_SIZE_L		= 4'd3,
               TX_STAGE_SIZE_H		= 4'd4,
               TX_STAGE_DATA        = 4'd5,   
               TX_STAGE_CRC_L		= 4'd6,
               TX_STAGE_CRC_H		= 4'd7,
               TX_STAGE_DONE		= 4'd8;

    reg [3:0] rx_stage;
    reg [7:0] rx_header_i; //TODO сделать проверку заголовка
    reg [7:0] rx_type_i;
    reg [15:0] rx_size;
    reg [15:0] rx_size_cnt;
    reg [15:0] rx_crc_calculated;
    // reg crc_error;
    reg rx_crc_complete;
    wire [15:0] rx_calc_crc;
    reg rxb_rdy;
    reg [7:0] rx_type;     

    reg txc_reg; 
    reg [3:0] tx_stage;
    reg [7:0] txd_buf;
    reg [7:0] tx_crc_buf;
    reg [9:0] rd_adr;
    
    reg fifo_purge;
   // wire [15:0] txd_status;
    //wire even;
    //reg wr_status;
    reg [1:0] spi_ncs_cnt;
    

    wire rx_cmd_class_store_i;
    wire rx_cmd_class_init_i;
    wire rx_cmd_class_get_conf_i;
    wire rx_cmd_get_usb_i;
    assign rx_cmd_class_store_i = (rx_type_i == CMD_CLASS_STORE_0) || 
                                   (rx_type_i == CMD_CLASS_STORE_1) || 
                                   (rx_type_i == CMD_CLASS_STORE_2) ||
                                   (rx_type_i == CMD_CLASS_STORE_3) ||
                                   (rx_type_i == CMD_CLASS_STORE_4) ||
                                   (rx_type_i == CMD_CLASS_STORE_5) ||
                                   (rx_type_i == CMD_CLASS_STORE_6) ||
                                   (rx_type_i == CMD_CLASS_STORE_7) ||
                                   (rx_type_i == CMD_CLASS_STORE_8) ||
                                   (rx_type_i == CMD_CLASS_STORE_9) ||
                                   (rx_type_i == CMD_CLASS_STORE_10) ||
                                   (rx_type_i == CMD_CLASS_STORE_11) ||
                                   (rx_type_i == CMD_CLASS_STORE_12) ||
                                   (rx_type_i == CMD_CLASS_STORE_13) ||
                                   (rx_type_i == CMD_CLASS_STORE_14) ||
                                   (rx_type_i == CMD_CLASS_STORE_15) ||
                                   (rx_type_i == CMD_CLASS_STORE_16) ||
                                   (rx_type_i == CMD_CLASS_STORE_CFG_0) ||
                                   (rx_type_i == CMD_CLASS_STORE_CFG_1) ||
                                   (rx_type_i == CMD_CLASS_STORE_CFG_2) ||
                                   (rx_type_i == CMD_CLASS_DEBUG);
    assign  rx_cmd_class_init_i = (rx_type_i == CMD_CLASS_INIT);
    assign rx_cmd_class_get_conf_i = (rx_type_i == CMD_CLASS_GET_CONF);
    assign rx_cmd_get_usb_i = (rx_type_i == 8'h04);
    assign irq2 = crc_error;
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                spi_ncs_reg <= 1'b1;
                spi_ncs_cnt <= 2'b00;
            end
        else
            begin
                if (spi_ncs)
                    spi_ncs_cnt <= spi_ncs_cnt + 1'b1;
                else 
                    begin
                        spi_ncs_cnt <= 2'b0;
                        spi_ncs_reg <= 1'b0;
                    end
                if (spi_ncs_cnt == 2'b11)                
                    spi_ncs_reg <= 1'b1;
            end        
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                rx_stage <= RX_STAGE_HEAD;
                crc_error <= 1'b0;
                rx_header_i <= 8'h00;
                rx_type_i <= 8'h00;
                rx_type <= 8'h00;
                rxb_rdy <= 1'b0;
                rx_size <= 16'h00;
                rx_size_cnt <= 16'h0000;
                rx_crc_complete <= 1'b0;
                rx_crc <= 16'h0000;
                rx_crc_calculated <= 16'h0000;
                fifo_purge <= 1'b0;
            end
        else 
            begin
                 if (rxb_rst)
                    begin
                        rx_stage <= RX_STAGE_HEAD;
                        rxb_rdy <= 1'b0;
                        rx_size_cnt <= 16'h0001;
                        rx_crc_complete <= 1'b0;
                    end
                else
                    begin
                        fifo_purge <= 1'b0;
                        if(rxb_empty) 
                            begin
                                rxb_rdy <= 1'b0;
                                rx_type <= 8'h00;
                                rx_cmd_class_store <= 1'b0;
                                rx_cmd_class_init <= 1'b0;
                            end                           
                        if (spi_ncs_reg)
                            begin
                                rx_cmd_class_store <= 1'b0;
                                rx_cmd_class_init <= 1'b0;                                
                                rxb_rdy <= 1'b0;
                                rx_stage <= RX_STAGE_HEAD; 
                                if (rx_stage != RX_STAGE_HEAD)
                                    begin
                                        fifo_purge <= 1'b1;
                                    end                         
                            end
                        else
                            begin
                                if (rxc) 
                                    case (rx_stage)
                                        RX_STAGE_HEAD:
                                            begin                           
                                                rx_size_cnt <= 16'h0001;                                            
                                                rx_stage <= RX_STAGE_TYPE;
                                                rx_header_i[7:0] <= rxd;
                                            end
                                        RX_STAGE_TYPE:
                                            begin
                                                rx_stage <= RX_STAGE_MARK_L;
                                                rx_type_i[7:0] <= rxd;
                                            end
                                        RX_STAGE_MARK_L:        
                                            begin
                                                rx_stage <= RX_STAGE_MARK_H;
                                                //for future use
                                            end    
                                        RX_STAGE_MARK_H:
                                            begin
                                                rx_stage <= RX_STAGE_SIZE_L;
                                                //for future use
                                            end                          
                                        RX_STAGE_SIZE_L:
                                            begin
                                                rx_stage <= RX_STAGE_SIZE_H;
                                                rx_size[7:0] <= rxd;
                                            end
                                        RX_STAGE_SIZE_H:
                                            begin
                                                rx_stage <= (rx_cmd_class_store_i | rx_cmd_class_get_conf_i | rx_cmd_get_usb_i) ? RX_STAGE_DATA : RX_STAGE_CRC_L;
                                                rx_size[15:8] <= rxd;
                                            end
                                        RX_STAGE_DATA:
                                            if (rx_size == rx_size_cnt)
                                                rx_stage <= RX_STAGE_CRC_L;
                                            else
                                                rx_size_cnt <= rx_size_cnt + 1'b1;                                    
                                        RX_STAGE_CRC_L:
                                            begin
                                                rx_crc[7:0] <= rxd;
                                                rx_crc_calculated <= rx_calc_crc;
                                                rx_stage <= RX_STAGE_CRC_H;
                                            end
                                        RX_STAGE_CRC_H:
                                            begin
                                                rx_crc[15:8] <= rxd;
                                                rx_crc_complete <= 1'b1;
                                                rx_stage <= RX_STAGE_HEAD;
                                            end    
                                    endcase
                            
                                if (rx_crc_complete)
                                    begin
                                        rx_crc_complete <= 1'b0;                                       
                                        rxb_rdy <= 1'b1;
                                        if (rx_crc == rx_crc_calculated)
                                            begin                                                
                                                crc_error <= 1'b0;
                                                rx_cmd_class_store <= rx_cmd_class_store_i;
                                                //if (tx_status[0] == 1'b1)
                                                    rx_cmd_class_init <= rx_cmd_class_init_i;
                                            end
                                        else 
                                            begin
                                                crc_error <= 1'b1;
                                                // fifo_purge <= 1'b1;
                                                rx_cmd_class_store <= rx_cmd_class_store_i;
                                            end
                                    end
                            end
                    end
            end

    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                tx_stage <= TX_STAGE_HEAD;
                txd_buf <= 8'h00;
                tx_crc_buf <= 8'h00;
                txc_reg <= 1'b0;
                rd_adr <= 10'h0_00;
                tx_done <= 1'b0;
            end
        else 
            begin
                txc_reg <= txc;
                if (txb_rst)
                    begin
                        tx_stage <= TX_STAGE_HEAD;
                        txd_buf <= 8'h00;
                        rd_adr <= 10'h0_00;
                    end
                else
                    begin
                        tx_done <= 1'b0;
                        irq1 <= tx_ready;
                        if (spi_ncs_reg)
                            begin
                                tx_stage <= TX_STAGE_HEAD;
                                txd_buf <= 8'h00;
                                rd_adr <= 10'h0_00;                                
                            end
                        else 
                           if (txc_reg)
                                case (tx_stage)
                                    TX_STAGE_HEAD:
                                        begin                                    
                                            txd_buf <= 8'h8D;
                                            tx_stage <= TX_STAGE_TYPE;
                                        end
                                    TX_STAGE_TYPE:
                                        begin
                                            txd_buf <= rx_type_i;
                                            tx_stage <= TX_STAGE_STATUS;
                                        end     
                                    TX_STAGE_STATUS:
                                        begin
                                            txd_buf <= tx_status;
                                            tx_stage <= TX_STAGE_SIZE_L;
                                        end             
                                    TX_STAGE_SIZE_L:
                                        begin
                                            txd_buf <= tx_size[7:0];
                                            tx_stage <= TX_STAGE_SIZE_H;
                                        end    
                                    TX_STAGE_SIZE_H:
                                        begin                                
                                            txd_buf <= tx_size[15:8];
                                            if (tx_size == 0)                                            
                                                begin                                                    
                                                    tx_stage <= TX_STAGE_CRC_L;
                                                end
                                            else 
                                                tx_stage <= TX_STAGE_DATA;
                                        end        
                                    TX_STAGE_DATA:
                                        begin
                                            txd_buf <= tx_ram_q;
                                            rd_adr <= rd_adr + 1'b1;                                            
                                            if (rd_adr == tx_size - 1'b1)
                                                begin
                                                    tx_stage <= TX_STAGE_CRC_L;
                                                end
                                            else
                                                tx_stage <= TX_STAGE_DATA;                         
                                        end                               
                                    TX_STAGE_CRC_L:
                                        begin
                                            txd_buf <= tx_crc[7:0];
                                            tx_crc_buf <= tx_crc[15:8];
                                            tx_stage <= TX_STAGE_CRC_H;
                                        end
                                    TX_STAGE_CRC_H:
                                        begin
                                            txd_buf <= tx_crc_buf;
                                            tx_stage <= TX_STAGE_DONE;
                                            
                                            tx_done <= (rx_cmd_class_get_conf_i | rx_cmd_get_usb_i) ? 1'b1 : 1'b0;
                                        end                            
                                    TX_STAGE_DONE:
                                        begin
                                            txd_buf <= 8'h00;
                                            tx_stage <= TX_STAGE_DONE;
                                        end
                                endcase
                    end
            end
    assign txc = sr[3];
    assign rxc = sr[2];

    assign cr[0] = enabled;
    assign cr[1] = spi_mode_master;
    assign cr[3] = timeout_ack;
    assign cr[4] = msb_first;
    assign cr[5] = cpol;
    assign cr[6] = cpha;
    assign cr[7] = start;

    assign txd = txd_buf;
    assign rxb_ne = rxb_rdy & ~rxb_empty;


    spi_master_slave spi0(
        .clk(clk),
        .rst(nrst),//active low
        
        .spi_clk (spi_clk),
        .spi_miso(spi_miso),
        .spi_mosi(spi_mosi),
        
        //output wire master,
        
        //control
        .cr(cr),
        .div(1),		
        .timeout(1),
        .sr(sr),
        //data
        .tx_buffer(txd),
        .rx_buffer(rxd)
    );

    spi_fifo rx_ram(
        .clock(clk),
        .sclr (~nrst | rxb_rst | fifo_purge),
        .data (rxd),
        .rdreq(rxb_re),	
        .wrreq(rxc),// | wr_status), //РўРµРїРµСЂСЊ РІ fifo РїРёС€СѓС‚СЃСЏ РІСЃРµ РґР°РЅРЅС‹Рµ, С‡С‚Рѕ РїСЂРёС…РѕРґСЏС‚ РїРѕ spi. РўР°Рє Р±СѓРґРµС‚ СѓРґРѕР±РЅРµРµ РїСЂРё Р·Р°РїРёcРё РґР°РЅРЅС‹С… РІ compact flash. 
                    //РќРµС‚ РЅРµРѕР±С…РѕРґРёРјРѕСЃС‚Рё РѕС‚РґРµР»СЊРЅРѕ РѕР±СЂР°Р±Р°С‚С‹РІР°С‚СЊ Р·Р°РіРѕР»РѕРІРѕРє
        .empty(rxb_empty),
        .full (),
        .q    (rxb_dout),
        .usedw()
        );
    
    spi_ram tx_ram(
        .clock(clk),
        .data(txb_din),
        .rdaddress(rd_adr),
        .wraddress(txb_adr),
        .wren(txb_wr),
        .q(tx_ram_q)
    ); 
        
    CRC16_D8 crc_rx0(
        .nrst(nrst & ~spi_ncs_reg & ~rxb_rst),
        .clk(clk),
        .data(rxd),
        .stb(rxc),
        .crc(rx_calc_crc)
    );

    CRC16_D8 crc_tx0(
        .nrst(nrst & ~spi_ncs_reg & ~txb_rst),
        .clk(clk),
        .data(txd),
        .stb(txc),
        .crc(tx_crc)
    );

endmodule

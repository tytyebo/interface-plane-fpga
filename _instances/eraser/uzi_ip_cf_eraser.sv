module uzi_ip_cf_eraser#(parameter CF_SECTORS_COUNT = 31293360)
                        (input bit clk, nrst,
                        // Wishbone master side
                        output logic [15:0] wbm_adr_o,
                        output logic [31:0] wbm_dat_o,
                        output logic [3:0] wbm_sel_o,
                        output logic wbm_cyc_o, wbm_stb_o, wbm_we_o,
                        input logic [31:0] wbm_dat_i,
                        input logic wbm_ack_i,
                        
                        // Wishbone slave side
                        input logic [15:0] wbs_adr_i,
                        input logic [31:0] wbs_dat_i,
                        input logic [3:0] wbs_sel_i,
                        input logic wbs_cyc_i, wbs_stb_i, wbs_we_i,
                        output logic [31:0] wbs_dat_o,
                        output logic wbs_ack_o);

    // Set settings by slave side
    logic [27:0] start_sector, sectors_count;
    logic mode, ena;
    logic [31:0] rdy_errors_count, drq_errors_count;
    
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs_dat_o <= '0;
            wbs_ack_o <= 1'b0;
            start_sector <= '0;
            sectors_count <= '0;
            mode <= 1'b0;
            ena <= 1'b0;
        end
        else begin
            wbs_ack_o <= 1'b0;
            if (wbs_stb_i && wbs_cyc_i && !wbs_ack_o) begin
                if (wbs_we_i) begin
                    case (wbs_adr_i[11:0])
                        12'd0: begin
                            if (wbs_sel_i == 4'hf) begin
                                start_sector <= wbs_dat_i[27:0];
                            end
                        end
                        12'd1: begin
                            if (wbs_sel_i == 4'hf) begin
                                sectors_count <= wbs_dat_i[27:0];
                            end
                        end
                        12'd2: begin
                            if (wbs_sel_i[0]) begin
                                {mode, ena} <= wbs_dat_i[1:0];
                            end
                        end
                    endcase
                end 
                else begin
                    case (wbs_adr_i[11:0])
                        12'd0: begin
                            if (wbs_sel_i == 4'hf) begin
                                wbs_dat_o <= {4'd0, start_sector};
                            end
                        end
                        12'd1: begin
                            if (wbs_sel_i == 4'hf) begin
                                wbs_dat_o <= {4'd0, sectors_count};
                            end
                        end
                        12'd2: begin
                            if (wbs_sel_i[0]) begin
                                wbs_dat_o <= {30'd0, mode, ena};
                            end
                        end
                        12'd3: begin
                            if (wbs_sel_i == 4'hf) begin
                                wbs_dat_o <= {12'd0, pages_cnt};
                            end
                        end
                        12'd4: begin
                            if (wbs_sel_i == 4'hf) begin
                                wbs_dat_o <= drq_errors_count;
                            end
                        end
                        12'd5: begin
                            if (wbs_sel_i == 4'hf) begin
                                wbs_dat_o <= rdy_errors_count;
                            end
                        end
                        default: begin
                            wbs_dat_o <= '0;
                        end
                    endcase
                end
                wbs_ack_o <= 1'b1;
            end 
            if (complete)
                ena <= 1'b0;
        end
    end    
    enum logic [3:0] {ST_IDLE, ST_FEATURES, ST_HEAD, ST_CYL_HIGH, ST_CYL_LOW, ST_SECTOR, ST_COUNT, ST_COMMAND,
          ST_GET_STATUS_DRQ, ST_CHECK_DRQ, ST_DATA, ST_GET_STATUS_RDY, ST_CHECK_RDY} state;
    localparam logic [15:0] ATA_FEATURES = 16'h11;
    localparam logic [15:0] ATA_HEAD = 16'h16;
    localparam logic [15:0] ATA_CYL_HIGH = 16'h15;
    localparam logic [15:0] ATA_CYL_LOW = 16'h14;
    localparam logic [15:0] ATA_SECTOR = 16'h13;
    localparam logic [15:0] ATA_COUNT = 16'h12;
    localparam logic [15:0] ATA_STATUS = 16'h17;
    localparam logic [15:0] ATA_COMMAND = 16'h17;
    localparam logic [15:0] ATA_DATA = 16'h10;
    
    logic start, last, complete, was_busy;
    logic [27:0] real_sectors_count, current_sector;
    logic [19:0] pages_cnt;
    logic [7:0] ata_sectors_count, ata_sectors_cnt, data_cnt;
    logic [15:0] null_data;
    logic [3:0] rdy_errors_cnt, drq_errors_cnt;
    assign null_data = (mode) ? 16'hffff : 16'h0000;
    assign real_sectors_count = (sectors_count + start_sector < CF_SECTORS_COUNT) ? sectors_count : '0; // Проверка выхода за границы размера CF 
    assign current_sector = start_sector + {pages_cnt, 8'd0};    
    // Set settings by slave side
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbm_adr_o <= '0;
            wbm_dat_o <= '0;
            wbm_sel_o <= '0;
            wbm_stb_o <= 1'b0;
            wbm_cyc_o <= 1'b0;
            wbm_we_o <= 1'b0;
            pages_cnt <= '0;
            start <= 1'b0;
            ata_sectors_count <= '0;
            ata_sectors_cnt <= '0;
            complete <= 1'b0;
            last <= 1'b0;
            was_busy <= 1'b0;
            data_cnt <= '0;
            state <= ST_IDLE;
            drq_errors_count <= '0;
            rdy_errors_count <= '0;
            rdy_errors_cnt <= '0;
            drq_errors_cnt <= '0;
        end
        else begin
            if (ena) begin
                start <= 1'b1;
            end  
            else begin
                start <= 1'b0;
                complete <= 1'b0;
            end
                
            case (state)
                ST_IDLE: begin
                    if (start & !complete) begin
                        if (pages_cnt < real_sectors_count[27:8]) begin
                            ata_sectors_count <= '0;
                        end
                        else begin
                            ata_sectors_count <= real_sectors_count[7:0];
                            last <= 1'b1;
                        end
                        state <= ST_FEATURES;
                    end
                end
                ST_FEATURES: begin
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_FEATURES;
                    wbm_dat_o <= '0;
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_HEAD;
                    end      
                end
                ST_HEAD: begin			 //1DD7FB0
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_HEAD;
                    wbm_dat_o <= {24'd0, 4'he, current_sector[27:24]};
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_CYL_HIGH;
                    end
                end
                ST_CYL_HIGH: begin
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_CYL_HIGH;
                    wbm_dat_o <= {24'd0, current_sector[23:16]};
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_CYL_LOW;
                    end
                end
                ST_CYL_LOW: begin
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_CYL_LOW;
                    wbm_dat_o <= {24'd0, current_sector[15:8]};
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_SECTOR;
                    end
                end
                ST_SECTOR: begin
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_SECTOR;
                    wbm_dat_o <= {24'd0, current_sector[7:0]};
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_COUNT;
                    end
                end
                ST_COUNT: begin
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_COUNT;
                    wbm_dat_o <= {24'd0, ata_sectors_count};
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_COMMAND;
                    end
                end
                ST_COMMAND: begin
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_COMMAND;
                    wbm_dat_o <= {24'd0, 8'h30};
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_GET_STATUS_DRQ;
                    end
                end
                ST_GET_STATUS_DRQ: begin
                    wbm_we_o <= 1'b0;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_STATUS;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_CHECK_DRQ;
                    end
                end
                ST_CHECK_DRQ: begin
                    if (!wbm_dat_i[0]) begin
                        if (!wbm_dat_i[7]) begin // nBSY
                            if (wbm_dat_i[3]) begin// DRQ
                                state <= ST_DATA;
                            end
                            else begin
                                state <= ST_GET_STATUS_DRQ;
                            end
                        end
                        else begin
                            state <= ST_GET_STATUS_DRQ;
                        end
                        drq_errors_count <= drq_errors_count + drq_errors_cnt;
                        drq_errors_cnt <= '0;
                    end
                    else begin
                        drq_errors_cnt <= drq_errors_cnt + 1'b1;
                        if (drq_errors_cnt == 4'd15) begin
                            drq_errors_count <= drq_errors_count + drq_errors_cnt;
                            drq_errors_cnt <= '0;
                            state <= ST_IDLE;
                        end
                    end
                    
                end
                ST_DATA: begin
                    wbm_we_o <= 1'b1;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_DATA;
                    wbm_dat_o <= {16'd0, null_data};
                    wbm_sel_o <= 4'h3;
                    if (wbm_ack_i) begin
                        data_cnt <= data_cnt + 1'b1;
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        if (data_cnt == 8'd255) begin
                            state <= ST_GET_STATUS_RDY;
                            ata_sectors_cnt <= ata_sectors_cnt + 1'b1;
                        end
                    end
                end                    
                ST_GET_STATUS_RDY: begin
                    wbm_we_o <= 1'b0;
                    wbm_cyc_o <= 1'b1;
                    wbm_stb_o  <= 1'b1;
                    wbm_adr_o <= ATA_STATUS;
                    if (wbm_ack_i) begin
                        wbm_cyc_o <= 1'b0;
                        wbm_stb_o  <= 1'b0;
                        state <= ST_CHECK_RDY;
                    end
                end
                ST_CHECK_RDY: begin
                    if (!wbm_dat_i[0]) begin
                        if (!wbm_dat_i[7]) begin // nBSY
                            if (was_busy) begin
                                if (wbm_dat_i[3]) begin // DRQ
                                    state <= ST_DATA;
                                end
                                else begin
                                    state <= ST_IDLE;
                                    pages_cnt <= pages_cnt + 1'b1;
                                    if (last) begin
                                        complete <= 1'b1;
                                        pages_cnt <= '0;
                                        last <= 1'b0;
                                    end
                                end
                                was_busy <= 1'b0;
                            end
                            else begin
                                state <= ST_GET_STATUS_RDY;
                            end
                        end
                        else begin
                            state <= ST_GET_STATUS_RDY;
                            was_busy <= 1'b1;
                        end
                        rdy_errors_count <= rdy_errors_count + rdy_errors_cnt;
                        rdy_errors_cnt <= '0;
                    end
                    else begin
                        rdy_errors_cnt <= rdy_errors_cnt + 1'b1;
                        if (rdy_errors_cnt == 4'd15) begin
                            rdy_errors_count <= rdy_errors_count + rdy_errors_cnt;
                            rdy_errors_cnt <= '0;
                            state <= ST_IDLE;
                        end
                    end
                end
            endcase
        end
    end
    
endmodule
`include "defines.svh"
`include "interfaces.svh"
module ird_ip_service_ctrl(input bit clk, nrst_pon,
                           input logic rec_cmd, rec_cmd_ena, rec_state,
                           wishbone_io.slave wbs,        
                           output logic nrst, rst);
                    
    localparam bit [31:0] reg0 = 32'hAAAA_5555, reg1 = 32'h5555_AAAA;
    logic [31:0] reg2;
    logic soft_rst_flag;
    always_ff @(posedge clk or negedge nrst) begin
        if (!nrst) begin
            wbs.wbs_ack <= 1'b0;
            reg2 <= '0;
            wbs.wbs_dat <= '0;
            soft_rst_flag <= 1'b0;
        end
        else begin
            wbs.wbs_ack <= 1'b0;
            soft_rst_flag <= 1'b0;
            if (wbs.wbm_stb && wbs.wbm_cyc && !wbs.wbs_ack) begin
                if (wbs.wbm_we) begin
                    unique case (wbs.wbm_adr[11:0])
                        12'h002:
                            reg2 <= wbs.wbm_dat;
                        12'h003:
                            soft_rst_flag <= 1'b1;
                    endcase
                end
                else begin
                    unique case (wbs.wbm_adr[11:0])
                        12'd0: begin
                            wbs.wbs_dat <= reg0;
                        end
                        12'd1: begin
                            wbs.wbs_dat <= reg1;
                        end
                        12'd2: begin
                            wbs.wbs_dat <= reg2;
                        end
                        12'd4: begin
                            wbs.wbs_dat <= version;
                        end
                        12'd5: begin
                            wbs.wbs_dat <= {29'd0, rec_cmd_ena, rec_cmd, rec_state};
                        end
                        default:
                            wbs.wbs_dat <= 32'd0;
                    endcase
                end
                wbs.wbs_ack <= 1'b1;
            end
        end
    end
            
    logic soft_rst;   
    logic [21:0] soft_rst_cnt;
    logic [1:0] state_soft_reset;
    always_ff @(posedge clk or negedge nrst_pon) begin
        if (!nrst_pon) begin
            soft_rst <= 1'b0;
            nrst <= 1'b0;
            state_soft_reset <= 2'd0;
            soft_rst_cnt <= 22'd0;
            rst <= 1'b0;                    
        end                
        else begin
            nrst <= ~soft_rst & nrst_pon;
            rst <= soft_rst | ~nrst_pon;
            soft_rst <= 1'b0;
            soft_rst_cnt <= soft_rst_cnt + 1'b1;                    
            unique case (state_soft_reset)
                2'd0: begin
                    soft_rst_cnt <= 22'd0;
                    if (soft_rst_flag)
                        state_soft_reset <= 2'd1;
                end
                2'd1: begin
                    if (soft_rst_cnt == 22'h2ffffd)
                        state_soft_reset <= 2'd2;
                end
                2'd2: begin
                    soft_rst <= 1'b1;
                    if (soft_rst_cnt == 22'h2ffffd)
                        state_soft_reset <= 2'd0;
                end
                default: begin
                    state_soft_reset <= 2'd0;
                end
            endcase    
        end
    end
        
    logic [31:0] version;
    version_reg versionReg(.vout(version));
endmodule: ird_ip_service_ctrl
`timescale 1ps/1ps
module USB_Buffers
    (
        nrst,
        clk,

        wb_adr_i,
        wb_dat_i,	
        wb_stb_i,
        wb_cyc_i,
        wb_sel_i,
        wb_we_i ,	
        wb_dat_o,
        wb_ack_o,
        
        mem0_full,
        mem1_full       
    );
    
    input nrst;
    input clk;

    input [11:0] wb_adr_i;
    input [31:0] wb_dat_i;
    input wb_stb_i;
    input wb_cyc_i;
    input [3:0] wb_sel_i;
    input wb_we_i;
    output reg [31:0]wb_dat_o;
    output reg wb_ack_o;

    output reg mem0_full;
    output reg mem1_full;  
    reg [31:0] mem0_addr_cnt;
    reg [31:0] mem1_addr_cnt;

    parameter DATA_WIDTH = 32;
    parameter ADDR_WIDTH = 12;
    parameter SEL_WIDTH  = 4;
    parameter RAM_DEPTH = 4096;
    
    reg [DATA_WIDTH-1:0] mem0[0:RAM_DEPTH-1];
    //reg [DATA_WIDTH-1:0] mem1[0:RAM_DEPTH-1];
    
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                mem0_full <= 1'b0;
                mem1_full <= 1'b0;
                mem0_addr_cnt <= 32'h0000_0000;
                mem1_addr_cnt <= 32'h0000_0000;
                wb_ack_o <= 1'b0;
            end
        else
            begin     
                wb_ack_o <= 1'b0;
                if (mem0_addr_cnt == RAM_DEPTH - 1)
                    mem0_full <= 1'b1;    
                    
                if (mem0_addr_cnt == 0)
                    mem0_full <= 1'b0;    
                    
                if (mem1_addr_cnt == RAM_DEPTH - 1)
                    mem1_full <= 1'b1;
                    
                if (mem1_addr_cnt == 0)
                    mem1_full <= 1'b0;
                    
                if (wb_stb_i && wb_cyc_i && ~wb_ack_o)
                    begin
                        wb_ack_o <= 1'b1;
                    end
                    
                if (wb_stb_i && wb_cyc_i)
                    if (wb_we_i)
                        case ({mem1_full, mem0_full})
                            2'b00:
                                begin
                                    mem0_addr_cnt <= mem0_addr_cnt + 1'b1;
                                end
                            2'b01:
                                begin
                                    mem1_addr_cnt <= mem0_addr_cnt + 1'b1;
                                end
                            2'b10:
                                begin
                                    mem0_addr_cnt <= mem0_addr_cnt + 1'b1;
                                end
                            2'b11:
                                begin
                                end                                    
                        endcase
                    else
                        case ({mem1_full, mem0_full})
                            2'b00:
                                begin
                                end
                            2'b01:
                                if (mem0_addr_cnt != 32'h0000_0000)
                                    mem0_addr_cnt <= mem0_addr_cnt - 1'b1;
                            2'b10:
                                if (mem1_addr_cnt != 32'h0000_0000)
                                    mem1_addr_cnt <= mem1_addr_cnt - 1'b1;
                            2'b11:
                                if (mem0_addr_cnt != 32'h0000_0000)
                                    mem0_addr_cnt <= mem0_addr_cnt - 1'b1;                                  
                        endcase
            end
            
    /*always @(posedge clk)        
        if (wb_stb_i && wb_cyc_i && wb_we_i)
            case ({mem1_full, mem0_full})
                    2'b00:
                        mem0[wb_adr_i]	<= wb_dat_i[DATA_WIDTH-1:0];
                    2'b01:                        begin
                        end 
                        //mem1[wb_adr_i]	<= wb_dat_i[DATA_WIDTH-1:0];
                    2'b10:
                        mem0[wb_adr_i]	<= wb_dat_i[DATA_WIDTH-1:0];
                    2'b11:
                        begin
                        end                                    
            endcase*/
            
    always @(posedge clk)
        if (wb_stb_i && wb_cyc_i)
            case ({mem1_full, mem0_full})
                2'b00:
                    begin
                    end
                2'b01:
                    wb_dat_o <= mem0[wb_adr_i];
                2'b10:                        begin
                        end 
                   // wb_dat_o <= mem1[wb_adr_i];
                2'b11:
                    wb_dat_o <= mem0[wb_adr_i];
            endcase                         
endmodule
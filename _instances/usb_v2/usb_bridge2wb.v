/** 
  * Версия 3. 20.02.2020
  * Приёмный FSM доработал для полуавтоматической записи в CF.
  * Версия 2. 18.09.2017
  * Исправил ошибки, добавил порты шины WB.
  * Версия 1. 16.08.2017
  * Нет истории измнений.
  */
module usb_bridge2wb
    (
        input nrst,
        input clk,
        
        input [7:0] burst_len, 
        input [1:0] burst_inc,
        input [3:0] sel, // Если sel = 4'h0 (что является ошибкой), то контроллер будет считать, что sel = 4'h1
        input we,
        
        input rxpak_rdy,	
        input [7:0] rxpak_din,	
        output rxpak_req,
        output rxpak_ack,

        input txpak_ack,
        input txpak_rdy,
        output txpak_stb,        
        output [7:0] txpak_dout,					
        output txpak_done,

        output [15:0] wb_adr_o,
        output [31:0] wb_dat_o,
        output [3:0] wb_sel_o,
        output wb_cyc_o,
        output wb_stb_o,
        output wb_we_o,
        input [31:0] wb_dat_i,
        input wb_ack_i
    );
    
    assign wb_adr_o = wb_addr;
    assign wb_dat_o = wb_dat;
    assign wb_cyc_o = wb_cyc;
    assign wb_stb_o = wb_stb;
    assign wb_we_o = wb_we;
    assign wb_sel_o = wb_sel;
    
    localparam ST_IDLE = 4'd0,
               ST_IN_ADDR_LOW = 4'd1,
               ST_IN_ADDR_HIGH = 4'd2,
               ST_IN_DATA_LL = 4'd3,
               ST_IN_DATA_LH = 4'd4,
               ST_IN_DATA_HL = 4'd5,
               ST_IN_DATA_HH = 4'd6,
               ST_GET_STATUS_REG = 4'd7,
               ST_CHECK_STATUS_REG = 4'd8,
               ST_WAIT_WR = 4'd9,
               ST_WAIT_RD = 4'd10,
               ST_WAIT_NEXT_RD = 4'd11,
               ST_TAIL = 4'd12;
    reg rxp_req;
    reg [15:0] addr;
    reg [15:0] addr_saved;
    reg [31:0] dword;
    reg [31:0] dword_saved;
    reg [3:0] rxp_sel;
    reg [3:0]  rxp_sel_saved;
    reg dword_full;
    reg rxp_we;
    reg rxp_we_saved;
    reg [3:0] state;
    reg [7:0] burst_cnt;
    reg [1:0] addr_inc;
    reg rxp_ack;
    reg read_start;
    reg [2:0] rxp_tail;
    reg reg_access, check_status;
    reg was_busy, mult_write;
    assign rxpak_req = rxp_req;
    assign rxpak_ack = rxp_ack;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    state <= ST_IDLE;
                    rxp_req <= 1'b0;
                    dword_full <= 1'b0;
                    addr <= 16'd0;
                    addr_saved <= 16'd0;
                    dword <= 32'd0;
                    dword_saved <= 32'd0;
                    burst_cnt <= 8'd0;   
                    rxp_sel <= 4'd0;
                    rxp_sel_saved <= 4'd0;
                    rxp_we <= 1'b0;
                    rxp_we_saved <= 1'b0;
                    addr_inc <= 2'd0;
                    rxp_ack <= 1'b0;
                    read_start <= 1'b0;
                    rxp_tail <= 3'd0;
                    reg_access <= 1'b0;
                    check_status <= 1'b0;
                    was_busy <= 1'b0;
                    mult_write <= 1'b0;
                end
            else
                begin
                    rxp_req <= 1'b0;
                    rxp_ack <= 1'b0;
                    case (state) // Вычитывает fifo с принятым пакетом и формирует адрес/данные для шины Wishbone
                        ST_IDLE:
                            begin
                                if (rxpak_rdy && ~rxp_ack && ~txp_busy)
                                    begin
                                        rxp_req <= 1'b1;
                                        burst_cnt <= burst_len;
                                        rxp_sel <= sel;
                                        rxp_we <= we;
                                        addr_inc <= burst_inc;
                                        state <= ST_IN_ADDR_LOW;
                                        mult_write <= 1'b1;
                                    end
                            end
                        ST_IN_ADDR_LOW:
                            begin
                                addr[7:0] <= rxpak_din;
                                rxp_req <= 1'b1;
                                state <= ST_IN_ADDR_HIGH;
                            end
                        ST_IN_ADDR_HIGH:
                            begin
                                addr[15:8] <= rxpak_din;
                                if (rxp_we)
                                    begin
                                        state <= ST_IN_DATA_LL;
                                    
                                        if (rxp_sel[3] == 1'b1)
                                            begin
                                                rxp_req <= 1'b1;
                                                state <= ST_IN_DATA_HH;
                                            end
                                            
                                        if (rxp_sel[2] == 1'b1)
                                            begin
                                                rxp_req <= 1'b1;
                                                state <= ST_IN_DATA_HL;
                                            end
                                            
                                        if (rxp_sel[1] == 1'b1)
                                            begin
                                                rxp_req <= 1'b1;
                                                state <= ST_IN_DATA_LH;
                                            end
                                            
                                        if (rxp_sel[0] == 1'b1)
                                            begin
                                                rxp_req <= 1'b1;
                                                state <= ST_IN_DATA_LL;
                                            end
                                    end
                                else
                                    begin
                                        dword_full <= 1'b1;
                                        read_start <= 1'b1;
                                        state <= ST_WAIT_RD;
                                    end
                                
                            end
                        ST_IN_DATA_LL:
                            begin
                                dword[7:0] <= rxpak_din;
                                if (!mult_write || addr != 16'h0010)
                                    begin
                                        state <= ST_WAIT_WR;
                                        dword_full <= 1'b1;
                                    end
                                else
                                    begin
                                        state <= ST_GET_STATUS_REG;
                                        check_status <= 1'b1;
                                    end
              
                                if (rxp_sel[3] == 1'b1)
                                    begin
                                        rxp_req <= 1'b1;
                                        state <= ST_IN_DATA_HH;
                                        check_status <= 1'b0;
                                        dword_full <= 1'b0;
                                    end
                                    
                                if (rxp_sel[2] == 1'b1)
                                    begin
                                        rxp_req <= 1'b1;
                                        state <= ST_IN_DATA_HL;
                                        check_status <= 1'b0;
                                        dword_full <= 1'b0;
                                    end
                                    
                                if (rxp_sel[1] == 1'b1)
                                    begin
                                        rxp_req <= 1'b1;
                                        state <= ST_IN_DATA_LH;
                                        check_status <= 1'b0;
                                        dword_full <= 1'b0;
                                    end                               
                            end
                        ST_IN_DATA_LH:
                            begin
                                dword[15:8] <= rxpak_din;

                                if (!mult_write || addr != 16'h0010)
                                    begin
                                        state <= ST_WAIT_WR;
                                        dword_full <= 1'b1;                                
                                    end
                                else
                                    begin
                                        state <= ST_GET_STATUS_REG;
                                        check_status <= 1'b1;
                                    end
                                
                                if (rxp_sel[3] == 1'b1)
                                    begin
                                        rxp_req <= 1'b1;
                                        state <= ST_IN_DATA_HH;    
                                        check_status <= 1'b0;
                                        dword_full <= 1'b0;                                        
                                    end
                                    
                                if (rxp_sel[2] == 1'b1)
                                    begin
                                        rxp_req <= 1'b1;
                                        state <= ST_IN_DATA_HL;
                                        check_status <= 1'b0;
                                        dword_full <= 1'b0;
                                    end
                            end
                        ST_IN_DATA_HL:
                            begin
                                dword[23:16] <= rxpak_din;
                                if (!mult_write || addr != 16'h0010)
                                    begin
                                        state <= ST_WAIT_WR;
                                        dword_full <= 1'b1;                                
                                    end
                                else
                                    begin
                                        state <= ST_GET_STATUS_REG;
                                        check_status <= 1'b1;
                                    end
                                
                                if (rxp_sel[3] == 1'b1)
                                    begin
                                        rxp_req <= 1'b1;
                                        state <= ST_IN_DATA_HH;
                                        check_status <= 1'b0;
                                        dword_full <= 1'b0;
                                    end
                            end
                        ST_IN_DATA_HH:
                            begin
                                dword[31:24] <= rxpak_din;
                                if (!mult_write || addr != 16'h0010)
                                    begin
                                        state <= ST_WAIT_WR;
                                        dword_full <= 1'b1;                                
                                    end
                                else
                                    begin
                                        state <= ST_GET_STATUS_REG;
                                        check_status <= 1'b1;
                                    end
                            end
                        ST_GET_STATUS_REG:
                            begin
                                if (bus_ack || check_status)
                                    begin
                                        if (addr != 16'h0010)
                                            begin
                                                dword_full <= 1'b0;
                                                state <= ST_TAIL;
                                            end
                                        else
                                            begin
                                                addr <= 16'h17;
                                                rxp_we <= 1'b0;
                                                rxp_sel <= 4'h3;
                                                dword_full <= 1'b1;
                                                reg_access <= 1'b1;
                                                addr_saved <= addr;
                                                rxp_we_saved <= rxp_we;
                                                dword_saved <= dword;
                                                rxp_sel_saved <= rxp_sel;
                                                state <= ST_CHECK_STATUS_REG;
                                                check_status <= 1'b0;
                                            end
                                    end
                            end
                        ST_CHECK_STATUS_REG:
                            begin
                                if (bus_ack)
                                    begin
                                        dword_full <= 1'b0;
                                        addr <= addr_saved;
                                        rxp_we <= rxp_we_saved;
                                        dword <= dword_saved;
                                        rxp_sel <= rxp_sel_saved;
                                    end
                                    
                                if (reg_ready)
                                    begin
                                        reg_access <= 1'b0;
                                        addr <= addr_saved;
                                        rxp_we <= rxp_we_saved;
                                        dword <= dword_saved;
                                        rxp_sel <= rxp_sel_saved;
                                        if (!wb_dat[7]) // nBSY
                                            begin
                                                if (mult_write)
                                                    begin
                                                        if (wb_dat[3]) // DRQ
                                                            begin
                                                                state <= ST_WAIT_WR;
                                                                dword_full <= 1'b1;   
                                                                mult_write <= 1'b0;
                                                            end
                                                        else
                                                            begin
                                                                state <= ST_GET_STATUS_REG;
                                                                check_status <= 1'b1;
                                                            end
                                                    end
                                                else
                                                    begin
                                                        if (was_busy)
                                                            begin
                                                                state <= ST_TAIL;
                                                                was_busy <= 1'b0;
                                                            end
                                                        else
                                                            begin // BSY
                                                                state <= ST_GET_STATUS_REG;
                                                                check_status <= 1'b1;
                                                            end
                                                            
                                                    end
                                            end
                                        else
                                            begin // BSY
                                                state <= ST_GET_STATUS_REG;
                                                check_status <= 1'b1;
                                                was_busy <= 1'b1;
                                            end
                                end  
                            end      
                        ST_WAIT_WR:
                            begin
                                if (bus_ack)
                                    begin
                                        addr <= addr + addr_inc;
                                        dword_full <= 1'b0;
                                        if (burst_cnt == 8'd0)
                                            begin
                                                // state <= ST_TAIL;
                                                check_status <= 1'b1;
                                                state <= ST_GET_STATUS_REG;
                                                rxp_tail <= 3'd0;
                                            end
                                        else
                                            begin
                                                burst_cnt <= burst_cnt - 1'b1;
                                                if (rxp_sel[3] == 1'b1)
                                                    begin
                                                        rxp_req <= 1'b1;
                                                        state <= ST_IN_DATA_HH;
                                                    end
                                                    
                                                if (rxp_sel[2] == 1'b1)
                                                    begin
                                                        rxp_req <= 1'b1;
                                                        state <= ST_IN_DATA_HL;
                                                    end
                                                    
                                                if (rxp_sel[1] == 1'b1)
                                                    begin
                                                        rxp_req <= 1'b1;
                                                        state <= ST_IN_DATA_LH;
                                                    end
                                                    
                                                if (rxp_sel[0] == 1'b1)
                                                    begin
                                                        rxp_req <= 1'b1;
                                                        state <= ST_IN_DATA_LL;
                                                    end   
                                            end
                                    end
                            end
                        ST_WAIT_RD:
                            begin
                                read_start <= 1'b1;
                                addr <= addr + addr_inc;
                                if (bus_ack)
                                    begin
                                        dword_full <= 1'b0;
                                        read_start <= 1'b0;
                                        if (burst_cnt != 8'd0)
                                            begin
                                                read_start <= 1'b1;
                                                if (txpak_rdy)
                                                    begin
                                                        burst_cnt <= burst_cnt - 1'b1;
                                                        dword_full <= 1'b1;
                                                    end
                                                else
                                                    state <= ST_WAIT_NEXT_RD;
                                            end
                                        else
                                            begin
                                                state <= ST_TAIL;
                                                rxp_tail <= 3'd4;
                                            end
                                    end
                            end
                        ST_WAIT_NEXT_RD:
                            begin
                                if (txpak_rdy)
                                    begin
                                        burst_cnt <= burst_cnt - 1'b1;
                                        read_start <= 1'b1;
                                        dword_full <= 1'b1;
                                        state <= ST_WAIT_RD;
                                    end
                            end
                        ST_TAIL: // в фифо остается контрольная сумма пакета, а при чтении еще и 4 байта данных, которые надо дочитать из фифо
                                 // TODO: Зачем было записывать их в фифо? Может просто не записывать? 16.01.2019
                            begin
                                rxp_tail <= rxp_tail - 1'd1;
                                rxp_req <= 1'b1;
                                if (rxp_tail == 3'd0)
                                    begin
                                        rxp_ack <= 1'b1;
                                        state <= ST_IDLE;
                                    end
                            end
                        default:
                            state <= ST_IDLE;                            
                    endcase
                end
        end
                    
    localparam WB_IDLE = 1'd0,
               WB_ACK = 1'd1;
    reg bus_ack;
    reg wb_state;
    reg wb_stb;
    reg wb_cyc;
    reg [3:0] wb_sel;
    reg wb_we;
    reg [31:0] wb_dat;
    reg [15:0] wb_addr;
    reg data_ready, reg_ready;
    wire ack_i = wb_ack_i;
    wire [31:0] dat_i = wb_dat_i;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    bus_ack <= 1'b0;
                    wb_state <= WB_IDLE;
                    wb_stb <= 1'b0;
                    wb_cyc <= 1'b0;
                    wb_sel <= 4'd0;
                    wb_we <= 1'b0;
                    wb_dat <= 32'd0;
                    wb_addr <= 16'd0;
                    data_ready <= 1'b0;
                    reg_ready <= 1'b0;
                end
            else
                begin
                    bus_ack <= 1'b0;
                    reg_ready <= 1'b0;
                    if (txd_ack)
                        data_ready <= 1'b0;
                        
                    case (wb_state) // Выполняет операции с шиной Wishbone (master)
                        WB_IDLE: 
                            begin
                                if (dword_full & ~data_ready)
                                    begin
                                        wb_stb <= 1'b1;
                                        wb_cyc <= 1'b1;
                                        wb_we <= rxp_we;
                                        wb_sel <= rxp_sel;
                                        wb_addr <= addr;
                                        wb_dat <= (rxp_we) ? dword : 32'd0;
                                        wb_state <= WB_ACK;
                                        bus_ack <= 1'b1;                                       
                                    end
                            end
                        WB_ACK: 
                            begin
                                if (ack_i)
                                    begin                                       
                                        wb_stb <= 1'b0;
                                        wb_cyc <= 1'b0;
                                        wb_dat <= (wb_we) ? 32'd0 : dat_i;
                                        wb_state <= WB_IDLE;
                                        data_ready <= (wb_we || reg_access) ? 1'b0 : 1'b1;
                                        reg_ready <= (wb_we) ? 1'b0 : reg_access;
                                    end                          
                            end
                    endcase
                end
        end
        
    localparam ST_OUT_ADDR_LOW = 3'd0,
               ST_OUT_ADDR_HIGH = 3'd1,
               ST_OUT_WAIT_DATA = 3'd2,
               ST_OUT_DATA_LL = 3'd3,
               ST_OUT_DATA_LH = 3'd4,
               ST_OUT_DATA_HL = 3'd5,
               ST_OUT_DATA_HH = 3'd6,
               ST_OUT_DONE = 3'd7;
    reg [7:0] txp_dout;
    reg [31:0] rd_data;
    reg txp_stb;
    reg [2:0] rd_state;
    reg txd_ack;
    reg txp_done;
    reg [7:0] burst_rd_cnt;
    reg txp_busy;
    assign txpak_stb = txp_stb;
    assign txpak_done = txp_done;
    assign txpak_dout = txp_dout;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    txp_dout <= 8'd0;
                    rd_data <= 32'd0;
                    txp_stb <= 1'b0;
                    rd_state <= ST_OUT_ADDR_LOW;
                    txd_ack <= 1'b0;
                    txp_done <= 1'b0;
                    burst_rd_cnt <= 8'd0;
                    txp_busy <= 1'b0;
                end
            else
                begin
                    txd_ack <= 1'b0;
                    txp_done <= 1'b0;;
                    txp_stb <= 1'b0;
                    if (txpak_rdy)
                        begin
                            case (rd_state) // Записывает прочитанные данные с шины Wishbone в fifo передаваемого пакета
                                ST_OUT_ADDR_LOW: 
                                    begin
                                        if (read_start)
                                            begin
                                                burst_rd_cnt <= burst_cnt;
                                                txp_dout <= addr[7:0];
                                                txp_stb <= 1'b1;   
                                                txp_busy <= 1'b1;
                                                rd_state <= ST_OUT_ADDR_HIGH;
                                            end
                                    end
                                ST_OUT_ADDR_HIGH:
                                    begin
                                        txp_dout <= addr[15:8];
                                        txp_stb <= 1'b1;
                                        rd_state <= ST_OUT_WAIT_DATA;
                                    end
                                ST_OUT_WAIT_DATA:
                                    begin
                                        if (data_ready)
                                            begin
                                                txd_ack <= 1'b1;
                                                rd_data <= wb_dat;
                                                if (rxp_sel[3] == 1'b1)
                                                    rd_state <= ST_OUT_DATA_HH;
                                                    
                                                if (rxp_sel[2] == 1'b1)
                                                    rd_state <= ST_OUT_DATA_HL;
                                                    
                                                if (rxp_sel[1] == 1'b1)
                                                    rd_state <= ST_OUT_DATA_LH;
                                                    
                                                if (rxp_sel[0] == 1'b1)
                                                    rd_state <= ST_OUT_DATA_LL; 
                                            end
                                    end
                                ST_OUT_DATA_LL:
                                    begin
                                        txp_dout <= rd_data[7:0];
                                        txp_stb <= 1'b1;
                                        
                                        if (burst_rd_cnt == 8'd0)
                                            rd_state <= ST_OUT_DONE;
                                        else
                                            begin
                                                rd_state <= ST_OUT_WAIT_DATA;
                                                burst_rd_cnt <= burst_rd_cnt - 1'b1;
                                            end
                                        
                                        if (rxp_sel[3] == 1'b1)
                                            begin
                                                rd_state <= ST_OUT_DATA_HH;
                                                burst_rd_cnt <= burst_rd_cnt;
                                            end
                                    
                                        if (rxp_sel[2] == 1'b1)
                                            begin
                                                rd_state <= ST_OUT_DATA_HL;
                                                burst_rd_cnt <= burst_rd_cnt;
                                            end    
                                        if (rxp_sel[1] == 1'b1)
                                            begin
                                                rd_state <= ST_OUT_DATA_LH;
                                                burst_rd_cnt <= burst_rd_cnt;
                                            end
                                    end
                                ST_OUT_DATA_LH:
                                    begin
                                        txp_dout <= rd_data[15:8];
                                        txp_stb <= 1'b1;
                                        if (burst_rd_cnt == 8'd0)
                                            rd_state <= ST_OUT_DONE;
                                        else
                                            begin
                                                rd_state <= ST_OUT_WAIT_DATA;
                                                burst_rd_cnt <= burst_rd_cnt - 1'b1;
                                            end
                                                
                                        if (rxp_sel[3] == 1'b1)
                                            begin
                                                rd_state <= ST_OUT_DATA_HH;
                                                burst_rd_cnt <= burst_rd_cnt;
                                            end

                                        if (rxp_sel[2] == 1'b1)
                                            begin
                                                rd_state <= ST_OUT_DATA_HL;
                                                burst_rd_cnt <= burst_rd_cnt;
                                            end
                                    end
                                ST_OUT_DATA_HL:
                                    begin
                                        txp_dout <= rd_data[23:16];
                                        txp_stb <= 1'b1;
                                        if (burst_rd_cnt == 8'd0)
                                            rd_state <= ST_OUT_DONE;
                                        else
                                            begin
                                                rd_state <= ST_OUT_WAIT_DATA;
                                                burst_rd_cnt <= burst_rd_cnt - 1'b1;
                                            end
                                        
                                        if (rxp_sel[3] == 1'b1)
                                            begin
                                                rd_state <= ST_OUT_DATA_HH;
                                                burst_rd_cnt <= burst_rd_cnt;
                                            end
                                    end
                                ST_OUT_DATA_HH:
                                    begin
                                        txp_dout <= rd_data[31:24];
                                        txp_stb <= 1'b1;
                                        if (burst_rd_cnt == 8'd0)
                                            rd_state <= ST_OUT_DONE;
                                        else
                                            begin
                                                rd_state <= ST_OUT_WAIT_DATA;
                                                burst_rd_cnt <= burst_rd_cnt - 1'b1;
                                            end
                                    end
                                ST_OUT_DONE:
                                    begin
                                        if (txpak_ack)
                                            begin
                                                rd_state <= ST_OUT_ADDR_LOW;
                                                txp_busy <= 1'b0;
                                            end
                                        else
                                            txp_done <= 1'b1;
                                    end
                            endcase
                        end
                end
        end
endmodule
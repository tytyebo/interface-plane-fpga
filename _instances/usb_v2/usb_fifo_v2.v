module usb_fifo_v2
    (
        input nrst,
        input clk,
 
        input ftdi_clkout,	//FTDI workds on rising edge of it's clock. Both for data output and input 
        inout [7:0] ftdi_adbus,
        input ftdi_rxf_n,
        input ftdi_txe_n,
        output reg  ftdi_rd_n,
        output ftdi_wr_n,

        output reg ftdi_oe_n,
        output reg ftdi_siwu_n,       
        
        output reg [7:0] rxd_o,
        output reg rxd_stb, // acknoledge
        output reg rxd_eof,
        output reg rxd_sof,
        input rxd_rdy, // ready
        input rxd_full,
        
        input [7:0] txd_i,
        input txd_stb,
        output reg txd_ack, //txd acknoledge, indicates that data is accepted and may be swapped
        input txd_eof, //EOF insertion, "txd_stb" must be deasserted
        input txd_sof //SOF insertion
    );
    localparam ESC = 8'hDB,
               EOF = 8'hD0,
               SOF = 8'hD3,
               ESC_SUB = 8'hDD;
    
    localparam TX_IDLE = 3'd0,
               TX_WAIT = 3'd1,
               TX_NORMAL = 3'd2,
               TX_STOP = 3'd3,
               TX_STORAGE1 = 3'd4,
               TX_STORAGE2 = 3'd5,
               TX_Q = 3'd6;
    
    localparam RXFIFO_FULL = 11'd1984;    

    
    reg [7:0] adbus_in;
    always @(posedge ftdi_clkout)
        begin
            adbus_in <= ftdi_adbus;
        end
     
    reg rx_ena;
    reg tx_ena;
    reg state_rx_tx;
    wire txfifo_empty;
    reg ftdi_txe_n_d1;
    reg rxfifo_full;   
    
    always @(posedge ftdi_clkout or negedge nrst)
        begin
            if (!nrst)
                begin
                    rx_ena <= 1'b0; // разрешение приема, 0 - выкл.
                    tx_ena <= 1'b0; // разрешение передачи, 0 - выкл.
                    state_rx_tx <= 1'b0;
                    ftdi_txe_n_d1 <= 1'b0;
                end
            else
                begin
                    ftdi_txe_n_d1 <= ftdi_txe_n;
                    case (state_rx_tx)
                        1'b0: // чистое чтение
                            begin
                                rx_ena <= 1'b0;
                                if (!ftdi_rxf_n && !rxfifo_full)
                                    rx_ena <= 1'b1;
                                else
                                    begin
                                        if (!txfifo_empty)
                                            state_rx_tx <= 1'b1;
                                    end
                            end
                        1'b1: // чистая запись
                            begin
                                tx_ena <= 1'b0;
                                if (!ftdi_txe_n)
                                    tx_ena <= 1'b1;
                                    
                                if (txfifo_empty)
                                    begin
                                        state_rx_tx <= 1'b0;
                                        tx_ena <= 1'b0;
                                    end
                            end
                    endcase
                end
        end
        
    wire [10:0] rx_wrusedw;
    always @(posedge ftdi_clkout or negedge nrst)
        begin
            if (!nrst)
                rxfifo_full <= 1'b0;
            else
                rxfifo_full <= (rx_wrusedw >= RXFIFO_FULL) ? 1'b1 : 1'b0;
        end                

    reg rxfifo_wrq; 
    reg txfifo_rrq_reg;
    wire txfifo_rrq = tx_ena & txfifo_rrq_reg;
    wire [7:0] txfifo_q;
    reg tx_stop;
    reg dir;
    reg [2:0] tx_state;    
    reg [7:0] adbus_out_d1;
    reg [7:0] adbus_out_d2;
    reg ftdi_wr_n_d1;
    reg ftdi_wr_n_d2;
    reg ftdi_txe_n_d2;
    reg txfifo_empty_d1;
    reg txfifo_empty_d2;
    reg skip_tx_q;
    reg wr_n;

    reg [7:0] adbus_out;
    assign ftdi_adbus = (dir & ~ftdi_txe_n_d1) ? adbus_out : 8'hzz;
    assign ftdi_wr_n = wr_n | ftdi_txe_n_d1;
    always @(posedge ftdi_clkout or negedge nrst)
        begin
            if (~nrst)
                begin
                    ftdi_oe_n <= 1'b1;
                    ftdi_rd_n <= 1'b1;
                    wr_n <= 1'b1;
                    rxfifo_wrq <= 1'b0;
                    ftdi_siwu_n <= 1'b1;
                    txfifo_rrq_reg <= 1'b0;
                    adbus_out <= 8'h00;
                    adbus_out_d1 <= 8'h00;
                    adbus_out_d2 <= 8'h00;

                    ftdi_wr_n_d1 <= 1'b0;
                    ftdi_wr_n_d2 <= 1'b0;
                    
                    txfifo_empty_d1 <= 1'b0;
                    txfifo_empty_d2 <= 1'b0;
                    ftdi_txe_n_d2 <= 1'b0;
                    tx_state <= 3'd0;
                    dir <= 1'b0;
                    skip_tx_q <= 1'b0;
                    tx_stop <= 1'b0;
                end
            else
                begin
                    ftdi_siwu_n <= 1'b1;
                    // RX PART //
                    ftdi_rd_n <= 1'b1;
                    ftdi_oe_n <= 1'b1;
                    rxfifo_wrq <= 1'b0;
                    
                    if (!ftdi_rd_n)
                        rxfifo_wrq <= 1'b1;
                        
                                    
                    if (!ftdi_oe_n)
                        ftdi_rd_n <= 1'b0;
                    
                    if (!ftdi_rxf_n)
                        begin
                            if (rx_ena)
                                ftdi_oe_n <= 1'b0; // active level
                            else
                                begin
                                    ftdi_rd_n <= 1'b1;
                                    ftdi_oe_n <= 1'b1;
                                end
                        end
                    else
                        begin
                            ftdi_rd_n <= 1'b1; // negative level
                            rxfifo_wrq <= 1'b0;
                        end
                                
                    // TX PART // 
                    txfifo_rrq_reg <= 1'b0;
                    wr_n <= 1'b1;
                    dir <= 1'b0;
                    if (!tx_stop)
                        begin
                            adbus_out_d1 <= adbus_out;
                            adbus_out_d2 <= adbus_out_d1;
                            ftdi_wr_n_d1 <= wr_n;
                            ftdi_wr_n_d2 <= ftdi_wr_n_d1;
                            ftdi_txe_n_d2 <= ftdi_txe_n_d1;
                            txfifo_empty_d1 <= txfifo_empty;
                            txfifo_empty_d2 <= txfifo_empty_d1;
                        end
                   
                    case (tx_state)
                        TX_IDLE:
                            begin
                                tx_stop <= 1'b0;
                                skip_tx_q <= 1'b0;
                                if (tx_ena & ~txfifo_empty)
                                    begin
                                        tx_state <= TX_WAIT;
                                        txfifo_rrq_reg <= 1'b1;

                                    end
                            end
                        TX_WAIT:
                            begin
                                txfifo_rrq_reg <= 1'b1;
                                tx_state <= TX_NORMAL;
                            end
                        TX_NORMAL:
                            begin
                                if (tx_ena)
                                    begin
                                        adbus_out <= txfifo_q;
                                        wr_n <= 1'b0;
                                        dir <= 1'b1;
                                        if (~txfifo_empty)
                                            txfifo_rrq_reg <= 1'b1;
                                    end    
                                else
                                    begin
                                        tx_stop <= 1'b1;
                                        tx_state <= TX_STOP;
                                            
                                    end
                            end
                        TX_STOP:
                            begin
                                tx_state <= TX_IDLE;
                                
                                if (ftdi_txe_n_d1 && ~ftdi_wr_n_d1)
                                    tx_state <= TX_STORAGE1;
                                    
                                if (ftdi_txe_n_d2 && ~ftdi_wr_n_d2)
                                    tx_state <= TX_STORAGE2; 

                                if (txfifo_empty_d2)
                                    skip_tx_q <= 1'b1;
                            end
                        TX_STORAGE2:
                            begin
                                if (tx_ena)
                                    begin
                                        adbus_out <= adbus_out_d2;
                                        wr_n <= 1'b0;
                                        dir <= 1'b1;
                                        tx_state <= TX_STORAGE1;
                                    end
                            end
                        TX_STORAGE1:
                            begin
                                if (tx_ena)
                                    begin
                                        adbus_out <= adbus_out_d1;
                                        wr_n <= 1'b0;
                                        dir <= 1'b1;
                                        if (skip_tx_q)
                                            tx_state <= TX_IDLE;
                                        else
                                            tx_state <= TX_Q;
                                    end
                            end
                        TX_Q:
                            begin
                                if (tx_ena)
                                    begin
                                        adbus_out <= txfifo_q;
                                        wr_n <= 1'b0;
                                        dir <= 1'b1;
                                        tx_state <= TX_IDLE;
                                    end                
                            end
                    endcase   
                end
        end
    
    localparam DESTUFF_IDLE             = 3'd0,
               DESTUFF_ESC              = 3'd1,
               DESTUFF_SOF              = 3'd2,
               DESTUFF_DATA             = 3'd3,
               DESTUFF_ESC_SUB_OR_EOF   = 3'd4,
			   DESTUFF_WAIT				= 3'd5;
    reg rxfifo_rrq;
    //reg rxd_sof;
    //reg rxd_eof;
    //reg [7:0] rxd_o;
    //reg rxd_stb;
    reg [2:0] state_destuff;
    wire rxfifo_empty;
    wire [7:0] rxfifo_q;
    reg rxd_rdy_d;
    wire was_busy;

    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    rxfifo_rrq <= 1'b0;
                    rxd_sof <= 1'b0;
                    rxd_stb <= 1'b0;
                    rxd_o <= 8'h00;
                    rxd_eof <= 1'b0;
                    state_destuff <= DESTUFF_ESC;
                    rxd_rdy_d <= 1'b0;
                end
            else
                begin
                    rxd_sof <= 1'b0;
                    rxd_stb <= 1'b0;
                    rxd_eof <= 1'b0;
                    rxfifo_rrq <= 1'b0;
                    rxd_rdy_d <= rxd_rdy;
                    if (rxd_rdy)
                        begin
                            if (!rxfifo_empty && !was_busy)
                                begin
                                    case (state_destuff)
                                        DESTUFF_ESC:
                                            begin
                                                rxfifo_rrq <= 1'b1;
                                                if (rxfifo_q == ESC)
                                                    state_destuff <= DESTUFF_SOF;
                                                else
                                                    state_destuff <= DESTUFF_ESC; // reset destuffing if first byte isn't ESC
                                            end
                                        DESTUFF_SOF:
                                            begin
                                                rxfifo_rrq <= 1'b1;
                                                if (rxfifo_q == SOF)
                                                    begin
                                                        rxd_sof <= 1'b1;
                                                        state_destuff <= DESTUFF_DATA;
                                                    end
                                                else
                                                    state_destuff <= DESTUFF_ESC; // reset destuffing if first byte isn't ESC
                                            end
                                        DESTUFF_DATA:
                                            begin
                                                rxfifo_rrq <= 1'b1;
                                                if (rxfifo_q == ESC)
                                                    begin
                                                        state_destuff <= DESTUFF_ESC_SUB_OR_EOF;

                                                    end
                                                else
                                                    begin        
                                                        //if (!rxd_full)
                                                            begin
                                                                rxd_stb <= 1'b1;
                                                                rxd_o <= rxfifo_q;
                                                            end
                                                    end                    
                                            end
                                        DESTUFF_ESC_SUB_OR_EOF:
                                            begin
                                                begin
                                                    rxfifo_rrq <= 1'b1;
                                                    if (rxfifo_q == EOF)
                                                        begin
                                                            rxd_eof <= 1'b1;
                                                            state_destuff <= DESTUFF_ESC;
                                                        end
                                                    if (rxfifo_q == ESC_SUB)
                                                        begin
                                                            rxd_o <= ESC;
                                                            rxd_stb <= 1'b1;
                                                            state_destuff <= DESTUFF_DATA;
                                                        end
                                                end
                                            end
                                        default:
                                            begin
                                                state_destuff <= DESTUFF_ESC;
                                                rxfifo_rrq <= 1'b0;
                                            end
                                    endcase
                                end
                            else
                                rxfifo_rrq <= 1'b1;
                        end
                end   
        end
    assign was_busy = rxd_rdy & !rxd_rdy_d;
    localparam STUFF_IDLE       = 3'd0,
               STUFF_SOF        = 3'd1,
               STUFF_DATA       = 3'd2,
               STUFF_ESC_SUB    = 3'd3,
               STUFF_EOF        = 3'd4;
    reg txfifo_wrq;
    reg [2:0] state_stuff;
    reg [7:0] txd;
    //reg txd_ack;
    wire txfifo_full;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    txfifo_wrq <= 1'b0;
                    state_stuff <= STUFF_IDLE;
                    txd <= 8'h00;
                    txd_ack <= 1'b0;
                end
            else
                begin
                    txfifo_wrq <= 1'b0;
                    txd_ack <= 1'b0;
                    if (~txfifo_full)
                        begin
                            case (state_stuff)
                                STUFF_IDLE:
                                    begin
                                        if (txd_sof)
                                            begin
                                                state_stuff <= STUFF_SOF;
                                                txfifo_wrq <= 1'b1;
                                                txd <= ESC;
                                                
                                            end
                                    end
                                STUFF_SOF:
                                    begin
                                        txfifo_wrq <= 1'b1;
                                        txd <= SOF;
                                        state_stuff <= STUFF_DATA;
                                        txd_ack <= 1'b1;
                                    end
                                STUFF_DATA:
                                    begin
                                        if (txd_stb && ~txd_ack)
                                            begin
                                                txfifo_wrq <= 1'b1;
                                                txd <= txd_i;
                                                txd_ack <= 1'b1;
                                                if (txd_i == ESC)
                                                    begin
                                                        state_stuff <= STUFF_ESC_SUB;
                                                        txd_ack <= 1'b0;
                                                    end
                                            end
                                        if (txd_eof)
                                            begin
                                                txfifo_wrq <= 1'b1;
                                                txd <= ESC;
                                                state_stuff <= STUFF_EOF;
                                                txd_ack <= 1'b1;
                                            end
                                    end
                                STUFF_ESC_SUB:
                                    begin
                                        txfifo_wrq <= 1'b1;
                                        txd <= ESC_SUB;
                                        txd_ack <= 1'b1;
                                        state_stuff <= STUFF_DATA;
                                    end
                                STUFF_EOF:
                                    begin
                                        txfifo_wrq <= 1'b1;
                                        txd <= EOF;
                                        state_stuff <= STUFF_IDLE;
                                    end    
                            endcase
                        end
                        
                end              
        end   
    
        
    usb_rxfifo rxfifo0 //show-ahead fifo
    (
        .aclr(~nrst),

        .wrclk(ftdi_clkout),
        .wrreq(rxfifo_wrq),
        .data(adbus_in),
        
        .rdclk(clk),
        .rdreq(rxfifo_rrq & rxd_rdy), //TODO: подумать как улучшить, может быть сделать busy на такт раньше в блоке выше (pakman)
        .q(rxfifo_q),
        
        .rdempty(rxfifo_empty),
        .rdusedw(),
        .rdfull(),
        
        .wrempty(),
        .wrfull(),
        .wrusedw(rx_wrusedw)
    );
    
    usb_txfifo txfifo0 //normal fifo
    (
        .aclr(~nrst),
        
        .wrclk(clk),
        .wrreq(txfifo_wrq),
        .data(txd),
        
        .rdclk(ftdi_clkout),
        .rdreq(txfifo_rrq), // rrq держится лишний такт, если rdempty = 1, но на схему не влияет 
        .q(txfifo_q),
        
        .rdempty(txfifo_empty),
        .rdusedw(),
        .rdfull(),
        
        .wrempty(),
        .wrfull(txfifo_full),
        .wrusedw()
    );        
endmodule
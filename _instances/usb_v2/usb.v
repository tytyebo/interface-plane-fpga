module usb(
	input nrst,
	input clk,
	
	//FTDI USB
	inout [7:0] ftdi_adbus,
	input ftdi_rxf_n,
	input ftdi_txe_n,
	output ftdi_rd_n ,
	output ftdi_wr_n ,
	output ftdi_siwu_n,
	input ftdi_clkout,
	output ftdi_oe_n ,
	
	output [15:0] wbm_adr_o,
	output [31:0] wbm_dat_o,
    output [3:0] wbm_sel_o,
	output wbm_cyc_o,
	output wbm_stb_o,
	output wbm_we_o,
	input [31:0] wbm_dat_i,
	input wbm_ack_i
);

wire txd_stb;
wire txd_ack;
wire txd_eof;
wire txd_sof;
wire [7:0]txd_dat;

wire rxd_stb;
wire [7:0]rxd_dat;
wire rxd_sof;
wire rxd_eof;
wire rxd_busy;
wire rxd_full;

wire rxpak_req;
wire [7:0]rxpak_dout;
wire rxpak_rdy;
wire rxpak_ack;


wire txpak_ack;
wire txpak_stb;
wire txpak_rdy;
wire txpak_done;
wire [7:0]txpak_dout;

wire [7:0] burst_len;
wire [1:0] burst_inc;
wire [3:0] sel;
wire we;


usb_fifo_v2 usb_fifo0
    (
        .nrst(nrst),
        .clk(clk),
        
        .ftdi_adbus(ftdi_adbus),
        .ftdi_rxf_n(ftdi_rxf_n),
        .ftdi_txe_n(ftdi_txe_n),
        .ftdi_rd_n (ftdi_rd_n),
        .ftdi_wr_n (ftdi_wr_n),
        .ftdi_siwu_n(ftdi_siwu_n),
        .ftdi_clkout(ftdi_clkout),	//FTDI workds on rising edge of it's clock. Both for data output and input
        .ftdi_oe_n (ftdi_oe_n),
               
        .rxd_o  (rxd_dat),
        .rxd_stb(rxd_stb),
        .rxd_eof(rxd_eof),
        .rxd_sof(rxd_sof),
        .rxd_rdy(~rxd_busy),
        .rxd_full(rxd_full),
        
        .txd_i  (txd_dat),
        .txd_stb(txd_stb),
        .txd_ack(txd_ack), //txd acknoledge, indicates that data is accepted and may be swapped
        .txd_eof(txd_eof), //EOF insertion, "txd_stb" must be deasserted
        .txd_sof(txd_sof)  //SOF insertion
    );

usb_packer packer0
    (
        .nrst(nrst),
        .clk(clk),
        
        .rxd_stb(rxd_stb),
        .rxd_dat(rxd_dat),
        .rxd_sof(rxd_sof),
        .rxd_eof(rxd_eof),
        .rxd_busy(rxd_busy),
        .rxd_full(rxd_full),
            
        .txd_stb(txd_stb),
        .txd_dat(txd_dat),
        .txd_ack(txd_ack),
        .txd_sof(txd_sof),
        .txd_eof(txd_eof),
        
        .rxpak_req(rxpak_req),
        .rxpak_ack(rxpak_ack),
        .rxpak_rdy(rxpak_rdy),
        .rxpak_dout(rxpak_dout),

        .txpak_rdy(txpak_rdy),
        .txpak_stb(txpak_stb),
        .txpak_din(txpak_dout),
        .txpak_ack(txpak_ack),
        .txpak_done(txpak_done),
        
        .burst_len(burst_len),
        .burst_inc(burst_inc),
        .sel(sel),
        .we(we)    
    );

usb_bridge2wb bridge0
    (
        .nrst(nrst),
        .clk(clk),
        
        .burst_len(burst_len),
        .burst_inc(burst_inc),
        .sel(sel),
        .we(we),
        
        .rxpak_rdy(rxpak_rdy),
        .rxpak_din(rxpak_dout),
        .rxpak_req(rxpak_req),
        .rxpak_ack(rxpak_ack),

        .txpak_ack(txpak_ack),
        .txpak_stb(txpak_stb),
        .txpak_dout(txpak_dout),
        .txpak_rdy(txpak_rdy),
        .txpak_done(txpak_done),
        
        .wb_adr_o(wbm_adr_o),
        .wb_dat_o(wbm_dat_o),
        .wb_sel_o(wbm_sel_o),
        .wb_cyc_o(wbm_cyc_o),
        .wb_stb_o(wbm_stb_o),
        .wb_we_o(wbm_we_o),
        .wb_dat_i(wbm_dat_i),
        .wb_ack_i(wbm_ack_i)
    );
    
endmodule

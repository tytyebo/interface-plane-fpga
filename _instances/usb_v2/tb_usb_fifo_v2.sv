`timescale 1ps/1ps
module tb_usb_fifo_v2();

    localparam ESC = 8'hDB;
    localparam EOF = 8'hD0;
    localparam SOF = 8'hD3;
    localparam ESC_SUB = 8'hDD;
    
    reg nrst;
    reg clk;
    reg ftdi_clk;

    initial 
        begin
            nrst <= 1'b0;
            #1000
            nrst <= 1'b1;
        end
    
    initial
        begin
            clk <= 0;
            forever 
                begin
                    #6.25 clk <= ~clk; // 80 MHz
                end
        end
    
    initial
        begin
            ftdi_clk <= 0;
            forever 
                begin
                    #8.333 ftdi_clk <= ~ftdi_clk; // 60 MHz
                end
        end
    
    reg ftdi_rxf_n;
    reg ftdi_txe_n;
    wire ftdi_rd_n;
    wire ftdi_wr_n;
    wire ftdi_siwu_n;
    wire ftdi_oe_n;
    wire [7:0] ftdi_adbus;
    
    reg [7:0] ftdi_dout;
    reg [7:0] chk;
    
    task ftdi_tx_byte;
        input [7:0] data;
        begin
            do 
                begin
                    ftdi_dout <= data;
                    @(posedge ftdi_clk);
                end
            while (ftdi_rd_n || ftdi_oe_n || ftdi_rxf_n);
        end
        chk = chk + data;
    endtask  
    
    assign ftdi_adbus = (ftdi_oe_n) ? 8'hzz : ftdi_dout;

    reg rxd_rdy;    
    
    reg [7:0] txd;
    reg txd_sof;
    reg txd_eof;
    reg txd_stb;
    wire txd_ack;
    task tx_byte;
        input [7:0] data;
        begin
            case (data)
                SOF:
                    txd_sof <= 1'b1;
                EOF:
                    txd_eof <= 1'b1;
                default:
                    txd <= data;
            endcase
            txd_stb <= 1'b1;
            do         
                @(posedge clk);
            while (~txd_ack);
            txd <= 8'h00;
            txd_stb <= 1'b0;
            txd_sof <= 1'b0;
            txd_eof <= 1'b0;
        end
    endtask  
    
    
    initial
        begin
            ftdi_rxf_n <= 1'b1;
            ftdi_txe_n <= 1'b1;
            rxd_rdy <= 1'b1;
            txd <= 8'h00;
            txd_sof <= 1'b0;
            txd_eof <= 1'b0;
            txd_stb <= 1'b0;
            repeat (50) 
                @(posedge ftdi_clk);
            ftdi_rxf_n <= 1'b0;
            ftdi_tx_byte(ESC);
            ftdi_tx_byte(SOF);
            ftdi_tx_byte(8'h01);
            ftdi_tx_byte(8'h02);
            //ftdi_tx_byte(ESC);
            //ftdi_tx_byte(ESC_SUB);
            ftdi_tx_byte(8'h03);
            ftdi_tx_byte(8'h04);
            ftdi_tx_byte(8'h05);
            ftdi_tx_byte(8'h06);
            ftdi_tx_byte(8'h07);
            ftdi_tx_byte(8'h08);
            ftdi_tx_byte(8'h09);
            ftdi_tx_byte(8'h0A);
            ftdi_tx_byte(ESC);
            ftdi_tx_byte(EOF);
            ftdi_rxf_n <= 1'b1;
            
            ftdi_txe_n <= 1'b0;
            @(posedge clk);
            rxd_rdy <= 1'b1;
            repeat (10) 
                @(posedge clk);             
            tx_byte(SOF); 
            tx_byte(8'h01);
            tx_byte(8'h02);
            tx_byte(8'h03);
            tx_byte(8'h04);
            ftdi_txe_n <= 1'b1;
            repeat (5) 
                @(posedge clk); 
            ftdi_txe_n <= 1'b0;
            tx_byte(8'h05);
            tx_byte(8'h06);
            tx_byte(8'h07);
            tx_byte(8'h08);
            tx_byte(EOF);
            

                
        end
usb_fifo_v2 usb_fifo0(
	.nrst(nrst),
	.clk(clk),
	
	.ftdi_adbus (ftdi_adbus),
	.ftdi_rxf_n (ftdi_rxf_n),
	.ftdi_txe_n (ftdi_txe_n),
	.ftdi_rd_n  (ftdi_rd_n),
	.ftdi_wr_n  (ftdi_wr_n),
	.ftdi_siwu_n(ftdi_siwu_n),
	.ftdi_clkout(ftdi_clk),
	.ftdi_oe_n  (ftdi_oe_n),
	
	
	.rxd_o  (),
	.rxd_stb(),
	.rxd_eof(),
    .rxd_sof(),
	.rxd_rdy(rxd_rdy),
	
	.txd_i  (txd),
	.txd_stb(txd_stb),
	.txd_ack(txd_ack),
	.txd_eof(txd_eof),
    .txd_sof(txd_sof)
);

endmodule

module usb_packer
    (
        input nrst,
        input clk,
        
        input rxd_stb,
        input [7:0] rxd_dat,
        input rxd_sof,
        input rxd_eof,
        output rxd_busy,
        output rxd_full,
            
        output txd_stb,
        output [7:0] txd_dat,
        input txd_ack,
        output txd_sof,
        output txd_eof,
        
        input rxpak_req,
        input  rxpak_ack,
        output rxpak_rdy,
        output [7:0] rxpak_dout,

        input txpak_done,
        input txpak_stb,
        input [7:0] txpak_din,
        output txpak_ack,
        output txpak_rdy,
        
        output [7:0] burst_len,
        output [1:0] burst_inc,
        output [3:0] sel,
        output we
    );
    
    localparam RXP_IDLE = 3'd0,
               RXP_WAIT_HEAD = 3'd1,
               RXP_COMMAND_LOW = 3'd2,
               RXP_COMMAND_HIGH = 3'd3,
               RXP_PARAM_LOW = 3'd4,
               RXP_PARAM_HIGH = 3'd5,
               RXP_WAIT_ACK = 3'd6;
               
    reg [3:0] rxp_state;
    reg [15:0] rxp_command;
    reg [15:0] rxp_param;
    reg rxp_req;
    reg rxp_reset;
    reg rxp_head_success;
    
    reg [1:0] rxp_cnt;
    reg rxp_error;
    reg if_rxd_sof;
    reg rxp_rdy;
    
    wire [7:0] rxp_q;
    wire rxp_rdreq;
    wire rxp_empty;
    wire [10:0] rxp_usedw;
    

    assign rxd_busy = (rxp_cnt == 2'd2) || rxd_full;
    assign rxpak_rdy = (rxp_cnt != 2'd0) && !(rxp_error || command_error) && rxp_rdy; // TODO: сделать регистром!
    assign rxpak_dout = rxp_q;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    rxp_state <= RXP_IDLE;
                    rxp_command <= 16'd0;
                    rxp_param <= 16'd0;
                    rxp_req <= 1'b0;
                    rxp_reset <= 1'b0;
                    rxp_cnt = 2'd0; // Важно! Блокируещее присваивание!
                    rxp_error <= 1'b0;
                    if_rxd_sof <= 1'b0;
                    rxp_rdy <= 1'b0;
                end
            else
                begin
                    rxp_req <= 1'b0;
                    rxp_reset <= 1'b0;
                    rxp_head_success <= 1'b0;
                    rxp_rdy <= 1'b0;                    

                      
                    if (rxp_error || command_error)
                        begin
                            if (rxp_cnt == 2'd1)
                                begin
                                    rxp_reset <= 1'b1;
                                    rxp_error <= 1'b0;
                                end
                        end
                    
                    if (rxd_eof)
                        rxp_cnt = rxp_cnt + 1'b1; // Важно! Блокируещее присваивание!
                    
                    if (rxd_eof)
                        begin
                            if (rxp_chk[7:0] != 8'hff)
                                rxp_error <= 1'b1;
                        end
                        
                    if (rxpak_ack)
                        rxp_cnt = rxp_cnt - 1'b1; // Важно! Блокируещее присваивание!
                        
                    if (rxd_sof)
                        if_rxd_sof <= 1'b1;
                        
                    case (rxp_state)
                        RXP_IDLE:
                            begin
                                if (rxd_sof | if_rxd_sof)
                                    begin
                                        if_rxd_sof <= 1'b0;
                                        rxp_state <= RXP_WAIT_HEAD;
                                    end
                            end
                        RXP_WAIT_HEAD:
                            begin
                                if (rxp_usedw >= 11'd4)
                                    begin
                                        rxp_state <= RXP_COMMAND_LOW;
                                        rxp_req <= 1'b1;
                                    end    
                            end    
                        RXP_COMMAND_LOW:
                            begin
                                rxp_command[7:0] <= rxp_q;
                                rxp_state <= RXP_COMMAND_HIGH;
                                rxp_req <= 1'b1;
                            end
                        RXP_COMMAND_HIGH:
                            begin
                                rxp_command[15:8] <= rxp_q;
                                rxp_state <= RXP_PARAM_LOW;
                                rxp_req <= 1'b1;
                            end
                        RXP_PARAM_LOW:
                            begin
                                rxp_param[7:0] <= rxp_q;
                                rxp_state <= RXP_PARAM_HIGH;
                                rxp_req <= 1'b1;
                            end
                        RXP_PARAM_HIGH:
                            begin
                                rxp_param[15:8] <= rxp_q;
                                rxp_state <= RXP_WAIT_ACK;
                                rxp_head_success <= 1'b1;
                            end
                        RXP_WAIT_ACK:
                            begin
                                rxp_rdy <= 1'b1;
                                if (rxpak_ack)
                                    begin
                                        rxp_state <= RXP_IDLE;
                                        rxp_rdy <= 1'b0;
                                    end
                            end
                        default:
                            rxp_state <= RXP_IDLE;
                    endcase
                end
        end
    
    reg [7:0] burst_len_1;
    reg [1:0] burst_inc_1;
    reg [3:0] sel_1;
    reg we_1;
    reg [7:0] burst_len_2;
    reg [1:0] burst_inc_2;
    reg [3:0] sel_2;
    reg we_2;
    reg [10:0] burst_byte_len;
    reg [2:0] sel_cnt;
    reg rxp_read;
    reg [1:0] cmd_cnt;
    assign burst_len = burst_len_1;
    assign burst_inc = burst_inc_1;
    assign sel = sel_1;
    assign we = we_1;
    always @(posedge clk or negedge nrst)
        begin
             if (~nrst)
                begin
                    burst_len_1 <= 8'd0;
                    burst_inc_1 <= 2'd0;
                    sel_1 <= 4'd0;
                    we_1 <= 1'b0;
                    
                    burst_len_2 <= 8'd0;
                    burst_inc_2 <= 2'd0;
                    sel_2 <= 4'd0;
                    we_2 <= 1'b0;
                    
                    burst_byte_len <= 11'd0;
                    sel_cnt = 3'd0; // Важно! Блокируещее присваивание!
                    cmd_cnt <= 2'd0;
                    command_error <= 1'b0;
                    rwe = 1'b0;
                    rxp_read <= 1'b0;
                end         
            else            
                begin
                    if (rxp_reset)
                        begin
                            burst_len_1 <= 8'd0;
                            burst_inc_1 <= 2'd0;
                            sel_1 <= 4'd0;
                            we_1 <= 1'b0;
                            
                            burst_len_2 <= 8'd0;
                            burst_inc_2 <= 2'd0;
                            sel_2 <= 4'd0;
                            we_2 <= 1'b0;
                            
                            burst_byte_len <= 11'd0;
                            sel_cnt = 3'd0; // Важно! Блокируещее присваивание!
                            cmd_cnt <= 2'd0;
                            command_error <= 1'b0;
                            rwe = 1'b0; // Важно! Блокируещее присваивание!  
                            rxp_read <= 1'b0;
                        end
                    else  
                        if (txp_state == TXP_IDLE)
                            rxp_read <= 1'b0;                        

                        if (rxp_head_success)
                            begin

                                command_error <= 1'b0;
                                case (rxp_command[3:0])
                                    4'h1:
                                        begin
                                            rwe = 1'b0; // Важно! Блокируещее присваивание!
                                            rxp_read <= 1'b1;
                                        end
                                    4'h2:
                                        rwe = 1'b1; // Важно! Блокируещее присваивание!  
                                    default:
                                        begin
                                            command_error <= 1'b1;
                                            rwe = 1'b0; // Важно! Блокируещее присваивание!  
                                        end
                                endcase
                                cmd_cnt <= cmd_cnt + 1'b1;
                                
                                if (cmd_cnt == 2'd0)
                                    begin
                                        burst_len_1 <= wburst_len;
                                        burst_inc_1 <= wburst_inc;
                                        sel_1 <= wsel;
                                        we_1 <= rwe;
                                    end
                                if (cmd_cnt == 2'd1)
                                    begin
                                        burst_len_2 <= wburst_len;
                                        burst_inc_2 <= wburst_inc;
                                        sel_2 <= wsel;
                                        we_2 <= rwe;
                                    end
                            end
                            
                        if (rxpak_ack)
                            begin
                                cmd_cnt <= cmd_cnt - 1'b1;
                                
                                if (cmd_cnt == 2'd2)
                                    begin
                                        burst_len_1 <= burst_len_2;
                                        burst_inc_1 <= burst_inc_2;
                                        sel_1 <= sel_2;
                                        we_1 <= we_2;
                                    end
                                if (cmd_cnt == 2'd1)
                                    begin
                                        burst_len_1 <= 8'd0;
                                        burst_inc_1 <= 2'd0;
                                        sel_1 <= 4'd0;
                                        we_1 <= 1'b0;
                                    end  
                            end
                        if (rxpak_ack & rxp_head_success)
                            cmd_cnt <= cmd_cnt;
                            
                        if (cmd_cnt == 2'd0)
                            begin
                                sel_cnt = wsel[3] + wsel[2] + wsel[1] + wsel[0]; // Важно! Блокируещее присваивание!                     
                                case (sel_cnt)
                                    1: burst_byte_len <= wburst_len + 11'd6;
                                    2: burst_byte_len <= wburst_len + wburst_len + 11'd7;
                                    3: burst_byte_len <= wburst_len + wburst_len + wburst_len + 11'd8;
                                    4: burst_byte_len <= wburst_len + wburst_len + wburst_len + wburst_len + 11'd9;
                                    default: burst_byte_len <= wburst_len;
                                endcase
                            end
                        else
                            begin
                                sel_cnt = sel_1[3] + sel_1[2] + sel_1[1] + sel_1[0]; // Важно! Блокируещее присваивание!                     
                                case (sel_cnt)
                                    1: burst_byte_len <= burst_len_1 + 11'd6;
                                    2: burst_byte_len <= burst_len_1 + burst_len_1 + 11'd7;
                                    3: burst_byte_len <= burst_len_1 + burst_len_1 + burst_len_1 + 11'd8;
                                    4: burst_byte_len <= burst_len_1 + burst_len_1 + burst_len_1 + burst_len_1 + 11'd9; //+1 держим в уме
                                    default: burst_byte_len <= burst_len_1;
                                endcase
                            end
                        
                end
        end      
        
    reg [7:0] rxp_chk;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin        
                    rxp_chk <= 8'd0;
                end
            else
                if (rxd_sof)
                    begin
                        rxp_chk <= 8'd0;
                    end
                else 
                    begin
                        if (rxd_stb)
                            begin
                                rxp_chk <= rxp_chk + rxd_dat;
                            end
                    end
        end
        
    pack_fifo rxpak
    (
        .clock(clk),
        .sclr (~nrst | rxp_reset),
        .wrreq(rxd_stb),
        .data (rxd_dat),
        .rdreq(rxpak_req | rxp_req),
        .q    (rxp_q),
        .empty(),
        .full (),        
        .usedw(rxp_usedw)
    );
    assign rxd_full = (rxp_usedw > 11'd2032) ? 1'b1 : 1'b0;
    
    reg rwe; // = 0 - чтение, = 1 - запись.
    reg command_error;
    
    wire [7:0] wburst_len = rxp_param[7:0]; // +1, т.е. = 0 - однократная операция, = 1 - две операции и т.д.
    wire [1:0] wburst_inc = rxp_param[9:8]; // = 0 - нет инкремента, = 1 - инкремент на +1 и т.д.
    wire [3:0] wsel = |rxp_param[13:10] ? rxp_param[13:10] : 4'b1111; // выбор байта данных для чтения или записи в шине wishbone, т.е. 4'b0001 - младший байт, 4'b1000 - старший байт и т.д. Значение 4'b0000 ошибочно, поэтому автоматически исправляется на 4'b1111.

    /*
     * РћРїРёСЃР°РЅРёРµ РїРµСЂРµРґР°С‚С‡РёРєР° (TX)
     */
    localparam TXP_IDLE = 3'd0,
               TXP_COMMAND_LOW = 3'd1,
               TXP_COMMAND_HIGH = 3'd2,
               TXP_PARAM_LOW = 3'd3,
               TXP_PARAM_HIGH = 3'd4,
               TXP_DATA = 3'd5;
               
    reg [2:0] txp_state;
    reg [7:0] txp_dbuf;
    reg txp_req;
    reg txp_ack;
    reg txp_rdy;
    assign txpak_ack = txp_ack;
    assign txpak_rdy = txp_rdy;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    txp_state <= TXP_IDLE;
                    txp_req <= 1'b0;
                    txp_ack <= 1'b0;
                    txp_dbuf <= 8'd0;
                    txp_rdy <= 1'b0;
                end
            else
                begin
                    txp_req <= 1'b0;
                    txp_ack <= 1'b0;
                    txp_rdy <= 1'b0;
                    case (txp_state)
                        TXP_IDLE:   
                            begin
                                if (rxp_read)
                                    txp_state <= TXP_COMMAND_LOW;
                            end
                        TXP_COMMAND_LOW:
                            begin
                                txp_dbuf <= rxp_command[7:0];
                                txp_req <= 1'b1;
                                txp_state <= TXP_COMMAND_HIGH;
                            end
                        TXP_COMMAND_HIGH:
                            begin
                                txp_dbuf <= rxp_command[15:8];
                                txp_req <= 1'b1;
                                txp_state <= TXP_PARAM_LOW;
                            end  
                        TXP_PARAM_LOW:
                            begin
                                txp_dbuf <= rxp_param[7:0];
                                txp_req <= 1'b1;
                                txp_state <= TXP_PARAM_HIGH;
                            end
                        TXP_PARAM_HIGH:
                            begin
                                txp_dbuf <= rxp_param[15:8];
                                txp_req <= 1'b1;
                                txp_state <= TXP_DATA;
                                txp_rdy <= 1'b1;
                            end
                        TXP_DATA:
                            begin
                                txp_rdy <= 1'b1;
                                if (txpak_stb)
                                    begin
                                        txp_req <= 1'b1;
                                        txp_dbuf <= txpak_din;
                                    end
                                if (txpak_done)
                                    begin
                                        txp_ack <= 1'b1;
                                        txp_state <= TXP_IDLE;
                                    end  
                            end
                        default:
                            txp_state <= TXP_IDLE;
                    endcase
                end
        end
    
    reg [7:0] txp_chk;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                txp_chk <= 8'd0;
            else
                if (sof)
                    txp_chk <= 8'd0;
                else 
                    if (txd_req_ena & txd_ack)
                        txp_chk <= txp_chk + txd_data;
        end 

    wire txp_fifo_empty;
    wire [7:0] txd_data;
    assign txd_dat = (txd_checksum) ? ~txp_chk : txd_data;
    pack_fifo txpak
        (
            .clock(clk),
            .sclr (~nrst),
            .wrreq(txp_req),
            .data (txp_dbuf),
            
            .rdreq(txd_req_ena & txd_ack),
            .q(txd_data),
            
            .empty(txp_fifo_empty),
            .full (),
            .usedw()
        );
    
    localparam TXD_IDLE = 3'd0,
               TXD_DATA = 3'd1,
               TXD_ACK = 3'd2,
               TXD_CHECKSUM = 3'd3,
               TXD_EOF = 3'd4,
               TXD_WAIT = 3'd5;
    reg sof;
    reg eof;
    reg stb;
    reg txd_req_ena;
    reg txd_ack_ready;
    reg [10:0] burst_cnt;
    reg txd_checksum;
    reg [2:0] txd_state;
    // reg rrq;
    assign txd_stb = stb & ~txp_fifo_empty | txd_checksum;
    assign txd_sof = sof;
    assign txd_eof = eof;
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    txd_state <= TXD_IDLE;
                    burst_cnt <= 11'd0;
                    sof <= 1'b0;
                    eof <= 1'b0;
                    stb <= 1'b0;
                    txd_req_ena <= 1'b0;
                    txd_checksum <= 1'b0;
                    txd_ack_ready <= 1'b0;
                    // rrq <= 1'b0;
                end
            else
                begin
                    sof <= 1'b0;                    
                    eof <= 1'b0;
                    stb <= 1'b0;
                    
                    if (txd_ack)
                        begin
                            txd_ack_ready <= 1'b1;
                            stb <= 1'b0;  
                            txd_checksum <= 1'b0;
                        end

                        
                    case (txd_state)
                        TXD_IDLE:
                            begin
                                if (~txp_fifo_empty)
                                    begin
                                        sof <= 1'b1;
                                        burst_cnt <= burst_byte_len;
                                        txd_state <= TXD_DATA;
                                    end
                            end
                        TXD_DATA:
                            begin                             
                                if (~txp_fifo_empty)
                                    begin
                                        // rrq <= 1'b0;
                                        stb <= 1'b1;
                                        if (txd_ack | txd_ack_ready)
                                            begin
                                                txd_req_ena <= 1'b1;
                                                txd_ack_ready <= 1'b0;
                                                burst_cnt <= burst_cnt - 1'b1;
                                                if (burst_cnt == 0)
                                                    txd_state <= TXD_CHECKSUM;
                                            end
                                    end
                                // else
                                    // rrq <= 1'b1;
                            end
                        TXD_CHECKSUM:
                            begin
                                stb <= 1'b1;
                                if (txd_ack)
                                    begin
                                        txd_ack_ready <= 1'b0;
                                        txd_req_ena <= 1'b0;
                                        txd_checksum <= 1'b1;
                                        txd_state <= TXD_EOF;
                                    end
                            end
                        TXD_EOF:
                            begin
                                stb <= 1'b1;
                                if (txd_ack)
                                    begin
                                        eof <= 1'b1;
                                        txd_ack_ready <= 1'b0;
                                        stb <= 1'b0;
                                        txd_state <= TXD_WAIT;
                                    end
                            end
                        TXD_WAIT:
                            begin
                                if (txd_ack)
                                    begin
                                        txd_ack_ready <= 1'b0;
                                        txd_state <= TXD_IDLE;
                                    end
                            end
                        default:
                            begin
                                txd_state <= TXD_IDLE;
                            end
                    endcase
                end
        end
endmodule
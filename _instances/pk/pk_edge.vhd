-- ## First Code : 16-05-2016               ##
-- ## First Compilation: 20:43 23-05-2016   ##
-- ## First Simulation: 11:40 24-05-2016   ##
-- ## Author     : Sergey Romanov           ##
-- ## Company    : NII VS @ SU              ##

-- 24.04.2017 :: Comment SI_FILT

LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE IEEE.std_logic_unsigned.all;

LIBRARY WORK;
USE WORK.exinf_se_package.all;

ENTITY pk_edge is 
	port(
		-- Signal Input
		SI : IN std_logic;
		
		-- PK-edge Outputs
		SI_RISE : OUT std_logic;
		SI_FALL : OUT std_logic;
		--SI_FILT : OUT std_logic;
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';
		CLK : IN std_logic
	);
END pk_edge;

ARCHITECTURE SEHDL of pk_edge is

-- SECTION 1. COMPONENTS
COMPONENT metastable_chain	-- ��������� ������������ �������
	GENERIC (CHAIN_DEPTH : POSITIVE;
             BUS_WIDTH : POSITIVE);
	PORT
	(
		din		:	 IN STD_LOGIC_VECTOR(BUS_WIDTH-1 DOWNTO 0);
		dout	:	 OUT STD_LOGIC_VECTOR(BUS_WIDTH-1 DOWNTO 0);
		clk		:	 IN STD_LOGIC;
        nrst	:	 IN STD_LOGIC
	);
END COMPONENT;

-- SECTION 2. SUBPROGRAMS (������� ��������������)
function  maj5(b : in std_logic_vector(4 downto 0)) return  std_logic is	-- majority function, size 5
	variable c0, c1 : natural := 0;
	variable mj : std_logic;
begin
	for i in 4 downto 0 loop
		if b(i) = '1' then c1 := c1 + 1; else c0 := c0 + 1; end if;
	end loop;
	if c1 > c0 then mj := '1'; else mj := '0'; end if;
	return mj;
end maj5;

-- SECTION 3. SIGNALS
signal si_mst : std_logic_vector(0 downto 0);

signal si_bus : std_logic_vector(0 downto 0);

signal si_maj_rg : std_logic_vector(3 downto 0);
signal si_maj : std_logic;

signal si_main_maj_rg : std_logic_vector(63 downto 0);
signal si_main_maj_cnt : std_logic_vector(6 downto 0);

signal si_main_maj, si_main_majz : std_logic;

signal si_main_maj_ctrl_rst_cnt : std_logic_vector(5 downto 0);
signal si_main_maj_ctrl_reset : std_logic;

signal si_edge_rise, si_edge_fall : std_logic;

BEGIN
si_bus(0) <= SI;
-- 1 :: SI MST Chain
si_msch_cmp : metastable_chain
GENERIC MAP(CHAIN_DEPTH => MSN,
            BUS_WIDTH => 1)
PORT MAP(din => si_bus, dout => si_mst, clk => CLK, nrst => '1');

-- 2 :: SI Majority Block
si_maj_proc: process(CLK, nRST)
variable si_mst_bus : std_logic_vector(4 downto 0);
begin
	if nRST = '0' then
		si_maj_rg <= (others => '0');
		si_maj <= '0';
	elsif CLK'event and (CLK = '1') then
		for i in 3 downto 1 loop
			si_maj_rg(i) <= si_maj_rg(i-1);	-- ����� ��������
		end loop;
		si_maj_rg(0) <= si_mst(0);
		-- ���������� ������������� �������� (������ ���� ��������� �������)
		si_mst_bus := si_maj_rg(3) & si_maj_rg(2) & si_maj_rg(1) & si_maj_rg(0) & si_mst(0);
		si_maj <= maj5(si_mst_bus);
	end if;
end process;

-- 3 :: SI Main Majority Block
si_main_maj_proc: process(CLK, nRST)
variable mi, mo : std_logic;
begin
	if nRST = '0' then
		si_main_maj_rg <= (others => '0');
		si_main_maj_cnt <= (others => '0');
		si_main_maj <= '0';
		si_main_majz <= '0';
		si_main_maj_ctrl_rst_cnt <= (others => '0');
		si_main_maj_ctrl_reset <= '0';
	elsif CLK'event and (CLK = '1') then
		for i in 63 downto 1 loop
			si_main_maj_rg(i) <= si_main_maj_rg(i-1);	-- ����� ��������
		end loop;
		si_main_maj_rg(0) <= si_maj;
		-- ���������� ������������� �������� (������ ���� ��������� �������)
		mi := si_maj;
		mo := si_main_maj_rg(63);
		if (mi and not mo) = '1' then		-- Increment
			si_main_maj_cnt <= si_main_maj_cnt + 1;
		elsif (mo and not mi) = '1' then	-- Decrement
			si_main_maj_cnt <= si_main_maj_cnt - 1;
		elsif si_main_maj_ctrl_reset = '1' then
			si_main_maj_cnt <= (others => '0');
		end if;
		si_main_maj <= si_main_maj_cnt(6) or si_main_maj_cnt(5);
		si_main_majz <= si_main_maj;
		-- ����� ������������ ������
		if si_maj = '0' then
			if si_main_maj_ctrl_rst_cnt = 62 then
				si_main_maj_ctrl_reset <= '1';
			else
				si_main_maj_ctrl_rst_cnt <= si_main_maj_ctrl_rst_cnt + 1;
			end if;
		else
			si_main_maj_ctrl_rst_cnt <= (others => '0');
			si_main_maj_ctrl_reset <= '0';
		end if;
	end if;
end process;

-- 4 :: SI Edge Block
si_edge_proc: process(CLK, nRST)
begin
	if nRST = '0' then
		si_edge_rise <= '0';
		si_edge_fall <= '0';		
	elsif CLK'event and (CLK = '1') then
		si_edge_rise <= si_main_maj and not si_main_majz;
		si_edge_fall <= not si_main_maj and si_main_majz;
	end if;
end process;

SI_RISE <= si_edge_rise;
SI_FALL <= si_edge_fall;
--SI_FILT <= si_main_majz;

END SEHDL;
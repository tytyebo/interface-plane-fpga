`timescale 1ps/1ps
module Parallel_Channel_Controller
    (
        nrst,
        clk,
        
        m4,
        rq,
        
        stp_clk,
        stp_sdi,
        stp_le,
        stp_noe,

        wb_adr_i,
        wb_dat_i,	
        wb_stb_i,
        wb_cyc_i,
        wb_sel_i,
        wb_we_i ,	
        wb_dat_o,
        wb_ack_o
    );
    input nrst;
    input clk;
    input m4;
    input rq;
    
    output stp_clk;
    output stp_sdi;
    output stp_le;
    output stp_noe;

    input [15:0] wb_adr_i;
    input [31:0] wb_dat_i;
    input wb_stb_i;
    input wb_cyc_i;
    input [3:0] wb_sel_i;
    input wb_we_i;
    output [31:0]wb_dat_o;
    output wb_ack_o;
    
    reg [2:0] mode;
    reg [31:0] fifo_data;
    reg ack_o;
    reg fifo_wrq;  
    reg test;
    reg [31:0] dat_o;
       
    wire fifo_full;
    wire [8:0] fifo_wudw;
    
    assign wb_ack_o = ack_o; 
    assign wb_dat_o = dat_o;
    
    always @(posedge clk or negedge nrst)
        if (~nrst)
            begin
                ack_o <= 1'b0;
                mode <= 3'b000;
                fifo_wrq <= 1'b0;
                fifo_data <= 32'h00000000;
                test <= 1'b0;
                dat_o <= 32'd0;
            end
        else
            begin
                ack_o <= 1'b0;
                fifo_wrq <= 1'b0;
                if (wb_stb_i && wb_cyc_i && ~wb_ack_o)
                    begin
                        ack_o <= 1'b1;
                        if (wb_we_i)
                            case (wb_adr_i[1:0])
                                2'd0: //fifo input register
                                    begin
                                        fifo_data <= {wb_dat_i[7:0], wb_dat_i[15:8], wb_dat_i[23:16], wb_dat_i[31:24]};
                                        fifo_wrq <= 1'b1;
                                    end
                                2'd1: //mode register
                                    {test, mode[2:0]} <= wb_dat_i[3:0];
                                2'd2:
                                    test <= wb_dat_i[0];
                            endcase
                        else 
                            case (wb_adr_i[1:0])
                                2'd0:
                                    dat_o <= {31'd0, fifo_full || (fifo_wudw > 9'd256 )};
                                2'd1:
                                    dat_o <= {28'd0, test, mode[2:0]};
                                2'd2:
                                    dat_o <= {31'd0, test};
                                default:
                                    dat_o <= 32'd0;    
                            endcase                        
                    end    
            end

    pk_phy pk_phy0(
//      -- PK External Interface Inputs
        .M4(m4),
        .RQ(rq),
        
//      -- STP Phy Interface
        .STP_CLK(stp_clk),
        .STP_SDI(stp_sdi),
        .STP_LE(stp_le),
        .STP_nOE(stp_noe),
        
//      -- PK TX-FIFO Write Interface
        .FIFO_WRQ(fifo_wrq),
        .FIFO_DATA(fifo_data), // FIFO_DATA is big-endian 32-bit words
        .FIFO_FULL(fifo_full),
        .FIFO_WUDW(fifo_wudw),
        
//		-- MODE TRUTH TABLE: Kilo-Requests per M4 circle (kRQpM4)
//		-- "000" - OFF
//		-- "001" -  2 kRQpM4 -  1 = "0001"
//		-- "010" -  4 kRQpM4 -  3 = "0011"
//		-- "011" -  8 kRQpM4 -  7 = "0111"	
//		-- "100" - 16 kRQpM4 - 15 = "1111"	
//		-- "100" - OFF
//		-- "101" - OFF
//		-- "111" - OFF
        .MODE(mode),
        .TEST(test),
//      -- System Signals
        .nRST(nrst),
        .CLK(clk)
    );
endmodule
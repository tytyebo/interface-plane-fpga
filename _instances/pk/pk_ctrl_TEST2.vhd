-- ## First Code : 23-05-2016               ##
-- ## First Compilation: 21:05 23-05-2016   ##
-- ## Author     : Sergey Romanov           ##
-- ## Company    : NII VS @ SU              ##

-- History ::
-- 21.06.2016 :: Added Off State
-- 23.06.2016 :: Fix bugs:  1) Corrected packet length to 256 of 32-bit words.
--							2) Corrected switch-off operation in ini-state.
--						    3) Added Off-flag signal.

LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_unsigned.all;
USE IEEE.std_logic_arith.all;

LIBRARY WORK;
USE WORK.exinf_se_package.all;

ENTITY pk_ctrl is 
	port(
		-- PK External Interface Edge Signals
		M4_RISE, M4_FALL : IN std_logic;
		RQ_RISE, RQ_FALL : IN std_logic;
		
		-- Control STP Phy Interface
		DATA : OUT std_logic_vector(15 downto 0);
		CMD : OUT std_logic_vector(2 downto 0);
		BUSY      : IN std_logic;
				
		-- PK TX-FIFO Read Interface
		FIFO_RRQ : OUT std_logic;
		FIFO_Q : IN std_logic_vector(31 downto 0);
		FIFO_HALF_FULL : IN std_logic;
		
		-- MODE
		MODE : IN std_logic_vector(1 downto 0);
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';
		CLK : IN std_logic
	);
END pk_ctrl;

ARCHITECTURE SEHDL of pk_ctrl is

-- Command Signal Bus
alias SHIFT_CMD : std_logic IS CMD(0);						-- ������� ���������������� �������� ������
alias LATCH_CMD : std_logic IS CMD(1);						-- ������� ��٨��� � ������ ������
alias OFF_CMD   : std_logic IS CMD(2);						-- ������� ����������

signal PACK_NUM_CODE : std_logic_vector(3 downto 0);

type type_fsm_state is (fsm_state_ini, fsm_state_idle, fsm_state_skip_packet, fsm_state_tx_packet);
signal fsm_state : type_fsm_state;	-- ������ ���������

--type type_fsm_stage is (stage0, stage1, stage2);
type type_fsm_stage is (stage0, stage1, stage2, stage3, stage4);
signal fsm_stage : type_fsm_stage;	-- ������ �����

type type_fsm_tx_stage is (fsm_tx_stage_shift, fsm_tx_stage_latch, fsm_tx_stage_next);
signal fsm_tx_stage : type_fsm_tx_stage;	-- ������ ����� � ��������� �������� ������

SUBTYPE PACK_IN_DWORD_COUNTER_TYPE IS NATURAL RANGE 0 TO PACK_IN_DWORD_SIZE_NUM;				-- ��� ��������� ���������� �������� (0..255)
signal fsm_wcnt : PACK_IN_DWORD_COUNTER_TYPE;

signal fsm_P2_flag : std_logic;
signal fsm_P3_flag : std_logic;

SUBTYPE BYTE_COUNTER_TYPE IS NATURAL RANGE 0 TO 3;				-- ��� ��������� ���������� ��������		
signal fsm_byte_cnt : BYTE_COUNTER_TYPE;

signal P : std_logic_vector(4 downto 1);	-- �������� �������������
signal S : std_logic_vector(2 downto 1);	-- ������� ��������� ��������������

SUBTYPE PACK_IN_BYTE_COUNTER_TYPE IS NATURAL RANGE 0 TO PACK_IN_BYTE_SIZE_NUM;				-- ��� ��������� ���������� �������� (0..1023)
signal puls_rqcnt : PACK_IN_BYTE_COUNTER_TYPE;

signal puls_pcnt : std_logic_vector(3 downto 0);

signal puls_rqcout : std_logic;
signal puls_pcout : std_logic;

signal fsm_data_reg : std_logic_vector(7 downto 0);
signal fsm_shift_ena : std_logic;

signal fsm_off_flag : std_logic;

signal debug_dcnt: std_logic_vector(15 downto 0);

BEGIN

pnum_proc: process(CLK, nRST)
begin
	if nRST = '0' then
		PACK_NUM_CODE <= (others => '0');
	elsif CLK'event and (CLK = '1') then
		PACK_NUM_CODE <= (MODE(1) and MODE(0)) & MODE(1) & (MODE(1) or MODE(0)) & '1';
	end if;
end process;

-- ��������� ����������
fsm_proc: process(CLK, nRST)
variable fsm_data_cs : std_logic_vector(1 downto 0);
begin
	if nRST = '0' then
		
		DATA <= (others => '0');
		CMD <= (others => '0');
		fsm_state <= fsm_state_ini;
		fsm_stage <= stage0;
		fsm_wcnt <= 0;
		fsm_P2_flag <= '0';
		fsm_P3_flag <= '0';
		fsm_byte_cnt <= 0;
		fsm_tx_stage <= fsm_tx_stage_shift;
		fsm_shift_ena <= '0';
		
		fsm_data_reg <= (others => '0');
		
		FIFO_RRQ <= '0';
		
		fsm_off_flag <= '0';
		
		debug_dcnt <= (others => '0');
				
	elsif CLK'event and (CLK = '1') then
		-- ������������ ��������
		CMD <= (others => '0');
		FIFO_RRQ <= '0';
		
		-- ������� ���������
		case fsm_state is
		-- INI
		when fsm_state_ini =>
			if fsm_off_flag = '0'  then
				case fsm_stage is
				when stage0 =>
					if BUSY = '0' then
						SHIFT_CMD <= '1';
						DATA <= (others => '1');
						fsm_stage <= stage1;
					end if;
				when stage1 => 
					if BUSY = '0' then
						LATCH_CMD <= '1';
						--fsm_stage <= stage2;	--debug comment
						fsm_stage <= stage3;	-- debug row
					end if;
				when stage2 => 
					if BUSY = '0' then
						OFF_CMD <= '1';
						fsm_off_flag <= '1';
						fsm_stage <= stage0;
						fsm_state <= fsm_state_idle;
					end if;
					-- debug on
				when stage3 => 
					if BUSY = '0' then
						SHIFT_CMD <= '1';
						DATA <= debug_dcnt;
						debug_dcnt <= debug_dcnt + 1;
						fsm_stage <= stage4;
					end if;
				when stage4 => 
					if BUSY = '0' then
						LATCH_CMD <= '1';
						fsm_stage <= stage3;
					end if;
					--debug off
				end case;
			else
				fsm_state <= fsm_state_idle;
			end if;
		-- IDLE
		when fsm_state_idle =>
			if P(1) = '1' then
				if FIFO_HALF_FULL = '0' then
					fsm_state <= fsm_state_skip_packet;
				else
					fsm_off_flag <= '0';
					fsm_state <= fsm_state_tx_packet;
				end if;
			elsif P(4) = '1' then
				fsm_state <= fsm_state_ini;
			end if;
			fsm_wcnt <= PACK_IN_DWORD_SIZE_NUM;
			fsm_P3_flag <= '1';
			fsm_byte_cnt <= 0;
			fsm_tx_stage <= fsm_tx_stage_shift;
		-- SKIP PACKET
		when fsm_state_skip_packet =>
			if P(2) = '1' then
				fsm_state <= fsm_state_ini;
			end if;
		-- TRANSMIT PACKET
		when fsm_state_tx_packet =>
			case fsm_tx_stage is
			when fsm_tx_stage_shift =>
				if (P(3) or fsm_P3_flag) = '1'  then
					fsm_shift_ena <= '1';
					case fsm_byte_cnt is
						when 0 => fsm_data_reg <= FIFO_Q(31 downto 24);
						when 1 => fsm_data_reg <= FIFO_Q(23 downto 16);
						when 2 => fsm_data_reg <= FIFO_Q(15 downto  8);
						when 3 => fsm_data_reg <= FIFO_Q( 7 downto  0);
					end case;
					if fsm_byte_cnt = 3 then
						fsm_byte_cnt <= 0;
						FIFO_RRQ <= '1';
					else
						fsm_byte_cnt <= fsm_byte_cnt + 1;
					end if;
					fsm_P3_flag <= '0';
					fsm_tx_stage <= fsm_tx_stage_latch;
				end if;
				fsm_P2_flag <= '0';
			when fsm_tx_stage_latch =>
				if (P(2) or fsm_P2_flag) = '1'  then
					if BUSY = '0' then
						LATCH_CMD <= '1';
						if fsm_byte_cnt = 0 then
							fsm_tx_stage <= fsm_tx_stage_next;
						else
							fsm_tx_stage <= fsm_tx_stage_shift;
						end if;
					end if;
					fsm_P2_flag <= '1';
				end if;
				fsm_P3_flag <= '0';
			when fsm_tx_stage_next =>
				if fsm_wcnt /= 0 then
					fsm_wcnt <= fsm_wcnt - 1;
					fsm_tx_stage <= fsm_tx_stage_shift;
				else
					fsm_state <= fsm_state_idle;
				end if;
			end case; -- stage
		end case; -- state
		-- Check-sum
		fsm_data_cs := "00";
		for i in 0 to 3 loop
			fsm_data_cs(0) := fsm_data_cs(0) xor fsm_data_reg(i);
		end loop;
		for i in 4 to 7 loop
			fsm_data_cs(1) := fsm_data_cs(1) xor fsm_data_reg(i);
		end loop;
		-- Data word format
		if fsm_shift_ena = '1' then
			if BUSY = '0' then
				SHIFT_CMD <= '1';
				DATA <= "0000" & "00" & fsm_data_reg & fsm_data_cs;
				fsm_shift_ena <= '0';
			end if;
		end if;
	end if;
end process;

-- ������������� �� ����������
puls_proc: process(CLK, nRST)
begin
	if nRST = '0' then
		P <= (others => '0');	-- �������� �������������
		S <= (others => '0');	-- ��������� ��������������
		puls_rqcnt <= 0;
		puls_pcnt <= (others => '0');
		puls_rqcout <= '0';
		puls_pcout <= '0';
				
	elsif CLK'event and (CLK = '1') then
		-- ������������ ��������
		P <= (others => '0');
		
		if M4_RISE = '1' then
			P(1) <= '1';
			S(1) <= '1';
			S(2) <= '0';
			puls_rqcnt <= 0;
			puls_pcnt <= (others => '0');
		elsif S(1) = '1' THEN
			if (RQ_RISE = '1') and (S(2) = '0') then
				P(2) <= '1';
				S(2) <= '1';
			elsif (RQ_FALL = '1') and (S(2) = '1') then
				S(2) <= '0';
				if puls_rqcout = '0' then
					puls_rqcnt <= puls_rqcnt + 1;
					P(3) <= '1';
				else
					puls_rqcnt <= 0;
					if puls_pcout = '0' then
						P(1) <= '1';
						puls_pcnt <= puls_pcnt + 1;
					else
						S(1) <= '0';
					end if;
				end if;
			end if;
		elsif RQ_RISE = '1' then
			P(4) <= '1';
		end if;

		puls_rqcout <= '0';
		puls_pcout <= '0';
		
		if puls_rqcnt = PACK_IN_BYTE_SIZE_NUM THEN
			puls_rqcout <= '1';
		end if;
		
		if puls_pcnt = PACK_NUM_CODE THEN
			puls_pcout <= '1';
		end if;

	end if;
end process;

END SEHDL;
`timescale 1ps/1ps
module tb_paraller_channel_gen();
/**
  * @brief  Описание регистров.
  */  
    reg nrst;
    reg clk;
    
/**
  * @brief  Выработка сигнала сброса "nrst".
  */     
    initial 
        begin
            nrst <= 0;
            #1000
            nrst <= 1;
        end
/**
  * @brief  Формирование сигнала тактовой частоты "clk".
  */ 
    initial 
        begin
            clk <= 0;
            forever 
                begin
                    #1 clk <= ~clk;
                end
            end
            
    wire rq;
    wire m4;
    reg tick = 0;
    reg fifo_wrq = 0;
    reg [31:0] fifo_data = 0;
    initial 
        begin
            repeat(1024)
                begin
                    if (~tick)
                        fifo_wrq <= 1;                                                   
                    else
                        begin
                            fifo_data <= fifo_data + 1;
                            fifo_wrq <= 1; 
                        end
                    tick <= ~tick;
                end           
        end   
    
    Parallel_Channel_Gen pk_gen 
        (
            .nrst(nrst),
            .clk(clk),              
            .m4(m4),
            .rq(rq)
        );
    pk_phy pk_phy0
        (
    //      -- PK External Interface Inputs
            .M4(m4),
            .RQ(rq),
            
    //      -- STP Phy Interface
            .STP_CLK(),
            .STP_SDI(),
            .STP_LE(),
            .STP_nOE(),
            
    //      -- PK TX-FIFO Write Interface
            .FIFO_WRQ(fifo_wrq),
            .FIFO_DATA(fifo_data),
            .FIFO_FULL(),
            .FIFO_WUDW(),
            
    //      -- MODE: Kilo-Requests per M4 circle (kRQpM4)
    //      -- TRUTH TABLE:
    //      -- "11" - 16 kRQpM4 - 15 = "1111"	
    //      -- "10" -  8 kRQpM4 -  7 = "0111"	
    //      -- "01" -  4 kRQpM4 -  3 = "0011"
    //      -- "00" -  2 kRQpM4 -  1 = "0001"
    //      -- FORMULA: PACK_NUM_CODE <= 2^(MODE + 1) - 1
    //      -- HDL CODE: PACK_NUM_CODE <= (MODE(1) and MODE(0)) & MODE(1) & (MODE(1) or MODE(0)) & '1';
            .MODE(mode),
            
    //      -- System Signals
            .nRST(nrst),
            .CLK(clk)
        );
endmodule

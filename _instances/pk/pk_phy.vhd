-- ## First Code : 20/05/2016               ##
-- ## First Compilation: 21:07 23-05-2016   ##
-- ## Author     : Sergey Romanov           ##
-- ## Company    : NII VS @ SU              ##

-- 24/04/2017 :: Add TEST input
-- 24/04/2017 :: Change MODE coding, comment SI_FILT

LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_arith.all;

ENTITY pk_phy is 
	port(
		-- PK External Interface Inputs
		M4 : IN std_logic;
		RQ : IN std_logic;
		
		-- STP Phy Interface
		STP_CLK : OUT std_logic;
		STP_SDI : OUT std_logic;
		STP_LE  : OUT std_logic;
		STP_nOE : OUT std_logic;
		
		-- PK TX-FIFO Write Interface
		FIFO_WRQ  :  IN std_logic;
		FIFO_DATA :  IN std_logic_vector(31 downto 0);
		FIFO_FULL : OUT std_logic;
		FIFO_WUDW  : OUT std_logic_vector(8 downto 0);
		
		-- MODE TRUTH TABLE: Kilo-Requests per M4 circle (kRQpM4)
		-- "000" - OFF
		-- "001" -  2 kRQpM4 -  1 = "0001"
		-- "010" -  4 kRQpM4 -  3 = "0011"
		-- "011" -  8 kRQpM4 -  7 = "0111"	
		-- "100" - 16 kRQpM4 - 15 = "1111"	
		-- "100" - OFF
		-- "101" - OFF
		-- "111" - OFF
		MODE : IN std_logic_vector(2 downto 0);
		TEST : IN std_logic;
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';
		CLK : IN std_logic
	);
END pk_phy;

ARCHITECTURE SEHDL of pk_phy is

-- Components
component pk_edge is 
	port(
		-- Signal Input
		SI : IN std_logic;
		
		-- PK-edge Outputs
		SI_RISE : OUT std_logic;
		SI_FALL : OUT std_logic;
		-- SI_FILT : OUT std_logic;
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';
		CLK : IN std_logic
	);
end component;

component pk_stp_phy is 
	port(
		-- Control Interface
		DATA : IN std_logic_vector(15 downto 0);
		CMD : IN std_logic_vector(2 downto 0);
		BUSY      : OUT std_logic;
		
		-- STP Phy Interface
		STP_CLK : OUT std_logic;
		STP_SDI : OUT std_logic;
		STP_LE  : OUT std_logic;
		STP_nOE : OUT std_logic;
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';
		CLK : IN std_logic
	);
end component;

component pk_ctrl is 
	port(
		-- PK External Interface Edge Signals
		M4_RISE, M4_FALL : IN std_logic;
		RQ_RISE, RQ_FALL : IN std_logic;
		
		-- Control STP Phy Interface
		DATA : OUT std_logic_vector(15 downto 0);
		CMD : OUT std_logic_vector(2 downto 0);
		BUSY      : IN std_logic;
				
		-- PK TX-FIFO Read Interface
		FIFO_RRQ : OUT std_logic;
		FIFO_Q : IN std_logic_vector(31 downto 0);
		FIFO_HALF_FULL : IN std_logic;
		
		-- MODE
		MODE : IN std_logic_vector(1 downto 0);
		TEST : IN std_logic;
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';
		CLK : IN std_logic
	);
end component;

component pk_tx_fifo	-- ��������� ������ ������ FIFO
	PORT
	(
		aclr		: IN STD_LOGIC ;
		clock		: IN STD_LOGIC ;
		data		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		rdreq		: IN STD_LOGIC ;
		sclr		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;
		full		: OUT STD_LOGIC ;
		q			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		usedw		: OUT STD_LOGIC_VECTOR (8 DOWNTO 0)
	);
end component;

-- SIGNALS

-- PK External Interface Signals
signal m4_rise : std_logic;
signal m4_fall : std_logic;
--signal m4_filt : std_logic;
signal rq_rise : std_logic;
signal rq_fall : std_logic;
--signal rq_filt : std_logic;

signal stp_phy_data : std_logic_vector(15 DOWNTO 0);
signal stp_phy_cmd : std_logic_vector(2 DOWNTO 0);
signal stp_phy_busy : std_logic;
			
-- PK TX-FIFO Signals
signal fifo_rrq : std_logic;
signal fifo_q : std_logic_vector(31 DOWNTO 0);
	
signal fifo_full_s : std_logic;
signal fifo_udw_s : std_logic_vector(8 DOWNTO 0);

signal fifo_half_full : std_logic;

signal fifo_clear : std_logic;

signal nRSTC : std_logic := '1';
signal CMODE : std_logic_vector(1 downto 0);

BEGIN

m4_edge : pk_edge
port map(
	-- Signal Input
	SI => M4,	-- : IN std_logic;
	
	-- PK-edge Outputs
	SI_RISE => m4_rise,	-- : OUT std_logic;
	SI_FALL => m4_fall,	-- : OUT std_logic;
	--SI_FILT => m4_filt,	-- : OUT std_logic;
	
	-- System Signals (��������� �������)
	nRST => nRST,	-- : IN std_logic := '1';
	CLK => CLK		-- : IN std_logic;
);

rq_edge : pk_edge
port map(
	-- Signal Input
	SI => RQ,	-- : IN std_logic;
	
	-- PK-edge Outputs
	SI_RISE => rq_rise,	-- : OUT std_logic;
	SI_FALL => rq_fall,	-- : OUT std_logic;
	--SI_FILT => rq_filt,	-- : OUT std_logic;
	
	-- System Signals (��������� �������)
	nRST => nRST,	-- : IN std_logic := '1';
	CLK => CLK		-- : IN std_logic;
);

stp_phy : pk_stp_phy
port map(
	-- Control Interface
	DATA => stp_phy_data,	-- : IN std_logic_vector(15 downto 0);
	CMD => stp_phy_cmd,	-- : IN std_logic_vector(2 downto 0);
	BUSY => stp_phy_busy,	--      : OUT std_logic;
	
	-- STP Phy Interface
	STP_CLK => STP_CLK,	-- : OUT std_logic;
	STP_SDI => STP_SDI,	-- : OUT std_logic;
	STP_LE => STP_LE,	--  : OUT std_logic;
	STP_nOE => STP_nOE,	-- : OUT std_logic;
	
	-- System Signals (��������� �������)
	nRST => nRST,	-- : IN std_logic := '1';
	CLK => CLK	-- : IN std_logic;
);

-- CMODE: Kilo-Requests per M4 circle (kRQpM4)
-- TRUTH TABLE:
-- "11" - 16 kRQpM4 - 15 = "1111"	
-- "10" -  8 kRQpM4 -  7 = "0111"	
-- "01" -  4 kRQpM4 -  3 = "0011"
-- "00" -  2 kRQpM4 -  1 = "0001"
-- FORMULA: PACK_NUM_CODE <= 2^(CMODE + 1) - 1
-- HDL CODE: PACK_NUM_CODE <= (CMODE(1) and CMODE(0)) & CMODE(1) & (CMODE(1) or CMODE(0)) & '1';
mode_mux: process(MODE, nRST)
begin
	nRSTC <= nRST;
	CMODE <= "00";
	case MODE is
		when "001" => CMODE <= "00";
		when "010" => CMODE <= "01";
		when "011" => CMODE <= "10";
		when "100" => CMODE <= "11";
		when others => nRSTC <= '0';
	end case;
end process;

ctrl : pk_ctrl
port map(
	-- PK External Interface Edge Signals
	M4_RISE => m4_rise,	--
	M4_FALL => m4_fall,	-- : IN std_logic;
	RQ_RISE => rq_rise,	--
	RQ_FALL => rq_fall,	-- : IN std_logic;
	
	-- Control STP Phy Interface
	DATA 	=> stp_phy_data,	-- : OUT : std_logic_vector(15 downto 0);
	CMD 	=> stp_phy_cmd,	-- : OUT : std_logic_vector(2 downto 0);
	BUSY 	=> stp_phy_busy,	--      : IN std_logic;
			
	-- PK TX-FIFO Read Interface
	FIFO_RRQ	=> fifo_rrq,	-- : OUT std_logic;
	FIFO_Q 		=> fifo_q,	-- : IN std_logic_vector(31 downto 0);
	FIFO_HALF_FULL 	=> fifo_half_full,	-- : IN std_logic_vector(9 downto 0);
	
	-- MODE
	MODE => CMODE,	-- : IN std_logic_vector(1 downto 0);
	TEST => TEST,
	
	-- System Signals (��������� �������)
	nRST => nRSTC,	-- : IN std_logic := '1';
	CLK => CLK	-- : IN std_logic;
);

fifo_buf : pk_tx_fifo	-- ��������� ������ ������ FIFO
port map
(
	wrreq		 => FIFO_WRQ,	--: IN STD_LOGIC ;
	data		 => FIFO_DATA,	--: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
	
	rdreq		 => fifo_rrq,	--: IN STD_LOGIC ;
	q			 => fifo_q,	--: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
	
	full		 => fifo_full_s,	--: OUT STD_LOGIC ;
	usedw		 => fifo_udw_s,	--: OUT STD_LOGIC_VECTOR (8 DOWNTO 0)

	aclr		 => fifo_clear,	--: IN STD_LOGIC;
	sclr		 => fifo_clear,	--: IN STD_LOGIC;	
	clock		 => CLK	--: IN STD_LOGIC;
);

fifo_clear <= not nRST;

fifo_half_full <= fifo_full_s OR fifo_udw_s(8);

FIFO_FULL	<= fifo_full_s;
FIFO_WUDW	<= fifo_udw_s;

END SEHDL;
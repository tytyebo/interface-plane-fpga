-- ## First Code : 19-05-2016               ##
-- ## First Compilation: 20:50 23-05-2016   ##
-- ## First Simulation: 11:45 24-05-2016    ##
-- ## Author     : Sergey Romanov           ##
-- ## Company    : NII VS @ SU              ##

-- History ::
-- 21.06.2016 :: Change Busy Logic

LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
USE IEEE.std_logic_arith.all;
USE IEEE.std_logic_unsigned.all;
USE IEEE.math_real.all;

LIBRARY WORK;
USE WORK.exinf_se_package.all;

ENTITY pk_stp_phy is 
	port(
		-- Control Interface
		DATA : IN std_logic_vector(15 downto 0);
		CMD : IN std_logic_vector(2 downto 0);
		BUSY      : OUT std_logic;
		
		-- STP Phy Interface
		STP_CLK : OUT std_logic;
		STP_SDI : OUT std_logic;
		STP_LE  : OUT std_logic;
		STP_nOE : OUT std_logic;
		
		-- System Signals (��������� �������)
		nRST : IN std_logic := '1';
		CLK : IN std_logic
	);
END pk_stp_phy;

ARCHITECTURE SEHDL of pk_stp_phy is

CONSTANT KDIV : NATURAL := NATURAL(FSYS / 37.5);		-- ����������� ������� �������

-- Command Signal Bus
alias SHIFT_CMD : std_logic IS CMD(0);						-- ������� ���������������� �������� ������
alias LATCH_CMD : std_logic IS CMD(1);						-- ������� ��٨��� � ������ ������
alias OFF_CMD   : std_logic IS CMD(2);						-- ������� ����������

signal STP_OE : std_logic;

type type_fsm_state is (fsm_state_idle, fsm_state_shift, fsm_state_shift_end, fsm_state_latch, fsm_state_off);
signal fsm_state : type_fsm_state;	-- ������ ���������

signal fsm_diag_ena : boolean;
signal fsm_shift_reg : std_logic_vector(15 downto 0);
signal fsm_cnt : std_logic_vector(3 downto 0);

signal diag_toggle : boolean;
signal diag_cout : boolean;
signal diag_dena : boolean;
signal diag_cena : boolean;

SUBTYPE DIAG_COUNTER_TYPE IS NATURAL RANGE 0 TO KDIV;				-- ��� ��������� ���������� ��������		
signal diag_cnt : DIAG_COUNTER_TYPE;

BEGIN

fsm_proc: process(CLK, nRST)
begin
	if nRST = '0' then

		STP_CLK <= '0';
		STP_SDI <= '0';
		STP_LE <= '0';
		STP_OE <= '0';
		
		fsm_state <= fsm_state_idle;
		fsm_diag_ena <= false;
		fsm_shift_reg <= (others => '0');
		fsm_cnt <= (others => '0');
		
	elsif CLK'event and (CLK = '1') then
	
		case fsm_state is
			-- IDLE
			when fsm_state_idle =>
				fsm_diag_ena <= false;
				if SHIFT_CMD = '1' then
					fsm_shift_reg <= DATA;
					fsm_diag_ena <= true;
					fsm_state <= fsm_state_shift;
				elsif LATCH_CMD = '1' then
					fsm_diag_ena <= true;
					fsm_state <= fsm_state_latch;
				elsif OFF_CMD = '1' then
					fsm_state <= fsm_state_off;
				end if;
				fsm_cnt <= (others => '0');
			-- SHIFT
			when fsm_state_shift =>
				if diag_dena then
					STP_SDI <= fsm_shift_reg(15);
					STP_CLK <= '0';
					fsm_shift_reg(15 downto 1) <= fsm_shift_reg(14 downto 0);
					fsm_cnt <= fsm_cnt - 1;
				end if;
				if diag_cena then
					STP_CLK <= '1';
					if fsm_cnt = 0 then
						fsm_state <= fsm_state_shift_end;
					end if;
				end if;
			-- SHIFT END
			when fsm_state_shift_end =>
				if diag_dena then
					STP_CLK <= '0';
					fsm_state <= fsm_state_idle;
				end if;
			-- LATCH
			when fsm_state_latch =>
				if diag_dena then
					STP_LE <= '1';
				end if;
				if diag_cena then
					STP_LE <= '0';
					STP_OE <= '1';
					fsm_state <= fsm_state_idle;
				end if;
			-- SWITCH OFF
			when fsm_state_off =>
				STP_OE <= '0';
				fsm_state <= fsm_state_idle;
		end case;
	end if;
end process;

diag_proc: process(CLK, nRST)
begin
	if nRST = '0' then

		diag_toggle <= false;
		diag_cout <= false;
		diag_dena <= false;
		diag_cena <= false;
		
		diag_cnt <= 0;
		
	elsif CLK'event and (CLK = '1') then
		
		diag_dena <= false;
		diag_cena <= false;

		if not fsm_diag_ena then
			diag_toggle <= false;
			diag_cnt <= KDIV;
			diag_cout <= true;
		else
			if diag_cnt /= 0 then
				diag_cnt <= diag_cnt - 1;
				diag_cout <= false;
			else
				diag_cnt <= KDIV;
				diag_cout <= true;
			end if;
			if diag_cout then
				diag_cena <= diag_toggle;
				diag_dena <= not diag_toggle;
				diag_toggle <= not diag_toggle;
			end if;
		end if;
	end if;
end process;

busy_proc: process(fsm_state, CMD)
begin
	if (fsm_state = fsm_state_idle) and (CMD = 0) then
		BUSY <= '0';
	else
		BUSY <= '1';
	end if;
end process;

STP_nOE <= not STP_OE;

END SEHDL;
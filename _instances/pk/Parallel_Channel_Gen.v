module Parallel_Channel_Gen
    (
        nrst,
        clk,       
        rq,
        m4
    );
    input nrst, clk;
    output reg rq, m4;
    
    //parameter FREQ = 128;
    reg [24:0] m4_cnt;
    reg [11:0] rq_cnt;
    
    
    always @(posedge clk or negedge nrst)
        begin
            if (~nrst)
                begin
                    rq_cnt <= 12'h000;
                    m4_cnt <= 25'h0000000;
                end
            else
                begin
                    rq_cnt <= rq_cnt + 1'b1;
                    m4_cnt <= m4_cnt + 1'b1;
                        
                    if (m4_cnt == 0) 
                        begin
                            m4 <= 1'b1;
                        end
                    if (m4_cnt == (973))
                        begin
                            m4 <= 1'b0;                           
                        end
                    if (m4_cnt == (32000000))
                        begin
                            m4_cnt <= 25'h0000000;                          
                        end    

                    if (m4 == 0)
                        begin
                            if (rq_cnt == 0)
                                begin
                                    rq <= 1'b1;  
                                end
                            if (rq_cnt == 973)
                                begin
                                    rq <= 1'b0;                                    
                                end  
                             if (rq_cnt == 1953)
                                begin
                                    rq_cnt <= 12'h000;
                                end
                        end
                    else
                        begin
                            rq_cnt <= 12'h000;
                            rq <= 0;
                        end
            end
        end
endmodule
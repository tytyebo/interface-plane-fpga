`include "interfaces.svh"
import defines_pkg::framLogMsg_s;
module top
    (
        input plm_clk,        
        //CF card interface (TrueIDE/UDMA)
        inout [15:0] cf_d,
        output [1:0] cf_csn,
        output [2: 0] cf_a,
        input [1:0] cf_cd,
        input cf_vs1,
        input cf_iois16,
        input cf_intrq,
        input cf_iordy, /*cf_pdiag, cf_dasp,*/ 
        input cf_dmarq,
        output cf_dmack,
        output cf_reset,
        output cf_iord,
        output cf_iowr,		
        //oribta serial interface
        input orbita,	
        //Parallel interface
        output stp_pk1_sdi,
        output stp_pk1_clk,
        output stp_pk1_le,
        output stp_pk_oe,	
        input pk1_m4,
        //pk1_si,
        input pk1_zap,
        output stp_pk2_sdi,
        output stp_pk2_clk,
        output stp_pk2_le,		
        input pk2_m4,
        //pk2_si,
        input pk2_zap,	
        //MKO
        output m_disrxa,
        input m_rxap,
        input m_rxan,       
        output m_disrxb,
        input m_rxbp,
        input m_rxbn,        
        output m_distxa,	
        //m_txap,
        //m_txan,       
        output m_distxb,
        output m_txbp,
        output m_txbn,	
        //FTDI USB
        inout [7:0] ftdi_adbus,
        input ftdi_rxf_n,
        input ftdi_txe_n,
        output ftdi_rd_n,
        output ftdi_wr_n,
        output ftdi_siwu_n,
        input ftdi_clkout,
        output ftdi_oe_n,
        // input ftdi_vcc_out,
        // CPU interface
        input fpga_clk,
        input fpga_mosi, 
        output fpga_miso, 
        input fpga_ncs, 
        output /*[1:0]*/ fpga_irq,
        // output cf_vcc_en,
        // Record 
        input fpga_rec,
        // FRAM
        output FRAM_CS, FRAM_SI, FRAM_SCK,
        input FRAM_SO
    );
    // assign cf_vcc_en = cf_vcc_en_reg;
    //
    //Internal signals
    //
    wire clk;	//main internal clock - 128Mhz
    wire rst_pon;
    wire nrst_pon;
    wire nrst;
    wire rst;
    //
    wire stp_pk1_oe;
    wire stp_pk2_oe;
    //ATA controller additional signals
    wire [15:0]dd_pad_i;
    wire [15:0]dd_pad_o;
    wire dd_pad_oe;
    wire cf_diord;
    wire cf_diowr;
    //
    // ------- Wishbone bus matrix connections ---------
    //
    //ATA controller slave port
    wire [15:0] ata_wb_addr;
    wire [31:0] ata_wb_data;
    wire [31:0] ata_wb_data_out;
    wire [3:0] ata_wb_sel;
    wire ata_wb_cyc;
    wire ata_wb_stb;
    wire ata_wb_ack;
    wire ata_wb_we;
    //USB master port
    wire [15:0] usb_wbm_adr_o;
    wire [31:0] usb_wbm_dat_o;
    wire usb_wbm_cyc_o;
    wire usb_wbm_stb_o;
    wire usb_wbm_we_o;
    wire [31:0] usb_wbm_dat_i;
    wire usb_wbm_ack_i;
    //Orbita Controller Wishbone master port
    wire [15:0] orb_wbm_adr;
    wire [31:0] orb_wbm_dat_o;
    wire orb_wbm_cyc;
    wire orb_wbm_stb;
    wire orb_wbm_we;
    wire [3:0] orb_wbm_sel;
    wire [31:0] orb_wbm_dat_i;
    wire orb_wbm_ack;
    //MKO controller Wishbone slave port
    wire [15:0] mko_wbs_adr;
    wire [31:0] mko_wbs_dat_o;
    wire mko_wbs_cyc;
    wire mko_wbs_stb;
    wire mko_wbs_we;
    wire [3:0] mko_wbs_sel;
    wire [31:0] mko_wbs_dat_i;
    wire mko_wbs_ack;
    //Parallel Channel Сontroller Wishbone slave port
    wire [15:0] pk1_wbs_adr;
    wire [31:0] pk1_wbs_dat_o;
    wire pk1_wbs_cyc;
    wire pk1_wbs_stb;
    wire pk1_wbs_we;
    wire [3:0] pk1_wbs_sel;
    wire [31:0] pk1_wbs_dat_i;
    wire pk1_wbs_ack;
    
    wire [15:0] pk2_wbs_adr;
    wire [31:0] pk2_wbs_dat_o;
    wire pk2_wbs_cyc;
    wire pk2_wbs_stb;
    wire pk2_wbs_we;
    wire [3:0] pk2_wbs_sel;
    wire [31:0] pk2_wbs_dat_i;
    wire pk2_wbs_ack;
    //Orbita Channel Сontroller Wishbone slave port
    wire [15:0] orb_wbs_adr;
    wire [31:0] orb_wbs_dat_i;
    wire orb_wbs_cyc;
    wire orb_wbs_stb;
    wire orb_wbs_we ;
    wire [3:0] orb_wbs_sel;
    wire [31:0] orb_wbs_dat_o;
    wire orb_wbs_ack;
    //Registers Channel Сontroller Wishbone slave port
    wire [15:0] regs_wbs_adr;
    wire [31:0] regs_wbs_dat_i;
    wire regs_wbs_cyc;
    wire regs_wbs_stb;
    wire regs_wbs_we ;
    wire [3:0] regs_wbs_sel;
    wire [31:0] regs_wbs_dat_o;
    wire regs_wbs_ack;
    // Eraser Сontroller Wishbone slave port
    wire [15:0] er_wbs_adr;
    wire [31:0] er_wbs_dat_i;
    wire er_wbs_cyc;
    wire er_wbs_stb;
    wire er_wbs_we ;
    wire [3:0] er_wbs_sel;
    wire [31:0] er_wbs_dat_o;
    wire er_wbs_ack;
    // Eraser Сontroller Wishbone master port
    wire [15:0] er_wbm_adr;
    wire [31:0] er_wbm_dat_i;
    wire er_wbm_cyc;
    wire er_wbm_stb;
    wire er_wbm_we ;
    wire [3:0] er_wbm_sel;
    wire [31:0] er_wbm_dat_o;
    wire er_wbm_ack;
    
    // FRAM Сontroller Wishbone slave port
    wire [15:0] fram_wbs_adr;
    wire [31:0] fram_wbs_dat_i;
    wire fram_wbs_cyc;
    wire fram_wbs_stb;
    wire fram_wbs_we ;
    wire [3:0] fram_wbs_sel;
    wire [31:0] fram_wbs_dat_o;
    wire fram_wbs_ack;
    assign stp_pk_oe = stp_pk1_oe & stp_pk2_oe;
    
    wire pll_locked;
    
    pll pll0
        (
            .inclk0(plm_clk),
            .c0(clk),
            .locked(pll_locked)
        );

    PONR PONR0
        (
            .CLOCK_IN(clk), 
            .PLL_LOCK_IN(pll_locked),
            .nRESET_OUT(nrst_pon), 
            .RESET_OUT(rst_pon)
        ); 

    atahost_top 
        #(
            .ARST_LVL(1'b0),
            .TWIDTH(8),
            .PIO_mode0_T1(8'd7),
            .PIO_mode0_T2(8'd36),
            .PIO_mode0_T4(8'd2),
            .PIO_mode0_Teoc(8'd29)
        ) 
    atahost0 
        (               
            .wb_clk_i(clk),
            .arst_i(nrst),
            .wb_rst_i(rst),
            .wb_cyc_i(ata_wb_cyc), 
            .wb_stb_i(ata_wb_stb),  
            .wb_ack_o(ata_wb_ack), 
            .wb_err_o(),
            .wb_adr_i(ata_wb_addr), 
            .wb_dat_i(ata_wb_data), 
            .wb_dat_o(ata_wb_data_out), 
            .wb_sel_i(ata_wb_sel),
            .wb_we_i(ata_wb_we),
            .wb_inta_o(ata_wb_inta),
            
            .DMA_req(), // out
            .DMA_Ack(1'b0), // in
            
            //ATA interface signals
            .resetn_pad_o(cf_reset),
            .dd_pad_i(dd_pad_i),	
            .dd_pad_o(dd_pad_o),
            .dd_padoe_o(dd_pad_oe),
            .da_pad_o(cf_a),
            .cs0n_pad_o(cf_csn[0]),
            .cs1n_pad_o(cf_csn[1]),
            .diorn_pad_o(cf_diord),
            .diown_pad_o(cf_diowr),
            .iordy_pad_i(cf_iordy),
            .intrq_pad_i(cf_intrq),
            .dmarq_pad_i(cf_dmarq),
            .dmackn_pad_o(cf_dmack)            
        );

    assign cf_d = dd_pad_oe ? dd_pad_o : 16'hzzzz;
    assign dd_pad_i = cf_d;

    assign cf_iord = cf_diord;
    assign cf_iowr = cf_diowr;

    wishbone_io cfhl_wbm();
    wishbone_io cfhl_wbs();
    logic fram_log_full, fram_log_rdy;
    logic [27:0] fram_cf_adr;
    cfhl_ctrl cfhl_ctrl0(
            .clk(clk),
            .nrst(nrst),
            .wbm(cfhl_wbm), 
            .wbs(cfhl_wbs),	
            .card_detect(cf_cd),
            .start_cf_adr(fram_cf_adr),
            .log_full(fram_log_full),
            .ena(fram_log_rdy));
    usb usb0
        (
            .nrst(nrst),
            .clk(clk),
            
            //FTDI USB
            .ftdi_adbus(ftdi_adbus),
            .ftdi_rxf_n(ftdi_rxf_n),
            .ftdi_txe_n(ftdi_txe_n),
            .ftdi_rd_n (ftdi_rd_n),
            .ftdi_wr_n (ftdi_wr_n),
            .ftdi_siwu_n(ftdi_siwu_n),
            .ftdi_clkout(ftdi_clkout),	//FTDI workds on rising edge of it's clock. Both for data output and input
            .ftdi_oe_n (ftdi_oe_n),
            
            .wbm_adr_o(usb_wbm_adr_o),
            .wbm_dat_o(usb_wbm_dat_o),
            .wbm_cyc_o(usb_wbm_cyc_o),
            .wbm_stb_o(usb_wbm_stb_o),
            .wbm_we_o (usb_wbm_we_o),
            .wbm_dat_i(usb_wbm_dat_i),
            .wbm_ack_i(usb_wbm_ack_i)
        );
    Parallel_Channel_Controller PK0
        (
            .nrst(nrst),
            .clk(clk),              
            .m4(pk1_m4),
            .rq(pk1_zap),        
            .stp_clk(stp_pk1_clk),
            .stp_sdi(stp_pk1_sdi),
            .stp_le(stp_pk1_le),
            .stp_noe(stp_pk1_oe),
            .wb_adr_i(pk1_wbs_adr),
            .wb_dat_i(pk1_wbs_dat_i),	
            .wb_stb_i(pk1_wbs_stb),
            .wb_cyc_i(pk1_wbs_cyc),
            .wb_sel_i(pk1_wbs_sel),
            .wb_we_i(pk1_wbs_we),	
            .wb_dat_o(pk1_wbs_dat_o),
            .wb_ack_o(pk1_wbs_ack)
        );
    Parallel_Channel_Controller PK1
        (
            .nrst(nrst),
            .clk(clk),              
            .m4(pk2_m4),
            .rq(pk2_zap),        
            .stp_clk(stp_pk2_clk),
            .stp_sdi(stp_pk2_sdi),
            .stp_le(stp_pk2_le),
            .stp_noe(stp_pk2_oe),
            .wb_adr_i(pk2_wbs_adr),
            .wb_dat_i(pk2_wbs_dat_i),	
            .wb_stb_i(pk2_wbs_stb),
            .wb_cyc_i(pk2_wbs_cyc),
            .wb_sel_i(pk2_wbs_sel),
            .wb_we_i(pk2_wbs_we),	
            .wb_dat_o(pk2_wbs_dat_o),
            .wb_ack_o(pk2_wbs_ack)
        );
    //MKO_Controller MK3
    MKO_Controller MKO3
        (
            .nrst(nrst),
            .clk(clk),
            
            //.txap(),
            //.txan(),
            .txinha(m_distxa),
            .rxena(m_disrxa),
            .rxap(m_rxap),
            .rxan(m_rxan),
            
            .txbp(m_txbp),
            .txbn(m_txbn),
            .txinhb(m_distxb),
            .rxenb(m_disrxb),
            .rxbp(m_rxbp),
            .rxbn(m_rxbn),
            
            //.rxen(),
            //.rxp(),
            //.rxn(),

            .wb_adr_i(mko_wbs_adr),
            .wb_dat_i(mko_wbs_dat_i),	
            .wb_stb_i(mko_wbs_stb),
            .wb_cyc_i(mko_wbs_cyc),
            .wb_sel_i(mko_wbs_sel),
            .wb_we_i(mko_wbs_we),	
            .wb_dat_o(mko_wbs_dat_o),
            .wb_ack_o(mko_wbs_ack)
        );
    logic [7:0] fram_rec_num;
    Orbita_Channel_Controller Orbita
        (
            .nrst(nrst),
            .clk(clk),
            
            .clk_ext(st3_clk),
            .video(orbita),       
            
            .wbm_adr_o(orb_wbm_adr),
            .wbm_dat_o(orb_wbm_dat_o),	
            .wbm_stb_o(orb_wbm_stb),
            .wbm_cyc_o(orb_wbm_cyc),
            .wbm_sel_o(orb_wbm_sel),
            .wbm_we_o(orb_wbm_we),	
            .wbm_dat_i(orb_wbm_dat_i),
            .wbm_ack_i(orb_wbm_ack),
            
            .wbs_adr_i(orb_wbs_adr),
            .wbs_dat_i(orb_wbs_dat_i),	
            .wbs_stb_i(orb_wbs_stb),
            .wbs_cyc_i(orb_wbs_cyc),
            .wbs_sel_i(orb_wbs_sel),
            .wbs_we_i(orb_wbs_we),	
            .wbs_dat_o(orb_wbs_dat_o),
            .wbs_ack_o(orb_wbs_ack),
            .test_rst(),
            .record_cmd(~rec_cmd));
    logic cf_ctrl_ready, cf_data_ready, sett_upload, rec_cmd_ena, rec_state;
    wishbone_io seq_wbm();
    wishbone_io seq_wbs();
    sequencer sequencer_interface(.nrst(nrst),
                                  .clk(clk),        
                                  .spi_clk (fpga_clk),
                                  .spi_miso(fpga_miso),
                                  .spi_mosi(fpga_mosi),
                                  .spi_ncs (fpga_ncs),       
                                  .wbm(seq_wbm),
                                  .wbs(seq_wbs),
                                  .irq1(fpga_irq),
                                  .irq2(),
                                  .rec_cmd(~rec_cmd),
                                  .rec_num(fram_rec_num),
                                  .sett_upload(sett_upload),
                                  .rec_cmd_ena(rec_cmd_ena),
                                  .rec_state(rec_state));
    wishbone_io service_wbs();
    ird_ip_service_ctrl serviceCtrl(.nrst_pon(nrst_pon),
                                    .clk(clk),            
                                    .wbs(service_wbs),
                                    .nrst(nrst),
                                    .rst(rst),
                                    .rec_cmd(~rec_cmd), 
                                    .rec_cmd_ena(rec_cmd_ena), 
                                    .rec_state(rec_state));   
    uzi_ip_cf_eraser eraser
        (
            .clk(clk),
            .nrst(nrst),
            // Wishbone master side
            .wbm_adr_o(er_wbm_adr),
            .wbm_dat_o(er_wbm_dat_o),
            .wbm_sel_o(er_wbm_sel),
            .wbm_cyc_o(er_wbm_cyc),
            .wbm_stb_o(er_wbm_stb),
            .wbm_we_o(er_wbm_we),
            .wbm_dat_i(er_wbm_dat_i),
            .wbm_ack_i(er_wbm_ack),         
            // Wishbone slave side
            .wbs_adr_i(er_wbs_adr),
            .wbs_dat_i(er_wbs_dat_i),
            .wbs_sel_i(er_wbs_sel),
            .wbs_cyc_i(er_wbs_cyc),
            .wbs_stb_i(er_wbs_stb), 
            .wbs_we_i(er_wbs_we),
            .wbs_dat_o(er_wbs_dat_o),
            .wbs_ack_o(er_wbs_ack)
        );
    wishbone_io fram_wbs();
    wb_conmax_top
        #(
            .dw(32), .aw(16), .rf_addr(4'hf),
            .pri_sel0(2'd0), .pri_sel1(2'd2), .pri_sel2(2'd1), .pri_sel3(2'd2), .pri_sel4(2'd2), .pri_sel5(2'd2), .pri_sel6(2'd2), .pri_sel7(2'd2),
            .pri_sel8(2'd2), .pri_sel9(2'd2), .pri_sel10(2'd2), .pri_sel11(2'd2), .pri_sel12(2'd2), .pri_sel13(2'd2), .pri_sel14(2'd2), .pri_sel15(2'd2)
        ) 
    wb_conmax_top0
        (
            .clk_i(clk), .rst_i(rst),
            // Master 0 Interface - USB
            .m0_data_i(usb_wbm_dat_o), .m0_data_o(usb_wbm_dat_i), .m0_addr_i(usb_wbm_adr_o), 
            .m0_sel_i(4'b1111), .m0_we_i(usb_wbm_we_o), .m0_cyc_i(usb_wbm_cyc_o),
            .m0_stb_i(usb_wbm_stb_o), .m0_ack_o(usb_wbm_ack_i), .m0_err_o(), .m0_rty_o(),
            // Master 1 Interface - high-level CF controller master port
            .m1_data_i(cfhl_wbm.wbm_dat), .m1_data_o(cfhl_wbm.wbs_dat), .m1_addr_i(cfhl_wbm.wbm_adr), 
            .m1_sel_i(cfhl_wbm.wbm_sel), 	.m1_we_i(cfhl_wbm.wbm_we), .m1_cyc_i(cfhl_wbm.wbm_cyc),
            .m1_stb_i(cfhl_wbm.wbm_stb), .m1_ack_o(cfhl_wbm.wbs_ack), .m1_err_o(), .m1_rty_o(),
            // Master 2 Interface - Orbita controller master port
            .m2_data_i(orb_wbm_dat_o), .m2_data_o(orb_wbm_dat_i), .m2_addr_i(orb_wbm_adr), 
            .m2_sel_i(orb_wbm_sel), .m2_we_i(orb_wbm_we), .m2_cyc_i(orb_wbm_cyc),
            .m2_stb_i(orb_wbm_stb), .m2_ack_o(orb_wbm_ack), .m2_err_o(), .m2_rty_o(),
            // Master 3 Interface
            .m3_data_i(seq_wbm.wbm_dat), .m3_data_o(seq_wbm.wbs_dat), .m3_addr_i(seq_wbm.wbm_adr),
            .m3_sel_i(seq_wbm.wbm_sel), .m3_we_i(seq_wbm.wbm_we), .m3_cyc_i(seq_wbm.wbm_cyc),
            .m3_stb_i(seq_wbm.wbm_stb), .m3_ack_o(seq_wbm.wbs_ack), .m3_err_o(), .m3_rty_o(),
            // Master 4 Interface
            .m4_data_i(er_wbm_dat_o), .m4_data_o(er_wbm_dat_i), .m4_addr_i(er_wbm_adr),
            .m4_sel_i(er_wbm_sel), .m4_we_i(er_wbm_we), .m4_cyc_i(er_wbm_cyc),
            .m4_stb_i(er_wbm_stb), .m4_ack_o(er_wbm_ack), .m4_err_o(), .m4_rty_o(),
            // Master 5 Interface
            .m5_data_i(0), .m5_data_o(), .m5_addr_i(0), .m5_sel_i(0), .m5_we_i(0), .m5_cyc_i(0),
            .m5_stb_i(0), .m5_ack_o(), .m5_err_o(), .m5_rty_o(),
            // Master 6 Interface
            .m6_data_i(0), .m6_data_o(), .m6_addr_i(0), .m6_sel_i(0), .m6_we_i(0), .m6_cyc_i(0),
            .m6_stb_i(0), .m6_ack_o(), .m6_err_o(), .m6_rty_o(),
            // Master 7 Interface
            .m7_data_i(0), .m7_data_o(), .m7_addr_i(0), .m7_sel_i(0), .m7_we_i(0), .m7_cyc_i(0),
            .m7_stb_i(0), .m7_ack_o(), .m7_err_o(), .m7_rty_o(),
            // Slave 0 Interface - low level ATA host controller
            .s0_data_i(ata_wb_data_out), .s0_data_o(ata_wb_data), .s0_addr_o(ata_wb_addr), 
            .s0_sel_o(ata_wb_sel), .s0_we_o(ata_wb_we), .s0_cyc_o(ata_wb_cyc),
            .s0_stb_o(ata_wb_stb), .s0_ack_i(ata_wb_ack), .s0_err_i(0), .s0_rty_i(0),	
            // Slave 1 Interface
            .s1_data_i(orb_wbs_dat_o), .s1_data_o(orb_wbs_dat_i), .s1_addr_o(orb_wbs_adr), 
            .s1_sel_o(orb_wbs_sel), .s1_we_o(orb_wbs_we), .s1_cyc_o(orb_wbs_cyc),
            .s1_stb_o(orb_wbs_stb), .s1_ack_i(orb_wbs_ack), .s1_err_i(0), .s1_rty_i(0),
            // Slave 2 Interface - high-level CF controller slave port
            .s2_data_i(cfhl_wbs.wbs_dat), .s2_data_o(cfhl_wbs.wbm_dat), .s2_addr_o(cfhl_wbs.wbm_adr), 
            .s2_sel_o(cfhl_wbs.wbm_sel), .s2_we_o(cfhl_wbs.wbm_we), .s2_cyc_o(cfhl_wbs.wbm_cyc),
            .s2_stb_o (cfhl_wbs.wbm_stb), .s2_ack_i (cfhl_wbs.wbs_ack), .s2_err_i (0), .s2_rty_i(0),
            // Slave 3 Interface - MKO slave port
            .s3_data_i(mko_wbs_dat_o), .s3_data_o(mko_wbs_dat_i), .s3_addr_o(mko_wbs_adr), 
            .s3_sel_o(mko_wbs_sel), .s3_we_o(mko_wbs_we), .s3_cyc_o(mko_wbs_cyc),
            .s3_stb_o(mko_wbs_stb), .s3_ack_i(mko_wbs_ack), .s3_err_i(0), .s3_rty_i(0),
            // Slave 4 Interface
            .s4_data_i(service_wbs.wbs_dat), .s4_data_o(service_wbs.wbm_dat), .s4_addr_o(service_wbs.wbm_adr), 
            .s4_sel_o(service_wbs.wbm_sel), .s4_we_o(service_wbs.wbm_we), .s4_cyc_o(service_wbs.wbm_cyc),
            .s4_stb_o(service_wbs.wbm_stb), .s4_ack_i(service_wbs.wbs_ack), .s4_err_i(0), .s4_rty_i(0),
            // Slave 5 Interface
            .s5_data_i(er_wbs_dat_o), .s5_data_o(er_wbs_dat_i), .s5_addr_o(er_wbs_adr), 
            .s5_sel_o(er_wbs_sel), .s5_we_o(er_wbs_we), .s5_cyc_o(er_wbs_cyc),
            .s5_stb_o (er_wbs_stb), .s5_ack_i (er_wbs_ack), .s5_err_i (0), .s5_rty_i(0),
            // Slave 6 Interface - Parallel Channel Controller slave port
            .s6_data_i(pk1_wbs_dat_o), .s6_data_o(pk1_wbs_dat_i), .s6_addr_o(pk1_wbs_adr),
				.s6_sel_o(pk1_wbs_sel), .s6_we_o(pk1_wbs_we), .s6_cyc_o(pk1_wbs_cyc),
            .s6_stb_o(pk1_wbs_stb), .s6_ack_i(pk1_wbs_ack), .s6_err_i(0), .s6_rty_i(0),
            // Slave 7 Interface
            .s7_data_i(pk2_wbs_dat_o), .s7_data_o(pk2_wbs_dat_i), .s7_addr_o(pk2_wbs_adr),
				.s7_sel_o(pk2_wbs_sel), .s7_we_o(pk2_wbs_we), .s7_cyc_o(pk2_wbs_cyc),
            .s7_stb_o(pk2_wbs_stb), .s7_ack_i(pk2_wbs_ack), .s7_err_i(0), .s7_rty_i(0),
            // Slave 8 Interface
            .s8_data_i(seq_wbs.wbs_dat), .s8_data_o(seq_wbs.wbm_dat), .s8_addr_o(seq_wbs.wbm_adr),
            .s8_sel_o(seq_wbs.wbm_sel), .s8_we_o(seq_wbs.wbm_we), .s8_cyc_o(seq_wbs.wbm_cyc),
            .s8_stb_o(seq_wbs.wbm_stb), .s8_ack_i(seq_wbs.wbs_ack), .s8_err_i(0), .s8_rty_i(0),
            // Slave 9 Interface
            .s9_data_i(fram_wbs.wbs_dat), .s9_data_o(fram_wbs.wbm_dat), .s9_addr_o(fram_wbs.wbm_adr),
            .s9_sel_o(fram_wbs.wbm_sel), .s9_we_o(fram_wbs.wbm_we), .s9_cyc_o(fram_wbs.wbm_cyc),
            .s9_stb_o(fram_wbs.wbm_stb), .s9_ack_i(fram_wbs.wbs_ack), .s9_err_i(0), .s9_rty_i(0),
            // Slave 10 Interface
            .s10_data_i(0), .s10_data_o(), .s10_addr_o(), .s10_sel_o(), .s10_we_o(), .s10_cyc_o(),
            .s10_stb_o(), .s10_ack_i(0), .s10_err_i(0), .s10_rty_i(0),
            // Slave 11 Interface
            .s11_data_i(0), .s11_data_o(), .s11_addr_o(), .s11_sel_o(), .s11_we_o(), .s11_cyc_o(),
            .s11_stb_o(), .s11_ack_i(0), .s11_err_i(0), .s11_rty_i(0),
            // Slave 12 Interface
            .s12_data_i(0), .s12_data_o(), .s12_addr_o(), .s12_sel_o(), .s12_we_o(), .s12_cyc_o(),
            .s12_stb_o(), .s12_ack_i(0), .s12_err_i(0), .s12_rty_i(0),
            // Slave 13 Interface
            .s13_data_i(0), .s13_data_o(), .s13_addr_o(), .s13_sel_o(), .s13_we_o(), .s13_cyc_o(),
            .s13_stb_o(), .s13_ack_i(0), .s13_err_i(0), .s13_rty_i(0),
            // Slave 14 Interface
            .s14_data_i(0), .s14_data_o(), .s14_addr_o(), .s14_sel_o(), .s14_we_o(), .s14_cyc_o(),
            .s14_stb_o(), .s14_ack_i(0), .s14_err_i(0), .s14_rty_i(0),
            // Slave 15 Interface
            .s15_data_i(0), .s15_data_o(), .s15_addr_o(), .s15_sel_o(), .s15_we_o(), .s15_cyc_o(),
            .s15_stb_o(), .s15_ack_i(0), .s15_err_i(0), .s15_rty_i(0)
        );
	
//-------------
/*    Parallel_Channel_Gen pk_gen1 (
        .nrst(nrst),
        .clk(clk),              
        .m4(pk1_m4),
        .rq(pk1_zap)
        );

    Parallel_Channel_Gen pk_gen2 (
        .nrst(nrst),
        .clk(clk),              
        .m4(pk2_m4),
        .rq(pk2_zap)
        );*/
        
/*    Orbita_Transmitter Orbita_Transmitter0
        (
            .nrst(nrst & test_rst),
            .clk(clk),        
            .clk_ext(st3_clk),
            .video(orbita)  
        );*/
    wire st1_clk;
    wire st2_clk;
    wire st3_clk;

    orb_pll_1 pll_st1
        (
            .inclk0(plm_clk),
            .c0    (st1_clk),
            .locked()
        );

    orb_pll_2 pll_st2
        (
            .inclk0(st1_clk),
            .c0    (st2_clk),
            .locked()
        );

    orb_pll_3 pll_st3
        (
            .inclk0(st2_clk),
            .c0    (st3_clk),
            .locked()
        );
    // assign cf_pdiag = 1'bz;
    // assign cf_dasp = 1'bz;
    
    logic rec_cmd;
    metastable_chain#(.CHAIN_DEPTH(3), .BUS_WIDTH(1)) 
    mtChRec(.clk(clk), .nrst(1'b1), .din(fpga_rec), .dout(rec_cmd));  
		 
	/* reg cf_vcc_en_reg;
	always @(posedge clk or negedge nrst)
		begin
			if (~nrst)
				cf_vcc_en_reg <= 1'b1;
			else
				cf_vcc_en_reg <= 1'b0;
		end
    */   
    spi_io fram_spi();
    assign FRAM_CS = fram_spi.ncs;
    assign FRAM_SI = fram_spi.mosi;
    assign FRAM_SCK = fram_spi.sck;
    assign fram_spi.miso = FRAM_SO;
    ird_ip_fm25l_ctrl irdIpFm25lCtrl(.clk(clk), .nrst(nrst), 
         // Wishbone slave interface
         .wbs(fram_wbs),
         
         // SPI interface
         .spi(fram_spi),         
         .ena(sett_upload),
         .log_full(fram_log_full),
         .next_cf_adr(fram_cf_adr),
         .next_rec_num(fram_rec_num),
         .log_rdy(fram_log_rdy));
endmodule
